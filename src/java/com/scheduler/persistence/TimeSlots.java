/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author S519479
 */
@Entity
@Table(name = "time_slots")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TimeSlots.findAll", query = "SELECT t FROM TimeSlots t"),
    @NamedQuery(name = "TimeSlots.findByTimeSlotId", query = "SELECT t FROM TimeSlots t WHERE t.timeSlotId = :timeSlotId"),
    @NamedQuery(name = "TimeSlots.findByDay", query = "SELECT t FROM TimeSlots t WHERE t.day = :day"),
    @NamedQuery(name = "TimeSlots.findByStartTime", query = "SELECT t FROM TimeSlots t WHERE t.startTime = :startTime"),
    @NamedQuery(name = "TimeSlots.findByEndTime", query = "SELECT t FROM TimeSlots t WHERE t.endTime = :endTime")})
public class TimeSlots implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "time_slot_id")
    private Integer timeSlotId;
    @Column(name = "day")
    private String day;
    @Column(name = "start_time")
    private String startTime;
    @Column(name = "end_time")
    private String endTime;
    @ManyToMany(mappedBy = "timeSlotsCollection")
    private Collection<Lab> labCollection;
    @ManyToMany(mappedBy = "timeSlotsCollection")
    private Collection<Section> sectionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "timeSlots")
    private Collection<Availability> availabilityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "timeSlots")
    private Collection<LegacyReport> legacyReportCollection;

    public TimeSlots() {
    }

    public TimeSlots(Integer timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public Integer getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(Integer timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @XmlTransient
    public Collection<Lab> getLabCollection() {
        return labCollection;
    }

    public void setLabCollection(Collection<Lab> labCollection) {
        this.labCollection = labCollection;
    }

    @XmlTransient
    public Collection<Section> getSectionCollection() {
        return sectionCollection;
    }

    public void setSectionCollection(Collection<Section> sectionCollection) {
        this.sectionCollection = sectionCollection;
    }

    @XmlTransient
    public Collection<Availability> getAvailabilityCollection() {
        return availabilityCollection;
    }

    public void setAvailabilityCollection(Collection<Availability> availabilityCollection) {
        this.availabilityCollection = availabilityCollection;
    }

    @XmlTransient
    public Collection<LegacyReport> getLegacyReportCollection() {
        return legacyReportCollection;
    }

    public void setLegacyReportCollection(Collection<LegacyReport> legacyReportCollection) {
        this.legacyReportCollection = legacyReportCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (timeSlotId != null ? timeSlotId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TimeSlots)) {
            return false;
        }
        TimeSlots other = (TimeSlots) object;
        if ((this.timeSlotId == null && other.timeSlotId != null) || (this.timeSlotId != null && !this.timeSlotId.equals(other.timeSlotId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.TimeSlots[ timeSlotId=" + timeSlotId + " ]";
    }
    
}
