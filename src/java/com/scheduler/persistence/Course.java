/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author S519479
 */
@Entity
@Table(name = "course")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Course.findAll", query = "SELECT c FROM Course c"),
    @NamedQuery(name = "Course.findByDepartmentId", query = "SELECT c FROM Course c WHERE c.coursePK.departmentId = :departmentId"),
    @NamedQuery(name = "Course.findByCourseId", query = "SELECT c FROM Course c WHERE c.coursePK.courseId = :courseId"),
    @NamedQuery(name = "Course.findByCourseName", query = "SELECT c FROM Course c WHERE c.courseName = :courseName"),
    @NamedQuery(name = "Course.findByLabType", query = "SELECT c FROM Course c WHERE c.labType = :labType")})
public class Course implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CoursePK coursePK;
    @Column(name = "course_name")
    private String courseName;
    @Column(name = "lab_type")
    private String labType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "course")
    private Collection<ProfessorCourse> professorCourseCollection;
    @JoinColumn(name = "department_id", referencedColumnName = "department_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Department department;

    public Course() {
    }

    public Course(CoursePK coursePK) {
        this.coursePK = coursePK;
    }

    public Course(int departmentId, int courseId) {
        this.coursePK = new CoursePK(departmentId, courseId);
    }

    public CoursePK getCoursePK() {
        return coursePK;
    }

    public void setCoursePK(CoursePK coursePK) {
        this.coursePK = coursePK;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getLabType() {
        return labType;
    }

    public void setLabType(String labType) {
        this.labType = labType;
    }

    @XmlTransient
    public Collection<ProfessorCourse> getProfessorCourseCollection() {
        return professorCourseCollection;
    }

    public void setProfessorCourseCollection(Collection<ProfessorCourse> professorCourseCollection) {
        this.professorCourseCollection = professorCourseCollection;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coursePK != null ? coursePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Course)) {
            return false;
        }
        Course other = (Course) object;
        if ((this.coursePK == null && other.coursePK != null) || (this.coursePK != null && !this.coursePK.equals(other.coursePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.Course[ coursePK=" + coursePK + " ]";
    }
    
}
