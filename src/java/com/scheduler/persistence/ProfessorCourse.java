/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author S519479
 */
@Entity
@Table(name = "professor_course")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProfessorCourse.findAll", query = "SELECT p FROM ProfessorCourse p"),
    @NamedQuery(name = "ProfessorCourse.findByDepartrmentId", query = "SELECT p FROM ProfessorCourse p WHERE p.professorCoursePK.departrmentId = :departrmentId"),
    @NamedQuery(name = "ProfessorCourse.findByCourseId", query = "SELECT p FROM ProfessorCourse p WHERE p.professorCoursePK.courseId = :courseId"),
    @NamedQuery(name = "ProfessorCourse.findByProfessorId", query = "SELECT p FROM ProfessorCourse p WHERE p.professorCoursePK.professorId = :professorId")})
public class ProfessorCourse implements Serializable, Comparator<ProfessorCourse>, Comparable<ProfessorCourse> {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProfessorCoursePK professorCoursePK;
    @JoinColumns({
        @JoinColumn(name = "departrment_id", referencedColumnName = "department_id", insertable = false, updatable = false),
        @JoinColumn(name = "course_id", referencedColumnName = "course_id", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Course course;
    @JoinColumn(name = "professor_id", referencedColumnName = "professor_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Professor professor;
    @OneToMany(mappedBy = "professorCourse")
    private Collection<Section> sectionCollection;

    public ProfessorCourse() {
    }

    public ProfessorCourse(ProfessorCoursePK professorCoursePK) {
        this.professorCoursePK = professorCoursePK;
    }

    public ProfessorCourse(int departrmentId, int courseId, String professorId) {
        this.professorCoursePK = new ProfessorCoursePK(departrmentId, courseId, professorId);
    }

    public ProfessorCoursePK getProfessorCoursePK() {
        return professorCoursePK;
    }

    public void setProfessorCoursePK(ProfessorCoursePK professorCoursePK) {
        this.professorCoursePK = professorCoursePK;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @XmlTransient
    public Collection<Section> getSectionCollection() {
        return sectionCollection;
    }

    public void setSectionCollection(Collection<Section> sectionCollection) {
        this.sectionCollection = sectionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (professorCoursePK != null ? professorCoursePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfessorCourse)) {
            return false;
        }
        ProfessorCourse other = (ProfessorCourse) object;
        if ((this.professorCoursePK == null && other.professorCoursePK != null) || (this.professorCoursePK != null && !this.professorCoursePK.equals(other.professorCoursePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.ProfessorCourse[ professorCoursePK=" + professorCoursePK + " ]";
    }

    @Override
    public int compare(ProfessorCourse o1, ProfessorCourse o2) {
         return o1.getProfessor().getLastName().compareTo(o2.getProfessor().getLastName());
    }

    @Override
    public int compareTo(ProfessorCourse o) {
        return this.getProfessor().getLastName().compareTo(o.getProfessor().getLastName());
    }

}
