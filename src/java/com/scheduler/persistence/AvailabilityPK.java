/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author S519479
 */
@Embeddable
public class AvailabilityPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "time_slot_id")
    private int timeSlotId;
    @Basic(optional = false)
    @Column(name = "building_id")
    private String buildingId;
    @Basic(optional = false)
    @Column(name = "room_number")
    private int roomNumber;

    public AvailabilityPK() {
    }

    public AvailabilityPK(int timeSlotId, String buildingId, int roomNumber) {
        this.timeSlotId = timeSlotId;
        this.buildingId = buildingId;
        this.roomNumber = roomNumber;
    }

    public int getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(int timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) timeSlotId;
        hash += (buildingId != null ? buildingId.hashCode() : 0);
        hash += (int) roomNumber;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AvailabilityPK)) {
            return false;
        }
        AvailabilityPK other = (AvailabilityPK) object;
        if (this.timeSlotId != other.timeSlotId) {
            return false;
        }
        if ((this.buildingId == null && other.buildingId != null) || (this.buildingId != null && !this.buildingId.equals(other.buildingId))) {
            return false;
        }
        if (this.roomNumber != other.roomNumber) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.AvailabilityPK[ timeSlotId=" + timeSlotId + ", buildingId=" + buildingId + ", roomNumber=" + roomNumber + " ]";
    }
    
}
