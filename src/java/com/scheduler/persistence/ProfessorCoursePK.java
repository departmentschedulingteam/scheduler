/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author S519479
 */
@Embeddable
public class ProfessorCoursePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "departrment_id")
    private int departrmentId;
    @Basic(optional = false)
    @Column(name = "course_id")
    private int courseId;
    @Basic(optional = false)
    @Column(name = "professor_id")
    private String professorId;

    public ProfessorCoursePK() {
    }

    public ProfessorCoursePK(int departrmentId, int courseId, String professorId) {
        this.departrmentId = departrmentId;
        this.courseId = courseId;
        this.professorId = professorId;
    }

    public int getDepartrmentId() {
        return departrmentId;
    }

    public void setDepartrmentId(int departrmentId) {
        this.departrmentId = departrmentId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getProfessorId() {
        return professorId;
    }

    public void setProfessorId(String professorId) {
        this.professorId = professorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) departrmentId;
        hash += (int) courseId;
        hash += (professorId != null ? professorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfessorCoursePK)) {
            return false;
        }
        ProfessorCoursePK other = (ProfessorCoursePK) object;
        if (this.departrmentId != other.departrmentId) {
            return false;
        }
        if (this.courseId != other.courseId) {
            return false;
        }
        if ((this.professorId == null && other.professorId != null) || (this.professorId != null && !this.professorId.equals(other.professorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.ProfessorCoursePK[ departrmentId=" + departrmentId + ", courseId=" + courseId + ", professorId=" + professorId + " ]";
    }
    
}
