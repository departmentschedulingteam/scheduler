/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author S519479
 */
@Entity
@Table(name = "legacy_report")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LegacyReport.findAll", query = "SELECT l FROM LegacyReport l"),
    @NamedQuery(name = "LegacyReport.findByYearSemesterReportName", query = "SELECT l FROM LegacyReport l WHERE l.legacyReportPK.year = :year AND l.legacyReportPK.semester = :semester AND l.legacyReportPK.reportName = :reportName"),
    @NamedQuery(name = "LegacyReport.findByYear", query = "SELECT l FROM LegacyReport l WHERE l.legacyReportPK.year = :year"),
    @NamedQuery(name = "LegacyReport.findBySemester", query = "SELECT l FROM LegacyReport l WHERE l.legacyReportPK.semester = :semester"),
    @NamedQuery(name = "LegacyReport.findByReportName", query = "SELECT l FROM LegacyReport l WHERE l.legacyReportPK.reportName = :reportName"),
    @NamedQuery(name = "LegacyReport.findByTimeSlotId", query = "SELECT l FROM LegacyReport l WHERE l.legacyReportPK.timeSlotId = :timeSlotId"),
    @NamedQuery(name = "LegacyReport.findBySectionOrLabFullId", query = "SELECT l FROM LegacyReport l WHERE l.legacyReportPK.sectionOrLabFullId = :sectionOrLabFullId"),
    @NamedQuery(name = "LegacyReport.findByDepartmentId", query = "SELECT l FROM LegacyReport l WHERE l.departmentId = :departmentId"),
    @NamedQuery(name = "LegacyReport.findByDepartmentName", query = "SELECT l FROM LegacyReport l WHERE l.departmentName = :departmentName"),
    @NamedQuery(name = "LegacyReport.findByCourseId", query = "SELECT l FROM LegacyReport l WHERE l.courseId = :courseId"),
    @NamedQuery(name = "LegacyReport.findByCourseName", query = "SELECT l FROM LegacyReport l WHERE l.courseName = :courseName"),
    @NamedQuery(name = "LegacyReport.findByProfessorId", query = "SELECT l FROM LegacyReport l WHERE l.professorId = :professorId"),
    @NamedQuery(name = "LegacyReport.findByProfessorFirstName", query = "SELECT l FROM LegacyReport l WHERE l.professorFirstName = :professorFirstName"),
    @NamedQuery(name = "LegacyReport.findByProfessorLastName", query = "SELECT l FROM LegacyReport l WHERE l.professorLastName = :professorLastName"),
    @NamedQuery(name = "LegacyReport.findByProfessorInitials", query = "SELECT l FROM LegacyReport l WHERE l.professorInitials = :professorInitials"),
    @NamedQuery(name = "LegacyReport.findByBuildingId", query = "SELECT l FROM LegacyReport l WHERE l.buildingId = :buildingId"),
    @NamedQuery(name = "LegacyReport.findByBuildingName", query = "SELECT l FROM LegacyReport l WHERE l.buildingName = :buildingName"),
    @NamedQuery(name = "LegacyReport.findByRoomNumber", query = "SELECT l FROM LegacyReport l WHERE l.roomNumber = :roomNumber"),
    @NamedQuery(name = "LegacyReport.findByRoomCapacity", query = "SELECT l FROM LegacyReport l WHERE l.roomCapacity = :roomCapacity")})
public class LegacyReport implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LegacyReportPK legacyReportPK;
    @Column(name = "department_id")
    private Integer departmentId;
    @Column(name = "department_name")
    private String departmentName;
    @Column(name = "course_id")
    private Integer courseId;
    @Column(name = "course_name")
    private String courseName;
    @Column(name = "professor_id")
    private String professorId;
    @Column(name = "professor_first_name")
    private String professorFirstName;
    @Column(name = "professor_last_name")
    private String professorLastName;
    @Column(name = "professor_initials")
    private String professorInitials;
    @Column(name = "building_id")
    private String buildingId;
    @Column(name = "building_name")
    private String buildingName;
    @Column(name = "room_number")
    private String roomNumber;
    @Column(name = "room_capacity")
    private String roomCapacity;
    @JoinColumn(name = "time_slot_id", referencedColumnName = "time_slot_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TimeSlots timeSlots;

    public LegacyReport() {
    }

    public LegacyReport(LegacyReportPK legacyReportPK) {
        this.legacyReportPK = legacyReportPK;
    }

    public LegacyReport(int year, String semester, String reportName, int timeSlotId, String sectionOrLabFullId) {
        this.legacyReportPK = new LegacyReportPK(year, semester, reportName, timeSlotId, sectionOrLabFullId);
    }

    public LegacyReportPK getLegacyReportPK() {
        return legacyReportPK;
    }

    public void setLegacyReportPK(LegacyReportPK legacyReportPK) {
        this.legacyReportPK = legacyReportPK;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getProfessorId() {
        return professorId;
    }

    public void setProfessorId(String professorId) {
        this.professorId = professorId;
    }

    public String getProfessorFirstName() {
        return professorFirstName;
    }

    public void setProfessorFirstName(String professorFirstName) {
        this.professorFirstName = professorFirstName;
    }

    public String getProfessorLastName() {
        return professorLastName;
    }

    public void setProfessorLastName(String professorLastName) {
        this.professorLastName = professorLastName;
    }

    public String getProfessorInitials() {
        return professorInitials;
    }

    public void setProfessorInitials(String professorInitials) {
        this.professorInitials = professorInitials;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomCapacity() {
        return roomCapacity;
    }

    public void setRoomCapacity(String roomCapacity) {
        this.roomCapacity = roomCapacity;
    }

    public TimeSlots getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(TimeSlots timeSlots) {
        this.timeSlots = timeSlots;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (legacyReportPK != null ? legacyReportPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LegacyReport)) {
            return false;
        }
        LegacyReport other = (LegacyReport) object;
        if ((this.legacyReportPK == null && other.legacyReportPK != null) || (this.legacyReportPK != null && !this.legacyReportPK.equals(other.legacyReportPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.LegacyReport[ legacyReportPK=" + legacyReportPK + " ]";
    }
    
}
