/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author S519479
 */
@Embeddable
public class LegacyReportPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @Column(name = "semester")
    private String semester;
    @Basic(optional = false)
    @Column(name = "report_name")
    private String reportName;
    @Basic(optional = false)
    @Column(name = "time_slot_id")
    private int timeSlotId;
    @Basic(optional = false)
    @Column(name = "section_or_lab_full_id")
    private String sectionOrLabFullId;

    public LegacyReportPK() {
    }

    public LegacyReportPK(int year, String semester, String reportName, int timeSlotId, String sectionOrLabFullId) {
        this.year = year;
        this.semester = semester;
        this.reportName = reportName;
        this.timeSlotId = timeSlotId;
        this.sectionOrLabFullId = sectionOrLabFullId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public int getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(int timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public String getSectionOrLabFullId() {
        return sectionOrLabFullId;
    }

    public void setSectionOrLabFullId(String sectionOrLabFullId) {
        this.sectionOrLabFullId = sectionOrLabFullId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) year;
        hash += (semester != null ? semester.hashCode() : 0);
        hash += (reportName != null ? reportName.hashCode() : 0);
        hash += (int) timeSlotId;
        hash += (sectionOrLabFullId != null ? sectionOrLabFullId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LegacyReportPK)) {
            return false;
        }
        LegacyReportPK other = (LegacyReportPK) object;
        if (this.year != other.year) {
            return false;
        }
        if ((this.semester == null && other.semester != null) || (this.semester != null && !this.semester.equals(other.semester))) {
            return false;
        }
        if ((this.reportName == null && other.reportName != null) || (this.reportName != null && !this.reportName.equals(other.reportName))) {
            return false;
        }
        if (this.timeSlotId != other.timeSlotId) {
            return false;
        }
        if ((this.sectionOrLabFullId == null && other.sectionOrLabFullId != null) || (this.sectionOrLabFullId != null && !this.sectionOrLabFullId.equals(other.sectionOrLabFullId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.LegacyReportPK[ year=" + year + ", semester=" + semester + ", reportName=" + reportName + ", timeSlotId=" + timeSlotId + ", sectionOrLabFullId=" + sectionOrLabFullId + " ]";
    }
    
}
