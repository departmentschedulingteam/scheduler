/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author S519479
 */
@Entity
@Table(name = "lab")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lab.findAll", query = "SELECT l FROM Lab l"),
    @NamedQuery(name = "Lab.findByLabFullId", query = "SELECT l FROM Lab l WHERE l.labFullId = :labFullId"),
    @NamedQuery(name = "Lab.findByOccupied", query = "SELECT l FROM Lab l WHERE l.occupied = :occupied")})
public class Lab implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "lab_full_id")
    private String labFullId;
    @Column(name = "occupied")
    private Integer occupied;
    @JoinTable(name = "lab_slot", joinColumns = {
        @JoinColumn(name = "lab_full_id", referencedColumnName = "lab_full_id")}, inverseJoinColumns = {
        @JoinColumn(name = "time_slot_id", referencedColumnName = "time_slot_id")})
    @ManyToMany
    private Collection<TimeSlots> timeSlotsCollection;
    @JoinColumns({
        @JoinColumn(name = "building_id", referencedColumnName = "building_id"),
        @JoinColumn(name = "room_number", referencedColumnName = "room_number")})
    @ManyToOne
    private Room room;
    @JoinColumn(name = "section_full_id", referencedColumnName = "section_full_id")
    @ManyToOne
    private Section sectionFullId;

    public Lab() {
    }

    public Lab(String labFullId) {
        this.labFullId = labFullId;
    }

    public String getLabFullId() {
        return labFullId;
    }

    public void setLabFullId(String labFullId) {
        this.labFullId = labFullId;
    }

    public Integer getOccupied() {
        return occupied;
    }

    public void setOccupied(Integer occupied) {
        this.occupied = occupied;
    }

    @XmlTransient
    public Collection<TimeSlots> getTimeSlotsCollection() {
        return timeSlotsCollection;
    }

    public void setTimeSlotsCollection(Collection<TimeSlots> timeSlotsCollection) {
        this.timeSlotsCollection = timeSlotsCollection;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Section getSectionFullId() {
        return sectionFullId;
    }

    public void setSectionFullId(Section sectionFullId) {
        this.sectionFullId = sectionFullId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (labFullId != null ? labFullId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lab)) {
            return false;
        }
        Lab other = (Lab) object;
        if ((this.labFullId == null && other.labFullId != null) || (this.labFullId != null && !this.labFullId.equals(other.labFullId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.Lab[ labFullId=" + labFullId + " ]";
    }
    
}
