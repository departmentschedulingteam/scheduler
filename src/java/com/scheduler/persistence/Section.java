/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author S519479
 */
@Entity
@Table(name = "section")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Section.findAll", query = "SELECT s FROM Section s"),
    @NamedQuery(name = "Section.findBySectionFullId", query = "SELECT s FROM Section s WHERE s.sectionFullId = :sectionFullId"),
    @NamedQuery(name = "Section.findBySectionId", query = "SELECT s FROM Section s WHERE s.sectionId = :sectionId"),
    @NamedQuery(name = "Section.findByOccupied", query = "SELECT s FROM Section s WHERE s.occupied = :occupied")})
public class Section implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "section_full_id")
    private String sectionFullId;
    @Column(name = "section_id")
    private Integer sectionId;
    @Column(name = "occupied")
    private Integer occupied;
    @JoinTable(name = "section_slot", joinColumns = {
        @JoinColumn(name = "section_full_id", referencedColumnName = "section_full_id")}, inverseJoinColumns = {
        @JoinColumn(name = "time_slot_id", referencedColumnName = "time_slot_id")})
    @ManyToMany
    private Collection<TimeSlots> timeSlotsCollection;
    @JoinColumns({
        @JoinColumn(name = "department_id", referencedColumnName = "departrment_id"),
        @JoinColumn(name = "course_id", referencedColumnName = "course_id"),
        @JoinColumn(name = "professor_id", referencedColumnName = "professor_id")})
    @ManyToOne
    private ProfessorCourse professorCourse;
    @JoinColumns({
        @JoinColumn(name = "building_id", referencedColumnName = "building_id"),
        @JoinColumn(name = "room_number", referencedColumnName = "room_number")})
    @ManyToOne
    private Room room;
    @OneToMany(mappedBy = "sectionFullId")
    private Collection<Lab> labCollection;

    public Section() {
    }

    public Section(String sectionFullId) {
        this.sectionFullId = sectionFullId;
    }

    public String getSectionFullId() {
        return sectionFullId;
    }

    public void setSectionFullId(String sectionFullId) {
        this.sectionFullId = sectionFullId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getOccupied() {
        return occupied;
    }

    public void setOccupied(Integer occupied) {
        this.occupied = occupied;
    }

    @XmlTransient
    public Collection<TimeSlots> getTimeSlotsCollection() {
        return timeSlotsCollection;
    }

    public void setTimeSlotsCollection(Collection<TimeSlots> timeSlotsCollection) {
        this.timeSlotsCollection = timeSlotsCollection;
    }

    public ProfessorCourse getProfessorCourse() {
        return professorCourse;
    }

    public void setProfessorCourse(ProfessorCourse professorCourse) {
        this.professorCourse = professorCourse;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @XmlTransient
    public Collection<Lab> getLabCollection() {
        return labCollection;
    }

    public void setLabCollection(Collection<Lab> labCollection) {
        this.labCollection = labCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sectionFullId != null ? sectionFullId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Section)) {
            return false;
        }
        Section other = (Section) object;
        if ((this.sectionFullId == null && other.sectionFullId != null) || (this.sectionFullId != null && !this.sectionFullId.equals(other.sectionFullId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.Section[ sectionFullId=" + sectionFullId + " ]";
    }
    
}
