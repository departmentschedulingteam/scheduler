/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author S519479
 */
@Embeddable
public class CoursePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "department_id")
    private int departmentId;
    @Basic(optional = false)
    @Column(name = "course_id")
    private int courseId;

    public CoursePK() {
    }

    public CoursePK(int departmentId, int courseId) {
        this.departmentId = departmentId;
        this.courseId = courseId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) departmentId;
        hash += (int) courseId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CoursePK)) {
            return false;
        }
        CoursePK other = (CoursePK) object;
        if (this.departmentId != other.departmentId) {
            return false;
        }
        if (this.courseId != other.courseId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.CoursePK[ departmentId=" + departmentId + ", courseId=" + courseId + " ]";
    }
    
}
