/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.TimeSlots;
import java.util.ArrayList;
import java.util.Collection;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author S519479
 */
public class SectionJpaController implements Serializable {

    public SectionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Section section) throws PreexistingEntityException, Exception {
        if (section.getTimeSlotsCollection() == null) {
            section.setTimeSlotsCollection(new ArrayList<TimeSlots>());
        }
        if (section.getLabCollection() == null) {
            section.setLabCollection(new ArrayList<Lab>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProfessorCourse professorCourse = section.getProfessorCourse();
            if (professorCourse != null) {
                professorCourse = em.getReference(professorCourse.getClass(), professorCourse.getProfessorCoursePK());
                section.setProfessorCourse(professorCourse);
            }
            Room room = section.getRoom();
            if (room != null) {
                room = em.getReference(room.getClass(), room.getRoomPK());
                section.setRoom(room);
            }
            Collection<TimeSlots> attachedTimeSlotsCollection = new ArrayList<TimeSlots>();
            for (TimeSlots timeSlotsCollectionTimeSlotsToAttach : section.getTimeSlotsCollection()) {
                timeSlotsCollectionTimeSlotsToAttach = em.getReference(timeSlotsCollectionTimeSlotsToAttach.getClass(), timeSlotsCollectionTimeSlotsToAttach.getTimeSlotId());
                attachedTimeSlotsCollection.add(timeSlotsCollectionTimeSlotsToAttach);
            }
            section.setTimeSlotsCollection(attachedTimeSlotsCollection);
            Collection<Lab> attachedLabCollection = new ArrayList<Lab>();
            for (Lab labCollectionLabToAttach : section.getLabCollection()) {
                labCollectionLabToAttach = em.getReference(labCollectionLabToAttach.getClass(), labCollectionLabToAttach.getLabFullId());
                attachedLabCollection.add(labCollectionLabToAttach);
            }
            section.setLabCollection(attachedLabCollection);
            em.persist(section);
            if (professorCourse != null) {
                professorCourse.getSectionCollection().add(section);
                professorCourse = em.merge(professorCourse);
            }
            if (room != null) {
                room.getSectionCollection().add(section);
                room = em.merge(room);
            }
            for (TimeSlots timeSlotsCollectionTimeSlots : section.getTimeSlotsCollection()) {
                timeSlotsCollectionTimeSlots.getSectionCollection().add(section);
                timeSlotsCollectionTimeSlots = em.merge(timeSlotsCollectionTimeSlots);
            }
            for (Lab labCollectionLab : section.getLabCollection()) {
                Section oldSectionFullIdOfLabCollectionLab = labCollectionLab.getSectionFullId();
                labCollectionLab.setSectionFullId(section);
                labCollectionLab = em.merge(labCollectionLab);
                if (oldSectionFullIdOfLabCollectionLab != null) {
                    oldSectionFullIdOfLabCollectionLab.getLabCollection().remove(labCollectionLab);
                    oldSectionFullIdOfLabCollectionLab = em.merge(oldSectionFullIdOfLabCollectionLab);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSection(section.getSectionFullId()) != null) {
                throw new PreexistingEntityException("Section " + section + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Section section) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Section persistentSection = em.find(Section.class, section.getSectionFullId());
            ProfessorCourse professorCourseOld = persistentSection.getProfessorCourse();
            ProfessorCourse professorCourseNew = section.getProfessorCourse();
            Room roomOld = persistentSection.getRoom();
            Room roomNew = section.getRoom();
            Collection<TimeSlots> timeSlotsCollectionOld = persistentSection.getTimeSlotsCollection();
            Collection<TimeSlots> timeSlotsCollectionNew = section.getTimeSlotsCollection();
            Collection<Lab> labCollectionOld = persistentSection.getLabCollection();
            Collection<Lab> labCollectionNew = section.getLabCollection();
            if (professorCourseNew != null) {
                professorCourseNew = em.getReference(professorCourseNew.getClass(), professorCourseNew.getProfessorCoursePK());
                section.setProfessorCourse(professorCourseNew);
            }
            if (roomNew != null) {
                roomNew = em.getReference(roomNew.getClass(), roomNew.getRoomPK());
                section.setRoom(roomNew);
            }
            Collection<TimeSlots> attachedTimeSlotsCollectionNew = new ArrayList<TimeSlots>();
            for (TimeSlots timeSlotsCollectionNewTimeSlotsToAttach : timeSlotsCollectionNew) {
                timeSlotsCollectionNewTimeSlotsToAttach = em.getReference(timeSlotsCollectionNewTimeSlotsToAttach.getClass(), timeSlotsCollectionNewTimeSlotsToAttach.getTimeSlotId());
                attachedTimeSlotsCollectionNew.add(timeSlotsCollectionNewTimeSlotsToAttach);
            }
            timeSlotsCollectionNew = attachedTimeSlotsCollectionNew;
            section.setTimeSlotsCollection(timeSlotsCollectionNew);
            Collection<Lab> attachedLabCollectionNew = new ArrayList<Lab>();
            for (Lab labCollectionNewLabToAttach : labCollectionNew) {
                labCollectionNewLabToAttach = em.getReference(labCollectionNewLabToAttach.getClass(), labCollectionNewLabToAttach.getLabFullId());
                attachedLabCollectionNew.add(labCollectionNewLabToAttach);
            }
            labCollectionNew = attachedLabCollectionNew;
            section.setLabCollection(labCollectionNew);
            section = em.merge(section);
            if (professorCourseOld != null && !professorCourseOld.equals(professorCourseNew)) {
                professorCourseOld.getSectionCollection().remove(section);
                professorCourseOld = em.merge(professorCourseOld);
            }
            if (professorCourseNew != null && !professorCourseNew.equals(professorCourseOld)) {
                professorCourseNew.getSectionCollection().add(section);
                professorCourseNew = em.merge(professorCourseNew);
            }
            if (roomOld != null && !roomOld.equals(roomNew)) {
                roomOld.getSectionCollection().remove(section);
                roomOld = em.merge(roomOld);
            }
            if (roomNew != null && !roomNew.equals(roomOld)) {
                roomNew.getSectionCollection().add(section);
                roomNew = em.merge(roomNew);
            }
            for (TimeSlots timeSlotsCollectionOldTimeSlots : timeSlotsCollectionOld) {
                if (!timeSlotsCollectionNew.contains(timeSlotsCollectionOldTimeSlots)) {
                    timeSlotsCollectionOldTimeSlots.getSectionCollection().remove(section);
                    timeSlotsCollectionOldTimeSlots = em.merge(timeSlotsCollectionOldTimeSlots);
                }
            }
            for (TimeSlots timeSlotsCollectionNewTimeSlots : timeSlotsCollectionNew) {
                if (!timeSlotsCollectionOld.contains(timeSlotsCollectionNewTimeSlots)) {
                    timeSlotsCollectionNewTimeSlots.getSectionCollection().add(section);
                    timeSlotsCollectionNewTimeSlots = em.merge(timeSlotsCollectionNewTimeSlots);
                }
            }
            for (Lab labCollectionOldLab : labCollectionOld) {
                if (!labCollectionNew.contains(labCollectionOldLab)) {
                    labCollectionOldLab.setSectionFullId(null);
                    labCollectionOldLab = em.merge(labCollectionOldLab);
                }
            }
            for (Lab labCollectionNewLab : labCollectionNew) {
                if (!labCollectionOld.contains(labCollectionNewLab)) {
                    Section oldSectionFullIdOfLabCollectionNewLab = labCollectionNewLab.getSectionFullId();
                    labCollectionNewLab.setSectionFullId(section);
                    labCollectionNewLab = em.merge(labCollectionNewLab);
                    if (oldSectionFullIdOfLabCollectionNewLab != null && !oldSectionFullIdOfLabCollectionNewLab.equals(section)) {
                        oldSectionFullIdOfLabCollectionNewLab.getLabCollection().remove(labCollectionNewLab);
                        oldSectionFullIdOfLabCollectionNewLab = em.merge(oldSectionFullIdOfLabCollectionNewLab);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = section.getSectionFullId();
                if (findSection(id) == null) {
                    throw new NonexistentEntityException("The section with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Section section;
            try {
                section = em.getReference(Section.class, id);
                section.getSectionFullId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The section with id " + id + " no longer exists.", enfe);
            }
            ProfessorCourse professorCourse = section.getProfessorCourse();
            if (professorCourse != null) {
                professorCourse.getSectionCollection().remove(section);
                professorCourse = em.merge(professorCourse);
            }
            Room room = section.getRoom();
            if (room != null) {
                room.getSectionCollection().remove(section);
                room = em.merge(room);
            }
            Collection<TimeSlots> timeSlotsCollection = section.getTimeSlotsCollection();
            for (TimeSlots timeSlotsCollectionTimeSlots : timeSlotsCollection) {
                timeSlotsCollectionTimeSlots.getSectionCollection().remove(section);
                timeSlotsCollectionTimeSlots = em.merge(timeSlotsCollectionTimeSlots);
            }
            Collection<Lab> labCollection = section.getLabCollection();
            for (Lab labCollectionLab : labCollection) {
                labCollectionLab.setSectionFullId(null);
                labCollectionLab = em.merge(labCollectionLab);
            }
            em.remove(section);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Section> findSectionEntities() {
        return findSectionEntities(true, -1, -1);
    }

    public List<Section> findSectionEntities(int maxResults, int firstResult) {
        return findSectionEntities(false, maxResults, firstResult);
    }

    private List<Section> findSectionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Section.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Section findSection(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Section.class, id);
        } finally {
            em.close();
        }
    }

    public int getSectionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Section> rt = cq.from(Section.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        }
