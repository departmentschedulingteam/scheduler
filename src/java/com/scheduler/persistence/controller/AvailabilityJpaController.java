/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import com.scheduler.persistence.Availability;
import com.scheduler.persistence.AvailabilityPK;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author S519479
 */
public class AvailabilityJpaController implements Serializable {

    public AvailabilityJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Availability availability) throws PreexistingEntityException, Exception {
        if (availability.getAvailabilityPK() == null) {
            availability.setAvailabilityPK(new AvailabilityPK());
        }
        availability.getAvailabilityPK().setTimeSlotId(availability.getTimeSlots().getTimeSlotId());
        availability.getAvailabilityPK().setRoomNumber(availability.getRoom().getRoomPK().getRoomNumber());
        availability.getAvailabilityPK().setBuildingId(availability.getRoom().getRoomPK().getBuildingId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Room room = availability.getRoom();
            if (room != null) {
                room = em.getReference(room.getClass(), room.getRoomPK());
                availability.setRoom(room);
            }
            TimeSlots timeSlots = availability.getTimeSlots();
            if (timeSlots != null) {
                timeSlots = em.getReference(timeSlots.getClass(), timeSlots.getTimeSlotId());
                availability.setTimeSlots(timeSlots);
            }
            em.persist(availability);
            if (room != null) {
                room.getAvailabilityCollection().add(availability);
                room = em.merge(room);
            }
            if (timeSlots != null) {
                timeSlots.getAvailabilityCollection().add(availability);
                timeSlots = em.merge(timeSlots);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAvailability(availability.getAvailabilityPK()) != null) {
                throw new PreexistingEntityException("Availability " + availability + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Availability availability) throws NonexistentEntityException, Exception {
        availability.getAvailabilityPK().setTimeSlotId(availability.getTimeSlots().getTimeSlotId());
        availability.getAvailabilityPK().setRoomNumber(availability.getRoom().getRoomPK().getRoomNumber());
        availability.getAvailabilityPK().setBuildingId(availability.getRoom().getRoomPK().getBuildingId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Availability persistentAvailability = em.find(Availability.class, availability.getAvailabilityPK());
            Room roomOld = persistentAvailability.getRoom();
            Room roomNew = availability.getRoom();
            TimeSlots timeSlotsOld = persistentAvailability.getTimeSlots();
            TimeSlots timeSlotsNew = availability.getTimeSlots();
            if (roomNew != null) {
                roomNew = em.getReference(roomNew.getClass(), roomNew.getRoomPK());
                availability.setRoom(roomNew);
            }
            if (timeSlotsNew != null) {
                timeSlotsNew = em.getReference(timeSlotsNew.getClass(), timeSlotsNew.getTimeSlotId());
                availability.setTimeSlots(timeSlotsNew);
            }
            availability = em.merge(availability);
            if (roomOld != null && !roomOld.equals(roomNew)) {
                roomOld.getAvailabilityCollection().remove(availability);
                roomOld = em.merge(roomOld);
            }
            if (roomNew != null && !roomNew.equals(roomOld)) {
                roomNew.getAvailabilityCollection().add(availability);
                roomNew = em.merge(roomNew);
            }
            if (timeSlotsOld != null && !timeSlotsOld.equals(timeSlotsNew)) {
                timeSlotsOld.getAvailabilityCollection().remove(availability);
                timeSlotsOld = em.merge(timeSlotsOld);
            }
            if (timeSlotsNew != null && !timeSlotsNew.equals(timeSlotsOld)) {
                timeSlotsNew.getAvailabilityCollection().add(availability);
                timeSlotsNew = em.merge(timeSlotsNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                AvailabilityPK id = availability.getAvailabilityPK();
                if (findAvailability(id) == null) {
                    throw new NonexistentEntityException("The availability with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(AvailabilityPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Availability availability;
            try {
                availability = em.getReference(Availability.class, id);
                availability.getAvailabilityPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The availability with id " + id + " no longer exists.", enfe);
            }
            Room room = availability.getRoom();
            if (room != null) {
                room.getAvailabilityCollection().remove(availability);
                room = em.merge(room);
            }
            TimeSlots timeSlots = availability.getTimeSlots();
            if (timeSlots != null) {
                timeSlots.getAvailabilityCollection().remove(availability);
                timeSlots = em.merge(timeSlots);
            }
            em.remove(availability);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Availability> findAvailabilityEntities() {
        return findAvailabilityEntities(true, -1, -1);
    }

    public List<Availability> findAvailabilityEntities(int maxResults, int firstResult) {
        return findAvailabilityEntities(false, maxResults, firstResult);
    }

    private List<Availability> findAvailabilityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Availability.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Availability findAvailability(AvailabilityPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Availability.class, id);
        } finally {
            em.close();
        }
    }

    public int getAvailabilityCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Availability> rt = cq.from(Availability.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        }
