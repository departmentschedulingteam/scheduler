/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import com.scheduler.persistence.Course;
import com.scheduler.persistence.Course_;
import com.scheduler.persistence.Professor;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.Professor_;
import com.scheduler.persistence.controller.exceptions.IllegalOrphanException;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author S519479
 */
public class ProfessorJpaController implements Serializable {

    public ProfessorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Professor professor) throws PreexistingEntityException, Exception {
        if (professor.getProfessorCourseCollection() == null) {
            professor.setProfessorCourseCollection(new ArrayList<ProfessorCourse>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<ProfessorCourse> attachedProfessorCourseCollection = new ArrayList<ProfessorCourse>();
            for (ProfessorCourse professorCourseCollectionProfessorCourseToAttach : professor.getProfessorCourseCollection()) {
                professorCourseCollectionProfessorCourseToAttach = em.getReference(professorCourseCollectionProfessorCourseToAttach.getClass(), professorCourseCollectionProfessorCourseToAttach.getProfessorCoursePK());
                attachedProfessorCourseCollection.add(professorCourseCollectionProfessorCourseToAttach);
            }
            professor.setProfessorCourseCollection(attachedProfessorCourseCollection);
            em.persist(professor);
            for (ProfessorCourse professorCourseCollectionProfessorCourse : professor.getProfessorCourseCollection()) {
                Professor oldProfessorOfProfessorCourseCollectionProfessorCourse = professorCourseCollectionProfessorCourse.getProfessor();
                professorCourseCollectionProfessorCourse.setProfessor(professor);
                professorCourseCollectionProfessorCourse = em.merge(professorCourseCollectionProfessorCourse);
                if (oldProfessorOfProfessorCourseCollectionProfessorCourse != null) {
                    oldProfessorOfProfessorCourseCollectionProfessorCourse.getProfessorCourseCollection().remove(professorCourseCollectionProfessorCourse);
                    oldProfessorOfProfessorCourseCollectionProfessorCourse = em.merge(oldProfessorOfProfessorCourseCollectionProfessorCourse);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProfessor(professor.getProfessorId()) != null) {
                throw new PreexistingEntityException("Professor " + professor + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Professor professor) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Professor persistentProfessor = em.find(Professor.class, professor.getProfessorId());
            Collection<ProfessorCourse> professorCourseCollectionOld = persistentProfessor.getProfessorCourseCollection();
            Collection<ProfessorCourse> professorCourseCollectionNew = professor.getProfessorCourseCollection();
            List<String> illegalOrphanMessages = null;
            for (ProfessorCourse professorCourseCollectionOldProfessorCourse : professorCourseCollectionOld) {
                if (!professorCourseCollectionNew.contains(professorCourseCollectionOldProfessorCourse)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProfessorCourse " + professorCourseCollectionOldProfessorCourse + " since its professor field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<ProfessorCourse> attachedProfessorCourseCollectionNew = new ArrayList<ProfessorCourse>();
            for (ProfessorCourse professorCourseCollectionNewProfessorCourseToAttach : professorCourseCollectionNew) {
                professorCourseCollectionNewProfessorCourseToAttach = em.getReference(professorCourseCollectionNewProfessorCourseToAttach.getClass(), professorCourseCollectionNewProfessorCourseToAttach.getProfessorCoursePK());
                attachedProfessorCourseCollectionNew.add(professorCourseCollectionNewProfessorCourseToAttach);
            }
            professorCourseCollectionNew = attachedProfessorCourseCollectionNew;
            professor.setProfessorCourseCollection(professorCourseCollectionNew);
            professor = em.merge(professor);
            for (ProfessorCourse professorCourseCollectionNewProfessorCourse : professorCourseCollectionNew) {
                if (!professorCourseCollectionOld.contains(professorCourseCollectionNewProfessorCourse)) {
                    Professor oldProfessorOfProfessorCourseCollectionNewProfessorCourse = professorCourseCollectionNewProfessorCourse.getProfessor();
                    professorCourseCollectionNewProfessorCourse.setProfessor(professor);
                    professorCourseCollectionNewProfessorCourse = em.merge(professorCourseCollectionNewProfessorCourse);
                    if (oldProfessorOfProfessorCourseCollectionNewProfessorCourse != null && !oldProfessorOfProfessorCourseCollectionNewProfessorCourse.equals(professor)) {
                        oldProfessorOfProfessorCourseCollectionNewProfessorCourse.getProfessorCourseCollection().remove(professorCourseCollectionNewProfessorCourse);
                        oldProfessorOfProfessorCourseCollectionNewProfessorCourse = em.merge(oldProfessorOfProfessorCourseCollectionNewProfessorCourse);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = professor.getProfessorId();
                if (findProfessor(id) == null) {
                    throw new NonexistentEntityException("The professor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Professor professor;
            try {
                professor = em.getReference(Professor.class, id);
                professor.getProfessorId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The professor with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<ProfessorCourse> professorCourseCollectionOrphanCheck = professor.getProfessorCourseCollection();
            for (ProfessorCourse professorCourseCollectionOrphanCheckProfessorCourse : professorCourseCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Professor (" + professor + ") cannot be destroyed since the ProfessorCourse " + professorCourseCollectionOrphanCheckProfessorCourse + " in its professorCourseCollection field has a non-nullable professor field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(professor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Professor> findProfessorEntities() {
        return findProfessorEntities(true, -1, -1);
    }

    public List<Professor> findProfessorEntities(int maxResults, int firstResult) {
        return findProfessorEntities(false, maxResults, firstResult);
    }

    private List<Professor> findProfessorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Course> prof = cq.from(Professor.class);
            cq.select(prof);
            cq.orderBy(cb.asc(prof.get(Professor_.lastName.getName())));
            Query q = em.createQuery(cq);
            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Professor findProfessor(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Professor.class, id);
        } finally {
            em.close();
        }
    }

    public int getProfessorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Professor> rt = cq.from(Professor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
