/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Lab;
import java.util.ArrayList;
import java.util.Collection;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.Availability;
import com.scheduler.persistence.LegacyReport;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.exceptions.IllegalOrphanException;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author S519479
 */
public class TimeSlotsJpaController implements Serializable {

    public TimeSlotsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TimeSlots timeSlots) throws PreexistingEntityException, Exception {
        if (timeSlots.getLabCollection() == null) {
            timeSlots.setLabCollection(new ArrayList<Lab>());
        }
        if (timeSlots.getSectionCollection() == null) {
            timeSlots.setSectionCollection(new ArrayList<Section>());
        }
        if (timeSlots.getAvailabilityCollection() == null) {
            timeSlots.setAvailabilityCollection(new ArrayList<Availability>());
        }
        if (timeSlots.getLegacyReportCollection() == null) {
            timeSlots.setLegacyReportCollection(new ArrayList<LegacyReport>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Lab> attachedLabCollection = new ArrayList<Lab>();
            for (Lab labCollectionLabToAttach : timeSlots.getLabCollection()) {
                labCollectionLabToAttach = em.getReference(labCollectionLabToAttach.getClass(), labCollectionLabToAttach.getLabFullId());
                attachedLabCollection.add(labCollectionLabToAttach);
            }
            timeSlots.setLabCollection(attachedLabCollection);
            Collection<Section> attachedSectionCollection = new ArrayList<Section>();
            for (Section sectionCollectionSectionToAttach : timeSlots.getSectionCollection()) {
                sectionCollectionSectionToAttach = em.getReference(sectionCollectionSectionToAttach.getClass(), sectionCollectionSectionToAttach.getSectionFullId());
                attachedSectionCollection.add(sectionCollectionSectionToAttach);
            }
            timeSlots.setSectionCollection(attachedSectionCollection);
            Collection<Availability> attachedAvailabilityCollection = new ArrayList<Availability>();
            for (Availability availabilityCollectionAvailabilityToAttach : timeSlots.getAvailabilityCollection()) {
                availabilityCollectionAvailabilityToAttach = em.getReference(availabilityCollectionAvailabilityToAttach.getClass(), availabilityCollectionAvailabilityToAttach.getAvailabilityPK());
                attachedAvailabilityCollection.add(availabilityCollectionAvailabilityToAttach);
            }
            timeSlots.setAvailabilityCollection(attachedAvailabilityCollection);
            Collection<LegacyReport> attachedLegacyReportCollection = new ArrayList<LegacyReport>();
            for (LegacyReport legacyReportCollectionLegacyReportToAttach : timeSlots.getLegacyReportCollection()) {
                legacyReportCollectionLegacyReportToAttach = em.getReference(legacyReportCollectionLegacyReportToAttach.getClass(), legacyReportCollectionLegacyReportToAttach.getLegacyReportPK());
                attachedLegacyReportCollection.add(legacyReportCollectionLegacyReportToAttach);
            }
            timeSlots.setLegacyReportCollection(attachedLegacyReportCollection);
            em.persist(timeSlots);
            for (Lab labCollectionLab : timeSlots.getLabCollection()) {
                labCollectionLab.getTimeSlotsCollection().add(timeSlots);
                labCollectionLab = em.merge(labCollectionLab);
            }
            for (Section sectionCollectionSection : timeSlots.getSectionCollection()) {
                sectionCollectionSection.getTimeSlotsCollection().add(timeSlots);
                sectionCollectionSection = em.merge(sectionCollectionSection);
            }
            for (Availability availabilityCollectionAvailability : timeSlots.getAvailabilityCollection()) {
                TimeSlots oldTimeSlotsOfAvailabilityCollectionAvailability = availabilityCollectionAvailability.getTimeSlots();
                availabilityCollectionAvailability.setTimeSlots(timeSlots);
                availabilityCollectionAvailability = em.merge(availabilityCollectionAvailability);
                if (oldTimeSlotsOfAvailabilityCollectionAvailability != null) {
                    oldTimeSlotsOfAvailabilityCollectionAvailability.getAvailabilityCollection().remove(availabilityCollectionAvailability);
                    oldTimeSlotsOfAvailabilityCollectionAvailability = em.merge(oldTimeSlotsOfAvailabilityCollectionAvailability);
                }
            }
            for (LegacyReport legacyReportCollectionLegacyReport : timeSlots.getLegacyReportCollection()) {
                TimeSlots oldTimeSlotsOfLegacyReportCollectionLegacyReport = legacyReportCollectionLegacyReport.getTimeSlots();
                legacyReportCollectionLegacyReport.setTimeSlots(timeSlots);
                legacyReportCollectionLegacyReport = em.merge(legacyReportCollectionLegacyReport);
                if (oldTimeSlotsOfLegacyReportCollectionLegacyReport != null) {
                    oldTimeSlotsOfLegacyReportCollectionLegacyReport.getLegacyReportCollection().remove(legacyReportCollectionLegacyReport);
                    oldTimeSlotsOfLegacyReportCollectionLegacyReport = em.merge(oldTimeSlotsOfLegacyReportCollectionLegacyReport);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTimeSlots(timeSlots.getTimeSlotId()) != null) {
                throw new PreexistingEntityException("TimeSlots " + timeSlots + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TimeSlots timeSlots) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TimeSlots persistentTimeSlots = em.find(TimeSlots.class, timeSlots.getTimeSlotId());
            Collection<Lab> labCollectionOld = persistentTimeSlots.getLabCollection();
            Collection<Lab> labCollectionNew = timeSlots.getLabCollection();
            Collection<Section> sectionCollectionOld = persistentTimeSlots.getSectionCollection();
            Collection<Section> sectionCollectionNew = timeSlots.getSectionCollection();
            Collection<Availability> availabilityCollectionOld = persistentTimeSlots.getAvailabilityCollection();
            Collection<Availability> availabilityCollectionNew = timeSlots.getAvailabilityCollection();
            Collection<LegacyReport> legacyReportCollectionOld = persistentTimeSlots.getLegacyReportCollection();
            Collection<LegacyReport> legacyReportCollectionNew = timeSlots.getLegacyReportCollection();
            List<String> illegalOrphanMessages = null;
            for (Availability availabilityCollectionOldAvailability : availabilityCollectionOld) {
                if (!availabilityCollectionNew.contains(availabilityCollectionOldAvailability)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Availability " + availabilityCollectionOldAvailability + " since its timeSlots field is not nullable.");
                }
            }
            for (LegacyReport legacyReportCollectionOldLegacyReport : legacyReportCollectionOld) {
                if (!legacyReportCollectionNew.contains(legacyReportCollectionOldLegacyReport)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LegacyReport " + legacyReportCollectionOldLegacyReport + " since its timeSlots field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Lab> attachedLabCollectionNew = new ArrayList<Lab>();
            for (Lab labCollectionNewLabToAttach : labCollectionNew) {
                labCollectionNewLabToAttach = em.getReference(labCollectionNewLabToAttach.getClass(), labCollectionNewLabToAttach.getLabFullId());
                attachedLabCollectionNew.add(labCollectionNewLabToAttach);
            }
            labCollectionNew = attachedLabCollectionNew;
            timeSlots.setLabCollection(labCollectionNew);
            Collection<Section> attachedSectionCollectionNew = new ArrayList<Section>();
            for (Section sectionCollectionNewSectionToAttach : sectionCollectionNew) {
                sectionCollectionNewSectionToAttach = em.getReference(sectionCollectionNewSectionToAttach.getClass(), sectionCollectionNewSectionToAttach.getSectionFullId());
                attachedSectionCollectionNew.add(sectionCollectionNewSectionToAttach);
            }
            sectionCollectionNew = attachedSectionCollectionNew;
            timeSlots.setSectionCollection(sectionCollectionNew);
            Collection<Availability> attachedAvailabilityCollectionNew = new ArrayList<Availability>();
            for (Availability availabilityCollectionNewAvailabilityToAttach : availabilityCollectionNew) {
                availabilityCollectionNewAvailabilityToAttach = em.getReference(availabilityCollectionNewAvailabilityToAttach.getClass(), availabilityCollectionNewAvailabilityToAttach.getAvailabilityPK());
                attachedAvailabilityCollectionNew.add(availabilityCollectionNewAvailabilityToAttach);
            }
            availabilityCollectionNew = attachedAvailabilityCollectionNew;
            timeSlots.setAvailabilityCollection(availabilityCollectionNew);
            Collection<LegacyReport> attachedLegacyReportCollectionNew = new ArrayList<LegacyReport>();
            for (LegacyReport legacyReportCollectionNewLegacyReportToAttach : legacyReportCollectionNew) {
                legacyReportCollectionNewLegacyReportToAttach = em.getReference(legacyReportCollectionNewLegacyReportToAttach.getClass(), legacyReportCollectionNewLegacyReportToAttach.getLegacyReportPK());
                attachedLegacyReportCollectionNew.add(legacyReportCollectionNewLegacyReportToAttach);
            }
            legacyReportCollectionNew = attachedLegacyReportCollectionNew;
            timeSlots.setLegacyReportCollection(legacyReportCollectionNew);
            timeSlots = em.merge(timeSlots);
            for (Lab labCollectionOldLab : labCollectionOld) {
                if (!labCollectionNew.contains(labCollectionOldLab)) {
                    labCollectionOldLab.getTimeSlotsCollection().remove(timeSlots);
                    labCollectionOldLab = em.merge(labCollectionOldLab);
                }
            }
            for (Lab labCollectionNewLab : labCollectionNew) {
                if (!labCollectionOld.contains(labCollectionNewLab)) {
                    labCollectionNewLab.getTimeSlotsCollection().add(timeSlots);
                    labCollectionNewLab = em.merge(labCollectionNewLab);
                }
            }
            for (Section sectionCollectionOldSection : sectionCollectionOld) {
                if (!sectionCollectionNew.contains(sectionCollectionOldSection)) {
                    sectionCollectionOldSection.getTimeSlotsCollection().remove(timeSlots);
                    sectionCollectionOldSection = em.merge(sectionCollectionOldSection);
                }
            }
            for (Section sectionCollectionNewSection : sectionCollectionNew) {
                if (!sectionCollectionOld.contains(sectionCollectionNewSection)) {
                    sectionCollectionNewSection.getTimeSlotsCollection().add(timeSlots);
                    sectionCollectionNewSection = em.merge(sectionCollectionNewSection);
                }
            }
            for (Availability availabilityCollectionNewAvailability : availabilityCollectionNew) {
                if (!availabilityCollectionOld.contains(availabilityCollectionNewAvailability)) {
                    TimeSlots oldTimeSlotsOfAvailabilityCollectionNewAvailability = availabilityCollectionNewAvailability.getTimeSlots();
                    availabilityCollectionNewAvailability.setTimeSlots(timeSlots);
                    availabilityCollectionNewAvailability = em.merge(availabilityCollectionNewAvailability);
                    if (oldTimeSlotsOfAvailabilityCollectionNewAvailability != null && !oldTimeSlotsOfAvailabilityCollectionNewAvailability.equals(timeSlots)) {
                        oldTimeSlotsOfAvailabilityCollectionNewAvailability.getAvailabilityCollection().remove(availabilityCollectionNewAvailability);
                        oldTimeSlotsOfAvailabilityCollectionNewAvailability = em.merge(oldTimeSlotsOfAvailabilityCollectionNewAvailability);
                    }
                }
            }
            for (LegacyReport legacyReportCollectionNewLegacyReport : legacyReportCollectionNew) {
                if (!legacyReportCollectionOld.contains(legacyReportCollectionNewLegacyReport)) {
                    TimeSlots oldTimeSlotsOfLegacyReportCollectionNewLegacyReport = legacyReportCollectionNewLegacyReport.getTimeSlots();
                    legacyReportCollectionNewLegacyReport.setTimeSlots(timeSlots);
                    legacyReportCollectionNewLegacyReport = em.merge(legacyReportCollectionNewLegacyReport);
                    if (oldTimeSlotsOfLegacyReportCollectionNewLegacyReport != null && !oldTimeSlotsOfLegacyReportCollectionNewLegacyReport.equals(timeSlots)) {
                        oldTimeSlotsOfLegacyReportCollectionNewLegacyReport.getLegacyReportCollection().remove(legacyReportCollectionNewLegacyReport);
                        oldTimeSlotsOfLegacyReportCollectionNewLegacyReport = em.merge(oldTimeSlotsOfLegacyReportCollectionNewLegacyReport);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = timeSlots.getTimeSlotId();
                if (findTimeSlots(id) == null) {
                    throw new NonexistentEntityException("The timeSlots with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TimeSlots timeSlots;
            try {
                timeSlots = em.getReference(TimeSlots.class, id);
                timeSlots.getTimeSlotId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The timeSlots with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Availability> availabilityCollectionOrphanCheck = timeSlots.getAvailabilityCollection();
            for (Availability availabilityCollectionOrphanCheckAvailability : availabilityCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TimeSlots (" + timeSlots + ") cannot be destroyed since the Availability " + availabilityCollectionOrphanCheckAvailability + " in its availabilityCollection field has a non-nullable timeSlots field.");
            }
            Collection<LegacyReport> legacyReportCollectionOrphanCheck = timeSlots.getLegacyReportCollection();
            for (LegacyReport legacyReportCollectionOrphanCheckLegacyReport : legacyReportCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TimeSlots (" + timeSlots + ") cannot be destroyed since the LegacyReport " + legacyReportCollectionOrphanCheckLegacyReport + " in its legacyReportCollection field has a non-nullable timeSlots field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Lab> labCollection = timeSlots.getLabCollection();
            for (Lab labCollectionLab : labCollection) {
                labCollectionLab.getTimeSlotsCollection().remove(timeSlots);
                labCollectionLab = em.merge(labCollectionLab);
            }
            Collection<Section> sectionCollection = timeSlots.getSectionCollection();
            for (Section sectionCollectionSection : sectionCollection) {
                sectionCollectionSection.getTimeSlotsCollection().remove(timeSlots);
                sectionCollectionSection = em.merge(sectionCollectionSection);
            }
            em.remove(timeSlots);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TimeSlots> findTimeSlotsEntities() {
        return findTimeSlotsEntities(true, -1, -1);
    }

    public List<TimeSlots> findTimeSlotsEntities(int maxResults, int firstResult) {
        return findTimeSlotsEntities(false, maxResults, firstResult);
    }

    private List<TimeSlots> findTimeSlotsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TimeSlots.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TimeSlots findTimeSlots(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TimeSlots.class, id);
        } finally {
            em.close();
        }
    }

    public int getTimeSlotsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TimeSlots> rt = cq.from(TimeSlots.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
