/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Course;
import com.scheduler.persistence.Department;
import com.scheduler.persistence.Department_;
import com.scheduler.persistence.controller.exceptions.IllegalOrphanException;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author S519479
 */
public class DepartmentJpaController implements Serializable {

    public DepartmentJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Department department) throws PreexistingEntityException, Exception {
        if (department.getCourseCollection() == null) {
            department.setCourseCollection(new ArrayList<Course>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Course> attachedCourseCollection = new ArrayList<Course>();
            for (Course courseCollectionCourseToAttach : department.getCourseCollection()) {
                courseCollectionCourseToAttach = em.getReference(courseCollectionCourseToAttach.getClass(), courseCollectionCourseToAttach.getCoursePK());
                attachedCourseCollection.add(courseCollectionCourseToAttach);
            }
            department.setCourseCollection(attachedCourseCollection);
            em.persist(department);
            for (Course courseCollectionCourse : department.getCourseCollection()) {
                Department oldDepartmentOfCourseCollectionCourse = courseCollectionCourse.getDepartment();
                courseCollectionCourse.setDepartment(department);
                courseCollectionCourse = em.merge(courseCollectionCourse);
                if (oldDepartmentOfCourseCollectionCourse != null) {
                    oldDepartmentOfCourseCollectionCourse.getCourseCollection().remove(courseCollectionCourse);
                    oldDepartmentOfCourseCollectionCourse = em.merge(oldDepartmentOfCourseCollectionCourse);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDepartment(department.getDepartmentId()) != null) {
                throw new PreexistingEntityException("Department " + department + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Department department) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Department persistentDepartment = em.find(Department.class, department.getDepartmentId());
            Collection<Course> courseCollectionOld = persistentDepartment.getCourseCollection();
            Collection<Course> courseCollectionNew = department.getCourseCollection();
            List<String> illegalOrphanMessages = null;
            for (Course courseCollectionOldCourse : courseCollectionOld) {
                if (!courseCollectionNew.contains(courseCollectionOldCourse)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Course " + courseCollectionOldCourse + " since its department field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Course> attachedCourseCollectionNew = new ArrayList<Course>();
            for (Course courseCollectionNewCourseToAttach : courseCollectionNew) {
                courseCollectionNewCourseToAttach = em.getReference(courseCollectionNewCourseToAttach.getClass(), courseCollectionNewCourseToAttach.getCoursePK());
                attachedCourseCollectionNew.add(courseCollectionNewCourseToAttach);
            }
            courseCollectionNew = attachedCourseCollectionNew;
            department.setCourseCollection(courseCollectionNew);
            department = em.merge(department);
            for (Course courseCollectionNewCourse : courseCollectionNew) {
                if (!courseCollectionOld.contains(courseCollectionNewCourse)) {
                    Department oldDepartmentOfCourseCollectionNewCourse = courseCollectionNewCourse.getDepartment();
                    courseCollectionNewCourse.setDepartment(department);
                    courseCollectionNewCourse = em.merge(courseCollectionNewCourse);
                    if (oldDepartmentOfCourseCollectionNewCourse != null && !oldDepartmentOfCourseCollectionNewCourse.equals(department)) {
                        oldDepartmentOfCourseCollectionNewCourse.getCourseCollection().remove(courseCollectionNewCourse);
                        oldDepartmentOfCourseCollectionNewCourse = em.merge(oldDepartmentOfCourseCollectionNewCourse);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = department.getDepartmentId();
                if (findDepartment(id) == null) {
                    throw new NonexistentEntityException("The department with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Department department;
            try {
                department = em.getReference(Department.class, id);
                department.getDepartmentId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The department with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Course> courseCollectionOrphanCheck = department.getCourseCollection();
            for (Course courseCollectionOrphanCheckCourse : courseCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Department (" + department + ") cannot be destroyed since the Course " + courseCollectionOrphanCheckCourse + " in its courseCollection field has a non-nullable department field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(department);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Department> findDepartmentEntities() {
        return findDepartmentEntities(true, -1, -1);
    }

    public List<Department> findDepartmentEntities(int maxResults, int firstResult) {
        return findDepartmentEntities(false, maxResults, firstResult);
    }

    private List<Department> findDepartmentEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Course> dept = cq.from(Department.class);
            cq.select(dept);
            cq.orderBy(cb.asc(dept.get(Department_.departmentName.getName())));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Department findDepartment(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Department.class, id);
        } finally {
            em.close();
        }
    }

    public int getDepartmentCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Department> rt = cq.from(Department.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
