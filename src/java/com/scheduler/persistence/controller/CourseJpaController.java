/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import com.scheduler.persistence.Course;
import com.scheduler.persistence.CoursePK;
import com.scheduler.persistence.Course_;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Department;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.controller.exceptions.IllegalOrphanException;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author S519479
 */
public class CourseJpaController implements Serializable {

    public CourseJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Course course) throws PreexistingEntityException, Exception {
        if (course.getCoursePK() == null) {
            course.setCoursePK(new CoursePK());
        }
        if (course.getProfessorCourseCollection() == null) {
            course.setProfessorCourseCollection(new ArrayList<ProfessorCourse>());
        }
        course.getCoursePK().setDepartmentId(course.getDepartment().getDepartmentId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Department department = course.getDepartment();
            if (department != null) {
                department = em.getReference(department.getClass(), department.getDepartmentId());
                course.setDepartment(department);
            }
            Collection<ProfessorCourse> attachedProfessorCourseCollection = new ArrayList<ProfessorCourse>();
            for (ProfessorCourse professorCourseCollectionProfessorCourseToAttach : course.getProfessorCourseCollection()) {
                professorCourseCollectionProfessorCourseToAttach = em.getReference(professorCourseCollectionProfessorCourseToAttach.getClass(), professorCourseCollectionProfessorCourseToAttach.getProfessorCoursePK());
                attachedProfessorCourseCollection.add(professorCourseCollectionProfessorCourseToAttach);
            }
            course.setProfessorCourseCollection(attachedProfessorCourseCollection);
            em.persist(course);
            if (department != null) {
                department.getCourseCollection().add(course);
                department = em.merge(department);
            }
            for (ProfessorCourse professorCourseCollectionProfessorCourse : course.getProfessorCourseCollection()) {
                Course oldCourseOfProfessorCourseCollectionProfessorCourse = professorCourseCollectionProfessorCourse.getCourse();
                professorCourseCollectionProfessorCourse.setCourse(course);
                professorCourseCollectionProfessorCourse = em.merge(professorCourseCollectionProfessorCourse);
                if (oldCourseOfProfessorCourseCollectionProfessorCourse != null) {
                    oldCourseOfProfessorCourseCollectionProfessorCourse.getProfessorCourseCollection().remove(professorCourseCollectionProfessorCourse);
                    oldCourseOfProfessorCourseCollectionProfessorCourse = em.merge(oldCourseOfProfessorCourseCollectionProfessorCourse);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCourse(course.getCoursePK()) != null) {
                throw new PreexistingEntityException("Course " + course + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Course course) throws IllegalOrphanException, NonexistentEntityException, Exception {
        course.getCoursePK().setDepartmentId(course.getDepartment().getDepartmentId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Course persistentCourse = em.find(Course.class, course.getCoursePK());
            Department departmentOld = persistentCourse.getDepartment();
            Department departmentNew = course.getDepartment();
            Collection<ProfessorCourse> professorCourseCollectionOld = persistentCourse.getProfessorCourseCollection();
            Collection<ProfessorCourse> professorCourseCollectionNew = course.getProfessorCourseCollection();
            List<String> illegalOrphanMessages = null;
            for (ProfessorCourse professorCourseCollectionOldProfessorCourse : professorCourseCollectionOld) {
                if (!professorCourseCollectionNew.contains(professorCourseCollectionOldProfessorCourse)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProfessorCourse " + professorCourseCollectionOldProfessorCourse + " since its course field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (departmentNew != null) {
                departmentNew = em.getReference(departmentNew.getClass(), departmentNew.getDepartmentId());
                course.setDepartment(departmentNew);
            }
            Collection<ProfessorCourse> attachedProfessorCourseCollectionNew = new ArrayList<ProfessorCourse>();
            for (ProfessorCourse professorCourseCollectionNewProfessorCourseToAttach : professorCourseCollectionNew) {
                professorCourseCollectionNewProfessorCourseToAttach = em.getReference(professorCourseCollectionNewProfessorCourseToAttach.getClass(), professorCourseCollectionNewProfessorCourseToAttach.getProfessorCoursePK());
                attachedProfessorCourseCollectionNew.add(professorCourseCollectionNewProfessorCourseToAttach);
            }
            professorCourseCollectionNew = attachedProfessorCourseCollectionNew;
            course.setProfessorCourseCollection(professorCourseCollectionNew);
            course = em.merge(course);
            if (departmentOld != null && !departmentOld.equals(departmentNew)) {
                departmentOld.getCourseCollection().remove(course);
                departmentOld = em.merge(departmentOld);
            }
            if (departmentNew != null && !departmentNew.equals(departmentOld)) {
                departmentNew.getCourseCollection().add(course);
                departmentNew = em.merge(departmentNew);
            }
            for (ProfessorCourse professorCourseCollectionNewProfessorCourse : professorCourseCollectionNew) {
                if (!professorCourseCollectionOld.contains(professorCourseCollectionNewProfessorCourse)) {
                    Course oldCourseOfProfessorCourseCollectionNewProfessorCourse = professorCourseCollectionNewProfessorCourse.getCourse();
                    professorCourseCollectionNewProfessorCourse.setCourse(course);
                    professorCourseCollectionNewProfessorCourse = em.merge(professorCourseCollectionNewProfessorCourse);
                    if (oldCourseOfProfessorCourseCollectionNewProfessorCourse != null && !oldCourseOfProfessorCourseCollectionNewProfessorCourse.equals(course)) {
                        oldCourseOfProfessorCourseCollectionNewProfessorCourse.getProfessorCourseCollection().remove(professorCourseCollectionNewProfessorCourse);
                        oldCourseOfProfessorCourseCollectionNewProfessorCourse = em.merge(oldCourseOfProfessorCourseCollectionNewProfessorCourse);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                CoursePK id = course.getCoursePK();
                if (findCourse(id) == null) {
                    throw new NonexistentEntityException("The course with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(CoursePK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Course course;
            try {
                course = em.getReference(Course.class, id);
                course.getCoursePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The course with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<ProfessorCourse> professorCourseCollectionOrphanCheck = course.getProfessorCourseCollection();
            for (ProfessorCourse professorCourseCollectionOrphanCheckProfessorCourse : professorCourseCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Course (" + course + ") cannot be destroyed since the ProfessorCourse " + professorCourseCollectionOrphanCheckProfessorCourse + " in its professorCourseCollection field has a non-nullable course field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Department department = course.getDepartment();
            if (department != null) {
                department.getCourseCollection().remove(course);
                department = em.merge(department);
            }
            em.remove(course);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Course> findCourseEntities() {
        return findCourseEntities(true, -1, -1);
    }
    public List<Course> findCourseEntitiesSortedByCourse() {
        return findCourseEntities(true, -1, -1);
    }

    public List<Course> findCourseEntities(int maxResults, int firstResult) {
        return findCourseEntities(false, maxResults, firstResult);
    }

    private List<Course> findCourseEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Course> course = cq.from(Course.class);
            cq.select(course);
            cq.orderBy(cb.asc(course.get(Course_.department)),cb.asc(course.get(Course_.coursePK)));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    private List<Course> findCourseEntitiesSortedByCourse(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Course> course = cq.from(Course.class);
            cq.select(course);
            cq.orderBy(cb.asc(course.get(Course_.coursePK)));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Course findCourse(CoursePK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Course.class, id);
        } finally {
            em.close();
        }
    }

    public int getCourseCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Course> rt = cq.from(Course.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
