/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import com.scheduler.persistence.LegacyReport;
import com.scheduler.persistence.LegacyReportPK;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author S519479
 */
public class LegacyReportJpaController implements Serializable {

    public LegacyReportJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LegacyReport legacyReport) throws PreexistingEntityException, Exception {
        if (legacyReport.getLegacyReportPK() == null) {
            legacyReport.setLegacyReportPK(new LegacyReportPK());
        }
        legacyReport.getLegacyReportPK().setTimeSlotId(legacyReport.getTimeSlots().getTimeSlotId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TimeSlots timeSlots = legacyReport.getTimeSlots();
            if (timeSlots != null) {
                timeSlots = em.getReference(timeSlots.getClass(), timeSlots.getTimeSlotId());
                legacyReport.setTimeSlots(timeSlots);
            }
            em.persist(legacyReport);
            if (timeSlots != null) {
                timeSlots.getLegacyReportCollection().add(legacyReport);
                timeSlots = em.merge(timeSlots);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLegacyReport(legacyReport.getLegacyReportPK()) != null) {
                throw new PreexistingEntityException("LegacyReport " + legacyReport + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LegacyReport legacyReport) throws NonexistentEntityException, Exception {
        legacyReport.getLegacyReportPK().setTimeSlotId(legacyReport.getTimeSlots().getTimeSlotId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LegacyReport persistentLegacyReport = em.find(LegacyReport.class, legacyReport.getLegacyReportPK());
            TimeSlots timeSlotsOld = persistentLegacyReport.getTimeSlots();
            TimeSlots timeSlotsNew = legacyReport.getTimeSlots();
            if (timeSlotsNew != null) {
                timeSlotsNew = em.getReference(timeSlotsNew.getClass(), timeSlotsNew.getTimeSlotId());
                legacyReport.setTimeSlots(timeSlotsNew);
            }
            legacyReport = em.merge(legacyReport);
            if (timeSlotsOld != null && !timeSlotsOld.equals(timeSlotsNew)) {
                timeSlotsOld.getLegacyReportCollection().remove(legacyReport);
                timeSlotsOld = em.merge(timeSlotsOld);
            }
            if (timeSlotsNew != null && !timeSlotsNew.equals(timeSlotsOld)) {
                timeSlotsNew.getLegacyReportCollection().add(legacyReport);
                timeSlotsNew = em.merge(timeSlotsNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                LegacyReportPK id = legacyReport.getLegacyReportPK();
                if (findLegacyReport(id) == null) {
                    throw new NonexistentEntityException("The legacyReport with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(LegacyReportPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LegacyReport legacyReport;
            try {
                legacyReport = em.getReference(LegacyReport.class, id);
                legacyReport.getLegacyReportPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The legacyReport with id " + id + " no longer exists.", enfe);
            }
            TimeSlots timeSlots = legacyReport.getTimeSlots();
            if (timeSlots != null) {
                timeSlots.getLegacyReportCollection().remove(legacyReport);
                timeSlots = em.merge(timeSlots);
            }
            em.remove(legacyReport);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LegacyReport> findLegacyReportEntities() {
        return findLegacyReportEntities(true, -1, -1);
    }

    public List<LegacyReport> findLegacyReportEntities(int maxResults, int firstResult) {
        return findLegacyReportEntities(false, maxResults, firstResult);
    }

    private List<LegacyReport> findLegacyReportEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LegacyReport.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LegacyReport findLegacyReport(LegacyReportPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LegacyReport.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<LegacyReport> findLegacyReportYearSemesterReportName(int year, String semester, String reportName) {

        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("LegacyReport.findByYearSemesterReportName", LegacyReport.class).setParameter("year", year).setParameter("semester", semester).setParameter("reportName", reportName).getResultList();
        } finally {
            em.close();
        }
    }

    public int getLegacyReportCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LegacyReport> rt = cq.from(LegacyReport.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
