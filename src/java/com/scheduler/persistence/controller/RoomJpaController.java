/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Building;
import com.scheduler.persistence.Section;
import java.util.ArrayList;
import java.util.Collection;
import com.scheduler.persistence.Availability;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.RoomPK;
import com.scheduler.persistence.controller.exceptions.IllegalOrphanException;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author S519479
 */
public class RoomJpaController implements Serializable {

    public RoomJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Room room) throws PreexistingEntityException, Exception {
        if (room.getRoomPK() == null) {
            room.setRoomPK(new RoomPK());
        }
        if (room.getSectionCollection() == null) {
            room.setSectionCollection(new ArrayList<Section>());
        }
        if (room.getAvailabilityCollection() == null) {
            room.setAvailabilityCollection(new ArrayList<Availability>());
        }
        if (room.getLabCollection() == null) {
            room.setLabCollection(new ArrayList<Lab>());
        }
        room.getRoomPK().setBuildingId(room.getBuilding().getBuildingId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Building building = room.getBuilding();
            if (building != null) {
                building = em.getReference(building.getClass(), building.getBuildingId());
                room.setBuilding(building);
            }
            Collection<Section> attachedSectionCollection = new ArrayList<Section>();
            for (Section sectionCollectionSectionToAttach : room.getSectionCollection()) {
                sectionCollectionSectionToAttach = em.getReference(sectionCollectionSectionToAttach.getClass(), sectionCollectionSectionToAttach.getSectionFullId());
                attachedSectionCollection.add(sectionCollectionSectionToAttach);
            }
            room.setSectionCollection(attachedSectionCollection);
            Collection<Availability> attachedAvailabilityCollection = new ArrayList<Availability>();
            for (Availability availabilityCollectionAvailabilityToAttach : room.getAvailabilityCollection()) {
                availabilityCollectionAvailabilityToAttach = em.getReference(availabilityCollectionAvailabilityToAttach.getClass(), availabilityCollectionAvailabilityToAttach.getAvailabilityPK());
                attachedAvailabilityCollection.add(availabilityCollectionAvailabilityToAttach);
            }
            room.setAvailabilityCollection(attachedAvailabilityCollection);
            Collection<Lab> attachedLabCollection = new ArrayList<Lab>();
            for (Lab labCollectionLabToAttach : room.getLabCollection()) {
                labCollectionLabToAttach = em.getReference(labCollectionLabToAttach.getClass(), labCollectionLabToAttach.getLabFullId());
                attachedLabCollection.add(labCollectionLabToAttach);
            }
            room.setLabCollection(attachedLabCollection);
            em.persist(room);
            if (building != null) {
                building.getRoomCollection().add(room);
                building = em.merge(building);
            }
            for (Section sectionCollectionSection : room.getSectionCollection()) {
                Room oldRoomOfSectionCollectionSection = sectionCollectionSection.getRoom();
                sectionCollectionSection.setRoom(room);
                sectionCollectionSection = em.merge(sectionCollectionSection);
                if (oldRoomOfSectionCollectionSection != null) {
                    oldRoomOfSectionCollectionSection.getSectionCollection().remove(sectionCollectionSection);
                    oldRoomOfSectionCollectionSection = em.merge(oldRoomOfSectionCollectionSection);
                }
            }
            for (Availability availabilityCollectionAvailability : room.getAvailabilityCollection()) {
                Room oldRoomOfAvailabilityCollectionAvailability = availabilityCollectionAvailability.getRoom();
                availabilityCollectionAvailability.setRoom(room);
                availabilityCollectionAvailability = em.merge(availabilityCollectionAvailability);
                if (oldRoomOfAvailabilityCollectionAvailability != null) {
                    oldRoomOfAvailabilityCollectionAvailability.getAvailabilityCollection().remove(availabilityCollectionAvailability);
                    oldRoomOfAvailabilityCollectionAvailability = em.merge(oldRoomOfAvailabilityCollectionAvailability);
                }
            }
            for (Lab labCollectionLab : room.getLabCollection()) {
                Room oldRoomOfLabCollectionLab = labCollectionLab.getRoom();
                labCollectionLab.setRoom(room);
                labCollectionLab = em.merge(labCollectionLab);
                if (oldRoomOfLabCollectionLab != null) {
                    oldRoomOfLabCollectionLab.getLabCollection().remove(labCollectionLab);
                    oldRoomOfLabCollectionLab = em.merge(oldRoomOfLabCollectionLab);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRoom(room.getRoomPK()) != null) {
                throw new PreexistingEntityException("Room " + room + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Room room) throws IllegalOrphanException, NonexistentEntityException, Exception {
        room.getRoomPK().setBuildingId(room.getBuilding().getBuildingId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Room persistentRoom = em.find(Room.class, room.getRoomPK());
            Building buildingOld = persistentRoom.getBuilding();
            Building buildingNew = room.getBuilding();
            Collection<Section> sectionCollectionOld = persistentRoom.getSectionCollection();
            Collection<Section> sectionCollectionNew = room.getSectionCollection();
            Collection<Availability> availabilityCollectionOld = persistentRoom.getAvailabilityCollection();
            Collection<Availability> availabilityCollectionNew = room.getAvailabilityCollection();
            Collection<Lab> labCollectionOld = persistentRoom.getLabCollection();
            Collection<Lab> labCollectionNew = room.getLabCollection();
            List<String> illegalOrphanMessages = null;
            for (Availability availabilityCollectionOldAvailability : availabilityCollectionOld) {
                if (!availabilityCollectionNew.contains(availabilityCollectionOldAvailability)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Availability " + availabilityCollectionOldAvailability + " since its room field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (buildingNew != null) {
                buildingNew = em.getReference(buildingNew.getClass(), buildingNew.getBuildingId());
                room.setBuilding(buildingNew);
            }
            Collection<Section> attachedSectionCollectionNew = new ArrayList<Section>();
            for (Section sectionCollectionNewSectionToAttach : sectionCollectionNew) {
                sectionCollectionNewSectionToAttach = em.getReference(sectionCollectionNewSectionToAttach.getClass(), sectionCollectionNewSectionToAttach.getSectionFullId());
                attachedSectionCollectionNew.add(sectionCollectionNewSectionToAttach);
            }
            sectionCollectionNew = attachedSectionCollectionNew;
            room.setSectionCollection(sectionCollectionNew);
            Collection<Availability> attachedAvailabilityCollectionNew = new ArrayList<Availability>();
            for (Availability availabilityCollectionNewAvailabilityToAttach : availabilityCollectionNew) {
                availabilityCollectionNewAvailabilityToAttach = em.getReference(availabilityCollectionNewAvailabilityToAttach.getClass(), availabilityCollectionNewAvailabilityToAttach.getAvailabilityPK());
                attachedAvailabilityCollectionNew.add(availabilityCollectionNewAvailabilityToAttach);
            }
            availabilityCollectionNew = attachedAvailabilityCollectionNew;
            room.setAvailabilityCollection(availabilityCollectionNew);
            Collection<Lab> attachedLabCollectionNew = new ArrayList<Lab>();
            for (Lab labCollectionNewLabToAttach : labCollectionNew) {
                labCollectionNewLabToAttach = em.getReference(labCollectionNewLabToAttach.getClass(), labCollectionNewLabToAttach.getLabFullId());
                attachedLabCollectionNew.add(labCollectionNewLabToAttach);
            }
            labCollectionNew = attachedLabCollectionNew;
            room.setLabCollection(labCollectionNew);
            room = em.merge(room);
            if (buildingOld != null && !buildingOld.equals(buildingNew)) {
                buildingOld.getRoomCollection().remove(room);
                buildingOld = em.merge(buildingOld);
            }
            if (buildingNew != null && !buildingNew.equals(buildingOld)) {
                buildingNew.getRoomCollection().add(room);
                buildingNew = em.merge(buildingNew);
            }
            for (Section sectionCollectionOldSection : sectionCollectionOld) {
                if (!sectionCollectionNew.contains(sectionCollectionOldSection)) {
                    sectionCollectionOldSection.setRoom(null);
                    sectionCollectionOldSection = em.merge(sectionCollectionOldSection);
                }
            }
            for (Section sectionCollectionNewSection : sectionCollectionNew) {
                if (!sectionCollectionOld.contains(sectionCollectionNewSection)) {
                    Room oldRoomOfSectionCollectionNewSection = sectionCollectionNewSection.getRoom();
                    sectionCollectionNewSection.setRoom(room);
                    sectionCollectionNewSection = em.merge(sectionCollectionNewSection);
                    if (oldRoomOfSectionCollectionNewSection != null && !oldRoomOfSectionCollectionNewSection.equals(room)) {
                        oldRoomOfSectionCollectionNewSection.getSectionCollection().remove(sectionCollectionNewSection);
                        oldRoomOfSectionCollectionNewSection = em.merge(oldRoomOfSectionCollectionNewSection);
                    }
                }
            }
            for (Availability availabilityCollectionNewAvailability : availabilityCollectionNew) {
                if (!availabilityCollectionOld.contains(availabilityCollectionNewAvailability)) {
                    Room oldRoomOfAvailabilityCollectionNewAvailability = availabilityCollectionNewAvailability.getRoom();
                    availabilityCollectionNewAvailability.setRoom(room);
                    availabilityCollectionNewAvailability = em.merge(availabilityCollectionNewAvailability);
                    if (oldRoomOfAvailabilityCollectionNewAvailability != null && !oldRoomOfAvailabilityCollectionNewAvailability.equals(room)) {
                        oldRoomOfAvailabilityCollectionNewAvailability.getAvailabilityCollection().remove(availabilityCollectionNewAvailability);
                        oldRoomOfAvailabilityCollectionNewAvailability = em.merge(oldRoomOfAvailabilityCollectionNewAvailability);
                    }
                }
            }
            for (Lab labCollectionOldLab : labCollectionOld) {
                if (!labCollectionNew.contains(labCollectionOldLab)) {
                    labCollectionOldLab.setRoom(null);
                    labCollectionOldLab = em.merge(labCollectionOldLab);
                }
            }
            for (Lab labCollectionNewLab : labCollectionNew) {
                if (!labCollectionOld.contains(labCollectionNewLab)) {
                    Room oldRoomOfLabCollectionNewLab = labCollectionNewLab.getRoom();
                    labCollectionNewLab.setRoom(room);
                    labCollectionNewLab = em.merge(labCollectionNewLab);
                    if (oldRoomOfLabCollectionNewLab != null && !oldRoomOfLabCollectionNewLab.equals(room)) {
                        oldRoomOfLabCollectionNewLab.getLabCollection().remove(labCollectionNewLab);
                        oldRoomOfLabCollectionNewLab = em.merge(oldRoomOfLabCollectionNewLab);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                RoomPK id = room.getRoomPK();
                if (findRoom(id) == null) {
                    throw new NonexistentEntityException("The room with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(RoomPK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Room room;
            try {
                room = em.getReference(Room.class, id);
                room.getRoomPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The room with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Availability> availabilityCollectionOrphanCheck = room.getAvailabilityCollection();
            for (Availability availabilityCollectionOrphanCheckAvailability : availabilityCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Room (" + room + ") cannot be destroyed since the Availability " + availabilityCollectionOrphanCheckAvailability + " in its availabilityCollection field has a non-nullable room field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Building building = room.getBuilding();
            if (building != null) {
                building.getRoomCollection().remove(room);
                building = em.merge(building);
            }
            Collection<Section> sectionCollection = room.getSectionCollection();
            for (Section sectionCollectionSection : sectionCollection) {
                sectionCollectionSection.setRoom(null);
                sectionCollectionSection = em.merge(sectionCollectionSection);
            }
            Collection<Lab> labCollection = room.getLabCollection();
            for (Lab labCollectionLab : labCollection) {
                labCollectionLab.setRoom(null);
                labCollectionLab = em.merge(labCollectionLab);
            }
            em.remove(room);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Room> findRoomEntities() {
        return findRoomEntities(true, -1, -1);
    }

    public List<Room> findRoomEntities(int maxResults, int firstResult) {
        return findRoomEntities(false, maxResults, firstResult);
    }

    private List<Room> findRoomEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Room.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Room findRoom(RoomPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Room.class, id);
        } finally {
            em.close();
        }
    }

    public int getRoomCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Room> rt = cq.from(Room.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
