/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Course;
import com.scheduler.persistence.Professor;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.ProfessorCoursePK;
import com.scheduler.persistence.ProfessorCourse_;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author S519479
 */
public class ProfessorCourseJpaController implements Serializable {

    public ProfessorCourseJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProfessorCourse professorCourse) throws PreexistingEntityException, Exception {
        if (professorCourse.getProfessorCoursePK() == null) {
            professorCourse.setProfessorCoursePK(new ProfessorCoursePK());
        }
        if (professorCourse.getSectionCollection() == null) {
            professorCourse.setSectionCollection(new ArrayList<Section>());
        }
        professorCourse.getProfessorCoursePK().setDepartrmentId(professorCourse.getCourse().getCoursePK().getDepartmentId());
        professorCourse.getProfessorCoursePK().setProfessorId(professorCourse.getProfessor().getProfessorId());
        professorCourse.getProfessorCoursePK().setCourseId(professorCourse.getCourse().getCoursePK().getCourseId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Course course = professorCourse.getCourse();
            if (course != null) {
                course = em.getReference(course.getClass(), course.getCoursePK());
                professorCourse.setCourse(course);
            }
            Professor professor = professorCourse.getProfessor();
            if (professor != null) {
                professor = em.getReference(professor.getClass(), professor.getProfessorId());
                professorCourse.setProfessor(professor);
            }
            Collection<Section> attachedSectionCollection = new ArrayList<Section>();
            for (Section sectionCollectionSectionToAttach : professorCourse.getSectionCollection()) {
                sectionCollectionSectionToAttach = em.getReference(sectionCollectionSectionToAttach.getClass(), sectionCollectionSectionToAttach.getSectionFullId());
                attachedSectionCollection.add(sectionCollectionSectionToAttach);
            }
            professorCourse.setSectionCollection(attachedSectionCollection);
            em.persist(professorCourse);
            if (course != null) {
                course.getProfessorCourseCollection().add(professorCourse);
                course = em.merge(course);
            }
            if (professor != null) {
                professor.getProfessorCourseCollection().add(professorCourse);
                professor = em.merge(professor);
            }
            for (Section sectionCollectionSection : professorCourse.getSectionCollection()) {
                ProfessorCourse oldProfessorCourseOfSectionCollectionSection = sectionCollectionSection.getProfessorCourse();
                sectionCollectionSection.setProfessorCourse(professorCourse);
                sectionCollectionSection = em.merge(sectionCollectionSection);
                if (oldProfessorCourseOfSectionCollectionSection != null) {
                    oldProfessorCourseOfSectionCollectionSection.getSectionCollection().remove(sectionCollectionSection);
                    oldProfessorCourseOfSectionCollectionSection = em.merge(oldProfessorCourseOfSectionCollectionSection);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProfessorCourse(professorCourse.getProfessorCoursePK()) != null) {
                throw new PreexistingEntityException("ProfessorCourse " + professorCourse + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProfessorCourse professorCourse) throws NonexistentEntityException, Exception {
        professorCourse.getProfessorCoursePK().setDepartrmentId(professorCourse.getCourse().getCoursePK().getDepartmentId());
        professorCourse.getProfessorCoursePK().setProfessorId(professorCourse.getProfessor().getProfessorId());
        professorCourse.getProfessorCoursePK().setCourseId(professorCourse.getCourse().getCoursePK().getCourseId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProfessorCourse persistentProfessorCourse = em.find(ProfessorCourse.class, professorCourse.getProfessorCoursePK());
            Course courseOld = persistentProfessorCourse.getCourse();
            Course courseNew = professorCourse.getCourse();
            Professor professorOld = persistentProfessorCourse.getProfessor();
            Professor professorNew = professorCourse.getProfessor();
            Collection<Section> sectionCollectionOld = persistentProfessorCourse.getSectionCollection();
            Collection<Section> sectionCollectionNew = professorCourse.getSectionCollection();
            if (courseNew != null) {
                courseNew = em.getReference(courseNew.getClass(), courseNew.getCoursePK());
                professorCourse.setCourse(courseNew);
            }
            if (professorNew != null) {
                professorNew = em.getReference(professorNew.getClass(), professorNew.getProfessorId());
                professorCourse.setProfessor(professorNew);
            }
            Collection<Section> attachedSectionCollectionNew = new ArrayList<Section>();
            for (Section sectionCollectionNewSectionToAttach : sectionCollectionNew) {
                sectionCollectionNewSectionToAttach = em.getReference(sectionCollectionNewSectionToAttach.getClass(), sectionCollectionNewSectionToAttach.getSectionFullId());
                attachedSectionCollectionNew.add(sectionCollectionNewSectionToAttach);
            }
            sectionCollectionNew = attachedSectionCollectionNew;
            professorCourse.setSectionCollection(sectionCollectionNew);
            professorCourse = em.merge(professorCourse);
            if (courseOld != null && !courseOld.equals(courseNew)) {
                courseOld.getProfessorCourseCollection().remove(professorCourse);
                courseOld = em.merge(courseOld);
            }
            if (courseNew != null && !courseNew.equals(courseOld)) {
                courseNew.getProfessorCourseCollection().add(professorCourse);
                courseNew = em.merge(courseNew);
            }
            if (professorOld != null && !professorOld.equals(professorNew)) {
                professorOld.getProfessorCourseCollection().remove(professorCourse);
                professorOld = em.merge(professorOld);
            }
            if (professorNew != null && !professorNew.equals(professorOld)) {
                professorNew.getProfessorCourseCollection().add(professorCourse);
                professorNew = em.merge(professorNew);
            }
            for (Section sectionCollectionOldSection : sectionCollectionOld) {
                if (!sectionCollectionNew.contains(sectionCollectionOldSection)) {
                    sectionCollectionOldSection.setProfessorCourse(null);
                    sectionCollectionOldSection = em.merge(sectionCollectionOldSection);
                }
            }
            for (Section sectionCollectionNewSection : sectionCollectionNew) {
                if (!sectionCollectionOld.contains(sectionCollectionNewSection)) {
                    ProfessorCourse oldProfessorCourseOfSectionCollectionNewSection = sectionCollectionNewSection.getProfessorCourse();
                    sectionCollectionNewSection.setProfessorCourse(professorCourse);
                    sectionCollectionNewSection = em.merge(sectionCollectionNewSection);
                    if (oldProfessorCourseOfSectionCollectionNewSection != null && !oldProfessorCourseOfSectionCollectionNewSection.equals(professorCourse)) {
                        oldProfessorCourseOfSectionCollectionNewSection.getSectionCollection().remove(sectionCollectionNewSection);
                        oldProfessorCourseOfSectionCollectionNewSection = em.merge(oldProfessorCourseOfSectionCollectionNewSection);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ProfessorCoursePK id = professorCourse.getProfessorCoursePK();
                if (findProfessorCourse(id) == null) {
                    throw new NonexistentEntityException("The professorCourse with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ProfessorCoursePK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProfessorCourse professorCourse;
            try {
                professorCourse = em.getReference(ProfessorCourse.class, id);
                professorCourse.getProfessorCoursePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The professorCourse with id " + id + " no longer exists.", enfe);
            }
            Course course = professorCourse.getCourse();
            if (course != null) {
                course.getProfessorCourseCollection().remove(professorCourse);
                course = em.merge(course);
            }
            Professor professor = professorCourse.getProfessor();
            if (professor != null) {
                professor.getProfessorCourseCollection().remove(professorCourse);
                professor = em.merge(professor);
            }
            Collection<Section> sectionCollection = professorCourse.getSectionCollection();
            for (Section sectionCollectionSection : sectionCollection) {
                sectionCollectionSection.setProfessorCourse(null);
                sectionCollectionSection = em.merge(sectionCollectionSection);
            }
            em.remove(professorCourse);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProfessorCourse> findProfessorCourseEntities() {
        return findProfessorCourseEntities(true, -1, -1);
    }

    public List<ProfessorCourse> findProfessorCourseEntities(int maxResults, int firstResult) {
        return findProfessorCourseEntities(false, maxResults, firstResult);
    }

    private List<ProfessorCourse> findProfessorCourseEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Course> dept = cq.from(ProfessorCourse.class);
            cq.select(dept);
            cq.orderBy(cb.asc(dept.get(ProfessorCourse_.professor.getName())));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProfessorCourse findProfessorCourse(ProfessorCoursePK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProfessorCourse.class, id);
        } finally {
            em.close();
        }
    }

    public int getProfessorCourseCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ProfessorCourse> rt = cq.from(ProfessorCourse.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<ProfessorCourse> findProfessorById(String profId) {

        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("ProfessorCourse.findByProfessorId", ProfessorCourse.class).setParameter("professorId", profId).getResultList();
        } finally {
            em.close();
        }
    }
        }
