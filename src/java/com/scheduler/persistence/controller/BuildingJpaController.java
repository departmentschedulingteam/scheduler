/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import com.scheduler.persistence.Building;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.controller.exceptions.IllegalOrphanException;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author S519479
 */
public class BuildingJpaController implements Serializable {

    public BuildingJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Building building) throws PreexistingEntityException, Exception {
        if (building.getRoomCollection() == null) {
            building.setRoomCollection(new ArrayList<Room>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Room> attachedRoomCollection = new ArrayList<Room>();
            for (Room roomCollectionRoomToAttach : building.getRoomCollection()) {
                roomCollectionRoomToAttach = em.getReference(roomCollectionRoomToAttach.getClass(), roomCollectionRoomToAttach.getRoomPK());
                attachedRoomCollection.add(roomCollectionRoomToAttach);
            }
            building.setRoomCollection(attachedRoomCollection);
            em.persist(building);
            for (Room roomCollectionRoom : building.getRoomCollection()) {
                Building oldBuildingOfRoomCollectionRoom = roomCollectionRoom.getBuilding();
                roomCollectionRoom.setBuilding(building);
                roomCollectionRoom = em.merge(roomCollectionRoom);
                if (oldBuildingOfRoomCollectionRoom != null) {
                    oldBuildingOfRoomCollectionRoom.getRoomCollection().remove(roomCollectionRoom);
                    oldBuildingOfRoomCollectionRoom = em.merge(oldBuildingOfRoomCollectionRoom);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBuilding(building.getBuildingId()) != null) {
                throw new PreexistingEntityException("Building " + building + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Building building) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Building persistentBuilding = em.find(Building.class, building.getBuildingId());
            Collection<Room> roomCollectionOld = persistentBuilding.getRoomCollection();
            Collection<Room> roomCollectionNew = building.getRoomCollection();
            List<String> illegalOrphanMessages = null;
            for (Room roomCollectionOldRoom : roomCollectionOld) {
                if (!roomCollectionNew.contains(roomCollectionOldRoom)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Room " + roomCollectionOldRoom + " since its building field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Room> attachedRoomCollectionNew = new ArrayList<Room>();
            for (Room roomCollectionNewRoomToAttach : roomCollectionNew) {
                roomCollectionNewRoomToAttach = em.getReference(roomCollectionNewRoomToAttach.getClass(), roomCollectionNewRoomToAttach.getRoomPK());
                attachedRoomCollectionNew.add(roomCollectionNewRoomToAttach);
            }
            roomCollectionNew = attachedRoomCollectionNew;
            building.setRoomCollection(roomCollectionNew);
            building = em.merge(building);
            for (Room roomCollectionNewRoom : roomCollectionNew) {
                if (!roomCollectionOld.contains(roomCollectionNewRoom)) {
                    Building oldBuildingOfRoomCollectionNewRoom = roomCollectionNewRoom.getBuilding();
                    roomCollectionNewRoom.setBuilding(building);
                    roomCollectionNewRoom = em.merge(roomCollectionNewRoom);
                    if (oldBuildingOfRoomCollectionNewRoom != null && !oldBuildingOfRoomCollectionNewRoom.equals(building)) {
                        oldBuildingOfRoomCollectionNewRoom.getRoomCollection().remove(roomCollectionNewRoom);
                        oldBuildingOfRoomCollectionNewRoom = em.merge(oldBuildingOfRoomCollectionNewRoom);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = building.getBuildingId();
                if (findBuilding(id) == null) {
                    throw new NonexistentEntityException("The building with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Building building;
            try {
                building = em.getReference(Building.class, id);
                building.getBuildingId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The building with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Room> roomCollectionOrphanCheck = building.getRoomCollection();
            for (Room roomCollectionOrphanCheckRoom : roomCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Building (" + building + ") cannot be destroyed since the Room " + roomCollectionOrphanCheckRoom + " in its roomCollection field has a non-nullable building field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(building);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Building> findBuildingEntities() {
        return findBuildingEntities(true, -1, -1);
    }

    public List<Building> findBuildingEntities(int maxResults, int firstResult) {
        return findBuildingEntities(false, maxResults, firstResult);
    }

    private List<Building> findBuildingEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Building.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Building findBuilding(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Building.class, id);
        } finally {
            em.close();
        }
    }

    public int getBuildingCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Building> rt = cq.from(Building.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
