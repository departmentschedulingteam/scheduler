/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence.controller;

import com.scheduler.persistence.Lab;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import com.scheduler.persistence.controller.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author S519479
 */
public class LabJpaController implements Serializable {

    public LabJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Lab lab) throws PreexistingEntityException, Exception {
        if (lab.getTimeSlotsCollection() == null) {
            lab.setTimeSlotsCollection(new ArrayList<TimeSlots>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Room room = lab.getRoom();
            if (room != null) {
                room = em.getReference(room.getClass(), room.getRoomPK());
                lab.setRoom(room);
            }
            Section sectionFullId = lab.getSectionFullId();
            if (sectionFullId != null) {
                sectionFullId = em.getReference(sectionFullId.getClass(), sectionFullId.getSectionFullId());
                lab.setSectionFullId(sectionFullId);
            }
            Collection<TimeSlots> attachedTimeSlotsCollection = new ArrayList<TimeSlots>();
            for (TimeSlots timeSlotsCollectionTimeSlotsToAttach : lab.getTimeSlotsCollection()) {
                timeSlotsCollectionTimeSlotsToAttach = em.getReference(timeSlotsCollectionTimeSlotsToAttach.getClass(), timeSlotsCollectionTimeSlotsToAttach.getTimeSlotId());
                attachedTimeSlotsCollection.add(timeSlotsCollectionTimeSlotsToAttach);
            }
            lab.setTimeSlotsCollection(attachedTimeSlotsCollection);
            em.persist(lab);
            if (room != null) {
                room.getLabCollection().add(lab);
                room = em.merge(room);
            }
            if (sectionFullId != null) {
                sectionFullId.getLabCollection().add(lab);
                sectionFullId = em.merge(sectionFullId);
            }
            for (TimeSlots timeSlotsCollectionTimeSlots : lab.getTimeSlotsCollection()) {
                timeSlotsCollectionTimeSlots.getLabCollection().add(lab);
                timeSlotsCollectionTimeSlots = em.merge(timeSlotsCollectionTimeSlots);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLab(lab.getLabFullId()) != null) {
                throw new PreexistingEntityException("Lab " + lab + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Lab lab) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Lab persistentLab = em.find(Lab.class, lab.getLabFullId());
            Room roomOld = persistentLab.getRoom();
            Room roomNew = lab.getRoom();
            Section sectionFullIdOld = persistentLab.getSectionFullId();
            Section sectionFullIdNew = lab.getSectionFullId();
            Collection<TimeSlots> timeSlotsCollectionOld = persistentLab.getTimeSlotsCollection();
            Collection<TimeSlots> timeSlotsCollectionNew = lab.getTimeSlotsCollection();
            if (roomNew != null) {
                roomNew = em.getReference(roomNew.getClass(), roomNew.getRoomPK());
                lab.setRoom(roomNew);
            }
            if (sectionFullIdNew != null) {
                sectionFullIdNew = em.getReference(sectionFullIdNew.getClass(), sectionFullIdNew.getSectionFullId());
                lab.setSectionFullId(sectionFullIdNew);
            }
            Collection<TimeSlots> attachedTimeSlotsCollectionNew = new ArrayList<TimeSlots>();
            for (TimeSlots timeSlotsCollectionNewTimeSlotsToAttach : timeSlotsCollectionNew) {
                timeSlotsCollectionNewTimeSlotsToAttach = em.getReference(timeSlotsCollectionNewTimeSlotsToAttach.getClass(), timeSlotsCollectionNewTimeSlotsToAttach.getTimeSlotId());
                attachedTimeSlotsCollectionNew.add(timeSlotsCollectionNewTimeSlotsToAttach);
            }
            timeSlotsCollectionNew = attachedTimeSlotsCollectionNew;
            lab.setTimeSlotsCollection(timeSlotsCollectionNew);
            lab = em.merge(lab);
            if (roomOld != null && !roomOld.equals(roomNew)) {
                roomOld.getLabCollection().remove(lab);
                roomOld = em.merge(roomOld);
            }
            if (roomNew != null && !roomNew.equals(roomOld)) {
                roomNew.getLabCollection().add(lab);
                roomNew = em.merge(roomNew);
            }
            if (sectionFullIdOld != null && !sectionFullIdOld.equals(sectionFullIdNew)) {
                sectionFullIdOld.getLabCollection().remove(lab);
                sectionFullIdOld = em.merge(sectionFullIdOld);
            }
            if (sectionFullIdNew != null && !sectionFullIdNew.equals(sectionFullIdOld)) {
                sectionFullIdNew.getLabCollection().add(lab);
                sectionFullIdNew = em.merge(sectionFullIdNew);
            }
            for (TimeSlots timeSlotsCollectionOldTimeSlots : timeSlotsCollectionOld) {
                if (!timeSlotsCollectionNew.contains(timeSlotsCollectionOldTimeSlots)) {
                    timeSlotsCollectionOldTimeSlots.getLabCollection().remove(lab);
                    timeSlotsCollectionOldTimeSlots = em.merge(timeSlotsCollectionOldTimeSlots);
                }
            }
            for (TimeSlots timeSlotsCollectionNewTimeSlots : timeSlotsCollectionNew) {
                if (!timeSlotsCollectionOld.contains(timeSlotsCollectionNewTimeSlots)) {
                    timeSlotsCollectionNewTimeSlots.getLabCollection().add(lab);
                    timeSlotsCollectionNewTimeSlots = em.merge(timeSlotsCollectionNewTimeSlots);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = lab.getLabFullId();
                if (findLab(id) == null) {
                    throw new NonexistentEntityException("The lab with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Lab lab;
            try {
                lab = em.getReference(Lab.class, id);
                lab.getLabFullId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The lab with id " + id + " no longer exists.", enfe);
            }
            Room room = lab.getRoom();
            if (room != null) {
                room.getLabCollection().remove(lab);
                room = em.merge(room);
            }
            Section sectionFullId = lab.getSectionFullId();
            if (sectionFullId != null) {
                sectionFullId.getLabCollection().remove(lab);
                sectionFullId = em.merge(sectionFullId);
            }
            Collection<TimeSlots> timeSlotsCollection = lab.getTimeSlotsCollection();
            for (TimeSlots timeSlotsCollectionTimeSlots : timeSlotsCollection) {
                timeSlotsCollectionTimeSlots.getLabCollection().remove(lab);
                timeSlotsCollectionTimeSlots = em.merge(timeSlotsCollectionTimeSlots);
            }
            em.remove(lab);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Lab> findLabEntities() {
        return findLabEntities(true, -1, -1);
    }

    public List<Lab> findLabEntities(int maxResults, int firstResult) {
        return findLabEntities(false, maxResults, firstResult);
    }

    private List<Lab> findLabEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Lab.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Lab findLab(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Lab.class, id);
        } finally {
            em.close();
        }
    }

    public int getLabCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Lab> rt = cq.from(Lab.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        }
