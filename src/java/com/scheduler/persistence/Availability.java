/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author S519479
 */
@Entity
@Table(name = "availability")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Availability.findAll", query = "SELECT a FROM Availability a"),
    @NamedQuery(name = "Availability.findByTimeSlotId", query = "SELECT a FROM Availability a WHERE a.availabilityPK.timeSlotId = :timeSlotId"),
    @NamedQuery(name = "Availability.findByBuildingId", query = "SELECT a FROM Availability a WHERE a.availabilityPK.buildingId = :buildingId"),
    @NamedQuery(name = "Availability.findByRoomNumber", query = "SELECT a FROM Availability a WHERE a.availabilityPK.roomNumber = :roomNumber"),
    @NamedQuery(name = "Availability.findBySeatsAvailability", query = "SELECT a FROM Availability a WHERE a.seatsAvailability = :seatsAvailability")})
public class Availability implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AvailabilityPK availabilityPK;
    @Column(name = "seats_availability")
    private Integer seatsAvailability;
    @JoinColumns({
        @JoinColumn(name = "building_id", referencedColumnName = "building_id", insertable = false, updatable = false),
        @JoinColumn(name = "room_number", referencedColumnName = "room_number", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Room room;
    @JoinColumn(name = "time_slot_id", referencedColumnName = "time_slot_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TimeSlots timeSlots;

    public Availability() {
    }

    public Availability(AvailabilityPK availabilityPK) {
        this.availabilityPK = availabilityPK;
    }

    public Availability(int timeSlotId, String buildingId, int roomNumber) {
        this.availabilityPK = new AvailabilityPK(timeSlotId, buildingId, roomNumber);
    }

    public AvailabilityPK getAvailabilityPK() {
        return availabilityPK;
    }

    public void setAvailabilityPK(AvailabilityPK availabilityPK) {
        this.availabilityPK = availabilityPK;
    }

    public Integer getSeatsAvailability() {
        return seatsAvailability;
    }

    public void setSeatsAvailability(Integer seatsAvailability) {
        this.seatsAvailability = seatsAvailability;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public TimeSlots getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(TimeSlots timeSlots) {
        this.timeSlots = timeSlots;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (availabilityPK != null ? availabilityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Availability)) {
            return false;
        }
        Availability other = (Availability) object;
        if ((this.availabilityPK == null && other.availabilityPK != null) || (this.availabilityPK != null && !this.availabilityPK.equals(other.availabilityPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scheduler.persistence.Availability[ availabilityPK=" + availabilityPK + " ]";
    }
    
}
