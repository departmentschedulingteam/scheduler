/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 
 
 Karthik Testu
 */
package com.scheduler.servlets;

import com.scheduler.persistence.Building;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.controller.BuildingJpaController;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class BuildingServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        BuildingJpaController buildingController = new BuildingJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            if (reqType.equals("getBuildingList")) {
                
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                gen.writeStartArray("buildings");

                for (Building build : buildingController.findBuildingEntities()) {
                    gen.writeStartObject();
                    System.out.println(build.getBuildingId()+build.getBuildingName());
                    gen.write("buildingID", build.getBuildingId());
                    gen.write("buildingName", build.getBuildingName());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equals("editBuilding")) {
                String buildingID = request.getParameter("buildingID");
                String buildingName = request.getParameter("buildingName");
                Building building = new Building(buildingID);
                building.setBuildingName(buildingName);
                building.setRoomCollection(buildingController.findBuilding(buildingID).getRoomCollection());
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                // System.out.println("set building: "+ buildDAO.setBuilding(building));
                buildingController.edit(building);

                gen.write("message", "Building  Edited");

                gen.writeStartArray("buildings");
                for (Building build : buildingController.findBuildingEntities()) {
                    gen.writeStartObject();
                    gen.write("buildingID", build.getBuildingId());
                    gen.write("buildingName", build.getBuildingName());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();

            } else if (reqType.equals("deleteBuilding")) {
                String buildingID = request.getParameter("buildingID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                try {
                    buildingController.destroy(buildingID);
                    gen.write("message", "Building with id " + buildingID + " deleted from Database");
                } catch (Exception e) {
                    gen.write("message", "There are rooms associated to this building, make sure to delete rooms first");
                }

                
                gen.writeStartArray("buildings");
                for (Building build : buildingController.findBuildingEntities()) {
                    gen.writeStartObject();
                    gen.write("buildingID", build.getBuildingId());
                    gen.write("buildingName", build.getBuildingName());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else {
                String buildingID = request.getParameter("buildingID");
                String buildingName = request.getParameter("buildingName");

                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                if (buildingController.findBuilding(buildingID)==null) {
                Building build = new Building();
                build.setBuildingId(buildingID);
                build.setBuildingName(buildingName);
                build.setRoomCollection(new ArrayList<Room>());
                buildingController.create(build);
                gen.write("message", "New building added to database");

                  } else {
                    gen.write("message", "This building id is already present in database");
                  }
                gen.writeStartArray("buildings");
                for (Building build1 : buildingController.findBuildingEntities()) {
                    gen.writeStartObject();
                    gen.write("buildingID", build1.getBuildingId());
                    gen.write("buildingName", build1.getBuildingName());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(BuildingServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(BuildingServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
