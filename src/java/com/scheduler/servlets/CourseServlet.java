/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.persistence.Course;
import com.scheduler.persistence.CoursePK;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.controller.CourseJpaController;
import com.scheduler.persistence.controller.DepartmentJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class CourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            CourseJpaController courseController = new CourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
            DepartmentJpaController deptController = new DepartmentJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
            if (reqType.equals("getCourseList")) {
                int departmentID = Integer.parseInt(request.getParameter("departmentID"));
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();

                gen.writeStartArray("courses");

                for (Course course : courseController.findCourseEntitiesSortedByCourse()) {
                    if (course.getDepartment().getDepartmentId() == departmentID) {
                        gen.writeStartObject();
                        gen.write("deptID", course.getCoursePK().getDepartmentId());
                        gen.write("courseID", course.getCoursePK().getCourseId());
                        gen.write("courseName", course.getCourseName());
                        gen.write("labType", course.getLabType());
                        gen.writeEnd();
                    }
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equals("getFullCoursesList")) {
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                populateCourses(gen, courseController.findCourseEntities());

                gen.writeEnd();
                gen.close();
            } else if (reqType.equalsIgnoreCase("delete")) {
                int departmentID = Integer.parseInt(request.getParameter("departmentID"));
                String courseID = request.getParameter("courseID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                System.out.println(new String(departmentID + courseID));
                try {
                    courseController.destroy(new CoursePK(departmentID, Integer.parseInt(courseID)));
                    gen.write("message", "Course Deleted. Click Get Courses to see the updated list. ");
                } catch (Exception ex) {
                    gen.write("message", "There is data dependent upon this course.");
                }

                gen.writeEnd();
                gen.close();

            } else if (reqType.equalsIgnoreCase("editing")) {
                System.out.println("*********************Editing Course Method");
                int departmentID = Integer.parseInt(request.getParameter("departmentID"));
                String courseID = request.getParameter("courseID");
                String labType = request.getParameter("labType");
                String courseName = request.getParameter("courseName");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();

                if (courseController.findCourse(new CoursePK(departmentID, Integer.parseInt(courseID))) != null) {
                    Course course = new Course();

                    course.setDepartment(deptController.findDepartment(departmentID));
                    course.setCoursePK(new CoursePK(departmentID, Integer.parseInt(courseID)));
                    course.setLabType(labType);
                    course.setCourseName(courseName);
                    course.setProfessorCourseCollection(courseController.findCourse(new CoursePK(departmentID, Integer.parseInt(courseID))).getProfessorCourseCollection());

                    try {
                        courseController.edit(course);

                        gen.write("message", "Y");
                    } catch (Exception ex) {

                        gen.write("message", "N");
                    }

                }

                gen.writeEnd();
                gen.close();

            } else {
                int departmentID = Integer.parseInt(request.getParameter("departmentID"));
                String courseID = request.getParameter("courseID");
                String labType = request.getParameter("labType");
                String courseName = request.getParameter("courseName");

                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                if (courseController.findCourse(new CoursePK(departmentID, Integer.parseInt(courseID))) == null) {
                    Course course = new Course();

                    course.setDepartment(deptController.findDepartment(departmentID));
                    course.setCoursePK(new CoursePK(departmentID, Integer.parseInt(courseID)));
                    course.setLabType(labType);
                    course.setCourseName(courseName);
                    course.setProfessorCourseCollection(new ArrayList<ProfessorCourse>());
                    try {
                        courseController.create(course);

                        gen.write("message", "New Course added to database");
                    } catch (Exception ex) {
                        gen.write("message", "Problem with database");
                    }
                } else {
                    gen.write("message", "This Course is already present in database");
                }
                gen.writeEnd();
                gen.close();
            }
        }
    }

    public void populateCourses(JsonGenerator gen, List<Course> courses) {

        gen.writeStartArray("courses");

        for (Course course : courses) {
            gen.writeStartObject();
            gen.write("courseID", course.getCoursePK().getCourseId());
            gen.write("courseName", course.getCourseName());
            gen.write("labType", course.getLabType());
            gen.write("departmentID", course.getDepartment().getDepartmentId());
            gen.writeEnd();
        }
        gen.writeEnd();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
