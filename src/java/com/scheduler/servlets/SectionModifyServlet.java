/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.helper.SchedulerHelper;
import com.scheduler.helper.SectionLabConflicts;
import com.scheduler.persistence.Availability;
import com.scheduler.persistence.AvailabilityPK;
import com.scheduler.persistence.CoursePK;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.AvailabilityJpaController;
import com.scheduler.persistence.controller.CourseJpaController;
import com.scheduler.persistence.controller.LabJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class SectionModifyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        CourseJpaController courseController = new CourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            if (reqType.equals("getSectionIDList")) {
                int departmentID = Integer.parseInt(request.getParameter("deptId"));
                int courseID = Integer.parseInt(request.getParameter("courseID"));
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();

                List<Section> sectionList = new ArrayList();
                List<ProfessorCourse> profCourseList = (List<ProfessorCourse>) courseController.findCourse((new CoursePK(departmentID, courseID))).getProfessorCourseCollection();
                for (ProfessorCourse professorCourse : profCourseList) {
                    if (professorCourse.getSectionCollection() != null || professorCourse.getSectionCollection().isEmpty()) {
                        for (Section section : professorCourse.getSectionCollection()) {
                            if (section != null && section.getLabCollection().isEmpty()) {
                                sectionList.add(section);
                            }
                        }
                    }
                }
                if (!sectionList.isEmpty()) {
                    Collections.sort(sectionList, (Section o1, Section o2) -> o1.getSectionId() - o2.getSectionId());
                    gen.writeStartArray("sectionIDList");
                    for (Section section : sectionList) {
                        gen.writeStartObject();
                        gen.write("sectionFullID", section.getSectionFullId());
                        System.out.println("sectionList: " + section.getSectionFullId());
                        gen.write("departmentID", section.getProfessorCourse().getProfessorCoursePK().getDepartrmentId());
                        gen.write("courseID", section.getProfessorCourse().getProfessorCoursePK().getCourseId());
                        gen.write("sectionID", section.getSectionId());
                        gen.write("professorID", section.getProfessorCourse().getProfessor().getProfessorId());
                        gen.write("professorName", section.getProfessorCourse().getProfessor().getFirstName() + " " + section.getProfessorCourse().getProfessor().getLastName());
                        gen.write("buildingID", section.getRoom().getRoomPK().getBuildingId());
                        gen.write("roomID", section.getRoom().getRoomPK().getRoomNumber());
                        gen.writeEnd();
                    }
                    gen.writeEnd();
                } else {
                    gen.write("message", "No Sections Found");
                }
                gen.writeEnd();
                gen.close();
            } else if (reqType.equals("getSectionLabIDList")) {
                int departmentID = Integer.parseInt(request.getParameter("deptId"));
                int courseID = Integer.parseInt(request.getParameter("courseID"));
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();

                List<Section> sectionList = new ArrayList();
                List<ProfessorCourse> profCourseList = (List<ProfessorCourse>) courseController.findCourse((new CoursePK(departmentID, courseID))).getProfessorCourseCollection();
                for (ProfessorCourse professorCourse : profCourseList) {
                    if (professorCourse.getSectionCollection() != null || professorCourse.getSectionCollection().isEmpty()) {
                        for (Section section : professorCourse.getSectionCollection()) {
                            if (section != null) {
                                sectionList.add(section);
                            }
                        }
                    }
                }
                if (!sectionList.isEmpty()) {
                    Collections.sort(sectionList, (Section o1, Section o2) -> o1.getSectionId() - o2.getSectionId());
                    gen.writeStartArray("sectionLabList");
                    for (Section section : sectionList) {
                        gen.writeStartObject();
                        gen.write("sectionLabFullID", section.getSectionFullId());
                        gen.write("professorName", section.getProfessorCourse().getProfessor().getFirstName() + " " + section.getProfessorCourse().getProfessor().getLastName());
                        gen.write("buildingRoom", section.getRoom().getRoomPK().getBuildingId() + " " + section.getRoom().getRoomPK().getRoomNumber());
                        if (!section.getLabCollection().isEmpty()) {
                            gen.write("hasLab", "YES");
                        } else {
                            gen.write("hasLab", "NO");
                        }
                        gen.write("seats",section.getOccupied());
                        SchedulerHelper schedulerHelper = new SchedulerHelper();
                        gen.write("timeSlots", schedulerHelper.getTimeSlotsString(section.getTimeSlotsCollection()));
                        gen.write("type", "NotLab");
                        gen.writeEnd();
                        if (!section.getLabCollection().isEmpty()) {
                            for (Lab lab : section.getLabCollection()) {
                                gen.writeStartObject();
                                gen.write("sectionLabFullID", lab.getLabFullId());
                                gen.write("professorName", lab.getSectionFullId().getProfessorCourse().getProfessor().getFirstName() + " " + lab.getSectionFullId().getProfessorCourse().getProfessor().getLastName());
                                gen.write("buildingRoom", lab.getRoom().getRoomPK().getBuildingId() + " " + section.getRoom().getRoomPK().getRoomNumber());
                                gen.write("timeSlots", schedulerHelper.getTimeSlotsString(lab.getTimeSlotsCollection()));
                                gen.write("type", "Lab");
                                gen.write("seats",lab.getOccupied());
                                gen.writeEnd();
                            }
                        }
                    }
                    gen.writeEnd();
                } else {
                    gen.write("message", "No Sections Found");
                }
                gen.writeEnd();
                gen.close();
            } else if(reqType.equals("getSectionDetails")){
                
                String sectionID = request.getParameter("sectionID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                Section section = sectionController.findSection(sectionID);
                gen.writeStartArray("sectionDetails");
                    if (section!=null) {
                        gen.writeStartObject();
                        gen.write("sectionFullID", section.getSectionFullId());
                        gen.write("departmentID", section.getProfessorCourse().getProfessorCoursePK().getDepartrmentId());
                        gen.write("departmentName", section.getProfessorCourse().getCourse().getDepartment().getDepartmentName());
                        gen.write("courseID", section.getProfessorCourse().getProfessorCoursePK().getCourseId());
                        gen.write("courseName", section.getProfessorCourse().getCourse().getCourseName());
                        gen.write("sectionID", section.getSectionId());
                        gen.write("professorID", section.getProfessorCourse().getProfessor().getProfessorId());
                        gen.write("professorName", section.getProfessorCourse().getProfessor().getFirstName() + " " + section.getProfessorCourse().getProfessor().getLastName());
                        gen.write("buildingID", section.getRoom().getRoomPK().getBuildingId());
                        gen.write("roomID", section.getRoom().getRoomPK().getRoomNumber());
                        gen.write("seats", section.getOccupied());
                        SchedulerHelper schedulerHelper = new SchedulerHelper();
                        gen.write("timeSlots", schedulerHelper.getTimeSlotsString(section.getTimeSlotsCollection()));
                        gen.writeEnd();
                    }else{
                        gen.write("message", "No Sections Found");
                    }
                    gen.writeEnd();
                 
                gen.writeEnd();
                gen.close();
            } else if(reqType.equals("delete")){
                String sectionID = request.getParameter("sectionID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                AvailabilityJpaController availabilityController = new AvailabilityJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                Section section = sectionController.findSection(sectionID);
                int seatsOccupied = section.getOccupied();
                for(TimeSlots timeSlot: section.getTimeSlotsCollection()){
                    AvailabilityPK aPK = new AvailabilityPK(timeSlot.getTimeSlotId(), section.getRoom().getRoomPK().getBuildingId(), section.getRoom().getRoomPK().getRoomNumber());
                    Availability availability = availabilityController.findAvailability(aPK);
                    if((availability.getSeatsAvailability()-seatsOccupied)<=0){
                        //availability.setSeatsAvailability(availability.getSeatsAvailability()-seatsOccupied);
                        availabilityController.destroy(aPK);
                    }else{
                        availability.setSeatsAvailability(availability.getSeatsAvailability()-seatsOccupied);
                        availabilityController.edit(availability);
                    }
                    
                }
                sectionController.destroy(section.getSectionFullId());
                gen.write("message", "Section/Lab deleted Succesfully");
                gen.writeEnd();
                gen.close();
            }else if (reqType.equals("editSection")) {
                String sectionFullID = request.getParameter("sectionFullID");
                String buildingID = request.getParameter("buildingID");
                int room = Integer.parseInt(request.getParameter("room"));
                int seats = Integer.parseInt(request.getParameter("seats"));
                String st = request.getParameter("startTime");
                String en = request.getParameter("endTime");
                String sectionDay[] = request.getParameterValues("sectionDay");
                String[] startTime = new String[sectionDay.length];
                String[] endTime = new String[sectionDay.length];
                for (int i = 0; i < sectionDay.length; i++) {
                    startTime[i] = st;
                    endTime[i] = en;
                }
                String allow = request.getParameter("allow");
                SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                Section section = sectionController.findSection(sectionFullID);
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                SchedulerHelper schedulerHelper = new SchedulerHelper();
                HashMap hMap = schedulerHelper.refineEditSection(section, buildingID, room, seats, startTime, endTime, sectionDay, allow);
                String allowOrDeny = "Allow";
                System.out.println("allow string: " + allow);
                if (!allow.equalsIgnoreCase("deny") || allow.equalsIgnoreCase("allow")) {
                    gen.writeStartArray("scheduler");
                    if (hMap.containsKey("conflicts")) {
                        for (SectionLabConflicts secLabConflicts : (List<SectionLabConflicts>) (Object) hMap.get("conflicts")) {
                            if (secLabConflicts.getConflict().equalsIgnoreCase("Hard Conflict")) {
                                allowOrDeny = "Deny";
                            }
                            if (!secLabConflicts.isIsLab()) {
                                Section sec = sectionController.findSection(secLabConflicts.getID());
                                gen.writeStartObject();
                                gen.write("sectionLabFullID", sec.getSectionFullId());
                                gen.write("professor", sec.getProfessorCourse().getProfessor().getLastName() + " " + sec.getProfessorCourse().getProfessor().getFirstName());
                                gen.write("buildingRoom", sec.getRoom().getBuilding().getBuildingId() + "" + sec.getRoom().getRoomPK().getRoomNumber());
                                gen.write("conflictType", secLabConflicts.getConflict());

                                gen.write("timeSlots", schedulerHelper.getTimeSlotsString(sec.getTimeSlotsCollection()));
                                gen.writeEnd();
                            } else {
                                Lab lab = labController.findLab(secLabConflicts.getID());
                                gen.writeStartObject();
                                gen.write("sectionLabFullID", lab.getLabFullId());
                                gen.write("professor", lab.getSectionFullId().getProfessorCourse().getProfessor().getLastName() + " " + lab.getSectionFullId().getProfessorCourse().getProfessor().getFirstName());
                                gen.write("buildingRoom", lab.getRoom().getBuilding().getBuildingId() + "" + lab.getRoom().getRoomPK().getRoomNumber());
                                gen.write("conflictType", secLabConflicts.getConflict());

                                gen.write("timeSlots", schedulerHelper.getTimeSlotsString(lab.getTimeSlotsCollection()));
                                gen.writeEnd();
                            }
                        }
                    }
                    gen.writeEnd();
                }
                List<SectionLabConflicts> sectionLabConflictsList = (List<SectionLabConflicts>) hMap.get("conflicts");
                if (!sectionLabConflictsList.isEmpty() && !allow.equalsIgnoreCase("allow")) {
                    gen.write("allowOrDeny", allowOrDeny);
                    gen.write("message", "Conflicts detected with");
                    gen.writeEnd();
                } else if (hMap.containsKey("availability")) {
                    allowOrDeny = "Deny";
                    gen.write("allowOrDeny", allowOrDeny);
                    gen.write("message", (String) hMap.get("availability"));
                    gen.writeEnd();
                } else {
                    gen.write("message", "Section Edited Successfully");
                    gen.writeEnd();
                }
                gen.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(SectionModifyServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
