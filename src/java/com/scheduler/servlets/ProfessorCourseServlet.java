/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.controller.ProfessorCourseJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class ProfessorCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ProfessorCourseJpaController professorCourseController = new ProfessorCourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            if (reqType.equals("getProfessorList")) {
                String courseID = request.getParameter("courseID");
                String deptID = request.getParameter("deptId");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                gen.writeStartArray("professors");
                List<ProfessorCourse> pclist = professorCourseController.findProfessorCourseEntities();
                sortEntities(pclist);
                for (ProfessorCourse prof : pclist) {
                    if (prof.getProfessorCoursePK().getCourseId() == Integer.parseInt(courseID)) {
                        if (prof.getProfessorCoursePK().getDepartrmentId() == Integer.parseInt(deptID)) {
                            gen.writeStartObject();
                            gen.write("professorID", prof.getProfessor().getProfessorId());
                            gen.write("professorName", prof.getProfessor().getFirstName() + " " + prof.getProfessor().getLastName());
                            gen.writeEnd();
                        }
                    }
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equals("getCourseList")) {
                String professorID = request.getParameter("professorID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                gen.writeStartArray("professors");

                for (ProfessorCourse prof : professorCourseController.findProfessorById(professorID)) {
                    gen.writeStartObject();
                    gen.write("courseID", prof.getCourse().getCoursePK().getCourseId());
                    gen.write("courseName", prof.getCourse().getCourseName());
                    gen.write("departmentID", prof.getCourse().getDepartment().getDepartmentId());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else {

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void sortEntities(List<ProfessorCourse> findProfessorCourseEntities) {
        Collections.sort(findProfessorCourseEntities, new ProfessorCourse());
    }

}
