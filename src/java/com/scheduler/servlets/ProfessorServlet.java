/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.persistence.Course;
import com.scheduler.persistence.CoursePK;
import com.scheduler.persistence.Professor;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.ProfessorCoursePK;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.controller.CourseJpaController;
import com.scheduler.persistence.controller.ProfessorCourseJpaController;
import com.scheduler.persistence.controller.ProfessorJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class ProfessorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");

            ProfessorJpaController professorController = new ProfessorJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
            ProfessorCourseJpaController professorCourseController = new ProfessorCourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
            CourseJpaController courseController = new CourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
            if (reqType.equals("getProfessorList")) {
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                gen.writeStartArray("professors");

                for (Professor professor : professorController.findProfessorEntities()) {
                    gen.writeStartObject();
                    gen.write("professorID", professor.getProfessorId());
                    gen.write("firstName", professor.getFirstName());
                    gen.write("lastName", professor.getLastName());
                    gen.write("professorCode", professor.getProfessorInitials());

                    String coursesStr = "";
                    for (ProfessorCourse pcourse : professorCourseController.findProfessorCourseEntities()) {
                        if (professor.getProfessorId().equals(pcourse.getProfessorCoursePK().getProfessorId())) {
                            Course courseObje = pcourse.getCourse();
                            coursesStr += courseObje.getDepartment().getDepartmentId() + "-" + courseObje.getCoursePK().getCourseId() + "-" + (pcourse.getSectionCollection().isEmpty() ? "N" : "R") + ",";
                        }

                    }

                    gen.write("courses", coursesStr.substring(0, coursesStr.length()));
                    //System.out.println(coursesStr.substring(0, coursesStr.length()-1));

                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equalsIgnoreCase("delete")) {
                String professorID = request.getParameter("professorID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();

                Professor professorOld = professorController.findProfessor(professorID);
                try {
                    for (ProfessorCourse pf : professorOld.getProfessorCourseCollection()) {
                        if (!pf.getSectionCollection().isEmpty()) {
                            throw new Exception();
                        }
                    }

                    for (ProfessorCourse pf : professorOld.getProfessorCourseCollection()) {
                        professorCourseController.destroy(pf.getProfessorCoursePK());
                    }
                    professorController.destroy(professorOld.getProfessorId());
                    gen.write("message", "Professor Deleted. Click View/Edit Professors to see the updated list.");
                } catch (Exception ex) {

                    gen.write("message", "There are sections for this professor, please remove those sections first");
                } finally {

                    gen.writeEnd();
                    gen.close();
                }

            } else if (reqType.equalsIgnoreCase("editing")) {
                String professorID = request.getParameter("professorID");
                String courses[] = request.getParameterValues("courses");

                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                Professor professorOld = professorController.findProfessor(professorID);
                ArrayList<ProfessorCourse> professorCourseList = new ArrayList<ProfessorCourse>();
                if (professorOld != null) {

                    ProfessorCourse professorCourse = null;

                    for (String str : courses) {
                        String[] strArr = str.split("-");
                        professorCourse = new ProfessorCourse();
                        professorCourse.setProfessor(professorOld);
                        professorCourse.setCourse(courseController.findCourse(new CoursePK(Integer.parseInt(strArr[0]), Integer.parseInt(strArr[1]))));
                        professorCourse.setSectionCollection(new ArrayList<Section>());
                        professorCourse.setProfessorCoursePK(new ProfessorCoursePK(Integer.parseInt(strArr[0]), Integer.parseInt(strArr[1]), professorID));
                        professorCourseList.add(professorCourse);
                    }

                    Collection<ProfessorCourse> professorCourseListToRemove = new ArrayList<ProfessorCourse>();
                    ArrayList<ProfessorCourse> professorCourseListToCreate = new ArrayList<ProfessorCourse>();
                    ArrayList<ProfessorCourse> professorCourseListTobeRemoved = new ArrayList<ProfessorCourse>();

                    for (ProfessorCourse pcourse : professorCourseList) {
                        if (!professorOld.getProfessorCourseCollection().contains(pcourse)) {

                            professorCourseListToCreate.add(pcourse);
                        }
                    }

                    for (ProfessorCourse pcourse : professorOld.getProfessorCourseCollection()) {
                        if (!professorCourseList.contains(pcourse)) {

                            professorCourseListTobeRemoved.add(pcourse);
                        }
                    }

                    try {
                        for (ProfessorCourse pf : professorCourseListToCreate) {
                            professorCourseController.create(pf);
                        }
                        for (ProfessorCourse pf : professorCourseListTobeRemoved) {
                            professorCourseController.destroy(pf.getProfessorCoursePK());
                        }
                        professorOld.getProfessorCourseCollection().addAll(professorCourseListToCreate);
                        professorOld.getProfessorCourseCollection().removeAll(professorCourseListTobeRemoved);
                        professorController.edit(professorOld);

                        gen.write("message", "Y");

                    } catch (Exception ex) {
                        System.out.println("Exception cerateing prof: " + ex);
                        gen.write("message", "There are sections for this professor scheduled for edited courses");
                    }

                }
                gen.writeEnd();
                gen.close();
            } else {
                String professorID = request.getParameter("professorID");
                String firstName = request.getParameter("firstName");
                String lastName = request.getParameter("lastName");
                String professorCode = request.getParameter("professorCode");
                String courses[] = request.getParameterValues("courses");
                String surveyCode = request.getParameter("surveyCode");

                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                if (professorController.findProfessor(professorID) == null) {
                    Professor professor = new Professor();
                    professor.setProfessorId(professorID);
                    professor.setFirstName(firstName);
                    professor.setLastName(lastName);
                    professor.setProfessorInitials(professorCode);
                    ProfessorCourse professorCourse = null;
                    ArrayList<ProfessorCourse> professorCourseList = new ArrayList<ProfessorCourse>();
                    //System.out.println(courses.length);

                    for (String str : courses) {
                        String[] strArr = str.split("-");
                        professorCourse = new ProfessorCourse();
                        professorCourse.setProfessor(professor);
                        professorCourse.setCourse(courseController.findCourse(new CoursePK(Integer.parseInt(strArr[0]), Integer.parseInt(strArr[1]))));
                        professorCourse.setSectionCollection(new ArrayList<Section>());
                        professorCourse.setProfessorCoursePK(new ProfessorCoursePK(Integer.parseInt(strArr[0]), Integer.parseInt(strArr[1]), professorID));
                        professorCourseList.add(professorCourse);
                    }
                    professor.setProfessorCourseCollection(new ArrayList<ProfessorCourse>());
                    try {

                        professorController.create(professor);

                        for (ProfessorCourse pc : professorCourseList) {
                            professorCourseController.create(pc);
                        }
                        professor.setProfessorCourseCollection(professorCourseList);
                        professorController.edit(professor);

                        gen.write("message", "New professor added to database");

                    } catch (Exception ex) {
                        System.out.println("Exception cerateing prof: " + ex);
                        gen.write("message", "problem with database");
                    }
                } else {
                    gen.write("message", "This professor id is already present in database");
                }
                gen.writeEnd();
                gen.close();
            }
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
