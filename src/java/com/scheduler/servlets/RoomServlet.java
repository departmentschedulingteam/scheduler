/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.persistence.Availability;
import com.scheduler.persistence.Building;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.RoomPK;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.controller.BuildingJpaController;
import com.scheduler.persistence.controller.RoomJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class RoomServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        RoomJpaController roomController = new RoomJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            if (reqType.equals("getRoomList")) {
                String buildingID = request.getParameter("buildingID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                gen.writeStartArray("rooms");

                for (Room room : roomController.findRoomEntities()) {
                    if (room.getBuilding().getBuildingId().equalsIgnoreCase(buildingID)) {
                        gen.writeStartObject();
                        gen.write("roomNumber", room.getRoomPK().getRoomNumber());
                        gen.write("labType", room.getLabType());
                        gen.write("capacity", room.getCapacity());
                        gen.write("buildingId", room.getBuilding().getBuildingId());
                        gen.writeEnd();
                    }
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equals("getRoomLabList")) {
                String buildingID = request.getParameter("buildingID");
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                gen.writeStartArray("rooms");

                for (Room room : roomController.findRoomEntities()) {
                    if (room.getLabType().equalsIgnoreCase("pc") || room.getLabType().equalsIgnoreCase("mac")) {
                        gen.writeStartObject();
                        gen.write("roomNumber", room.getRoomPK().getRoomNumber());
                        gen.write("labType", room.getLabType());
                        gen.write("capacity", room.getCapacity());
                        gen.write("buildingId", room.getBuilding().getBuildingId());
                        gen.writeEnd();
                    }
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equalsIgnoreCase("editing")) {
                String buildingID = request.getParameter("buildingID");
                int roomNumber = Integer.parseInt(request.getParameter("roomNumber"));
                String labType = request.getParameter("labType");
                int roomCapacity = Integer.parseInt(request.getParameter("roomCapacity"));
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                Room room = new Room();
                room.setBuilding(new Building(buildingID));
                room.setRoomPK(new RoomPK(buildingID, roomNumber));
                room.setCapacity(roomCapacity);
                room.setLabType(labType);
                Room roomOld = roomController.findRoom(new RoomPK(buildingID, roomNumber));
                room.setAvailabilityCollection(roomOld.getAvailabilityCollection());
                room.setLabCollection(roomOld.getLabCollection());
                room.setSectionCollection(roomOld.getSectionCollection());

                try {
                    roomController.edit(room);
                    gen.write("message", "Room Edited Successfully");

                } catch (Exception ex) {
                    gen.write("message", "Cannot edit room, problem with database");

                }
                gen.writeStartArray("rooms");

                for (Room roomCollection : roomController.findRoomEntities()) {
                    if (roomCollection.getBuilding().getBuildingId().equalsIgnoreCase(buildingID)) {
                        gen.writeStartObject();
                        gen.write("roomNumber", roomCollection.getRoomPK().getRoomNumber());
                        gen.write("labType", roomCollection.getLabType());
                        gen.write("capacity", roomCollection.getCapacity());
                        gen.write("buildingId", roomCollection.getBuilding().getBuildingId());
                        gen.writeEnd();
                    }
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();

            } else if (reqType.equalsIgnoreCase("delete")) {

                String buildingID = request.getParameter("buildingID");
                int roomNumber = Integer.parseInt(request.getParameter("roomNumber"));

                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                try {
                    SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                    for (Section section : sectionController.findSectionEntities()) {
                        if (section.getRoom().getRoomPK().getRoomNumber() == roomNumber && section.getRoom().getRoomPK().getBuildingId().equalsIgnoreCase(buildingID)) {
                            throw new Exception();
                        }
                    }
                    roomController.destroy(new RoomPK(buildingID, roomNumber));
                    gen.write("message", "Cannot Delete Room, There are dependencies on this room.");
                } catch (Exception ex) {
                    gen.write("message", "Cannot Delete Room, There are dependencies on this room.");

                }
                gen.writeStartArray("rooms");

                for (Room roomCollection : roomController.findRoomEntities()) {
                    if (roomCollection.getBuilding().getBuildingId().equalsIgnoreCase(buildingID)) {
                        gen.writeStartObject();
                        gen.write("roomNumber", roomCollection.getRoomPK().getRoomNumber());
                        gen.write("labType", roomCollection.getLabType());
                        gen.write("capacity", roomCollection.getCapacity());
                        gen.write("buildingId", roomCollection.getBuilding().getBuildingId());
                        gen.writeEnd();
                    }
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();

            } else {
                String buildingID = request.getParameter("buildingID");
                int roomNumber = Integer.parseInt(request.getParameter("roomNumber"));
                String labType = request.getParameter("labType");
                int roomCapacity = Integer.parseInt(request.getParameter("roomCapacity"));
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                BuildingJpaController buildingController = new BuildingJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));

                if (roomController.findRoom(new RoomPK(buildingID, roomNumber)) == null) {
                    Room room = new Room();

                    room.setBuilding(buildingController.findBuilding(buildingID));
                    room.setRoomPK(new RoomPK(buildingID, roomNumber));
                    room.setLabType(labType);
                    room.setCapacity(roomCapacity);
                    room.setAvailabilityCollection(new ArrayList<Availability>());
                    room.setLabCollection(new ArrayList<Lab>());
                    room.setSectionCollection(new ArrayList<Section>());
                    try {
                        roomController.create(room);

                        gen.write("message", "New Room added to database");
                    } catch (Exception ex) {

                        gen.write("message", "Cannot Create room with this data, Please see the availble rooms data");
                    }
                } else {

                    gen.write("message", "This room is already present in database, Please see the availble rooms data");
                }
                gen.writeEnd();
                gen.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
