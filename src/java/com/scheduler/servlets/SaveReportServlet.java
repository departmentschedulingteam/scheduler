/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.persistence.Availability;
import com.scheduler.persistence.AvailabilityPK;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.AvailabilityJpaController;
import com.scheduler.persistence.controller.LabJpaController;
import com.scheduler.persistence.controller.ProfessorCourseJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class SaveReportServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            if(reqType.equals("deleteReport")){
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                AvailabilityJpaController availabilityController = new AvailabilityJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                ProfessorCourseJpaController professorCourseController = new ProfessorCourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                
                for(Lab lab :labController.findLabEntities()){
                    labController.destroy(lab.getLabFullId());
                }
                
                for(Section section : sectionController.findSectionEntities()){
                    sectionController.destroy(section.getSectionFullId());
                }
                
                for(Availability availability : availabilityController.findAvailabilityEntities()){
                    availabilityController.destroy(availability.getAvailabilityPK());
                }
                
                for(ProfessorCourse professorCourse : professorCourseController.findProfessorCourseEntities()){
                    professorCourseController.destroy(professorCourse.getProfessorCoursePK());
                }
                
                
                gen.close();
            }
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(SaveReportServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
