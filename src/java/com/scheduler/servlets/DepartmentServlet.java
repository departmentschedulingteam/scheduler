/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.persistence.Course;
import com.scheduler.persistence.Department;
import com.scheduler.persistence.controller.DepartmentJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class DepartmentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            DepartmentJpaController departmentController = new DepartmentJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
            if (reqType.equals("getDepartmentList")) {
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                gen.writeStartArray("departments");

                for (Department depart : departmentController.findDepartmentEntities()) {
                    gen.writeStartObject();

                    gen.write("departmentID", depart.getDepartmentId());
                    gen.write("departmentName", depart.getDepartmentName());
                    gen.write("departmentDesc", depart.getDepartmentDescription());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equalsIgnoreCase("destroy")) {
                int departmentID = Integer.parseInt(request.getParameter("departmentID"));

                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                try {
                    departmentController.destroy(departmentID);
                    gen.write("message", "Deleted Successfully");
                } catch (Exception ex) {
                    gen.write("message", "There are courses related, Please delete those courses first");
                }
                gen.writeStartArray("departments");
                for (Department depart : departmentController.findDepartmentEntities()) {
                    gen.writeStartObject();
                    gen.write("departmentID", depart.getDepartmentId());
                    gen.write("departmentName", depart.getDepartmentName());
                    gen.write("departmentDesc", depart.getDepartmentDescription());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else {
                int departmentID = Integer.parseInt(request.getParameter("departmentID"));
                String departmentName = request.getParameter("departmentName");
                String departmentDesc = request.getParameter("departmentDesc");

                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                Department department = new Department();
                department.setDepartmentId(departmentID);
                department.setDepartmentName(departmentName);
                department.setDepartmentDescription(departmentDesc);

                if (departmentController.findDepartment(departmentID) == null && !reqType.equals("editDept")) {
                    try {
                        department.setCourseCollection(new ArrayList<Course>());
                        departmentController.create(department);
                        gen.write("message", "New Discipline added to database");
                    } catch (Exception ex) {
                        gen.write("message", "problem with database");
                    }
                } else if (!reqType.equals("editDept")) {
                    gen.write("message", "This Discipline id is already present in database");
                }

                if (departmentController.findDepartment(departmentID) != null && reqType.equals("editDept")) {
                    try {
                        department.setCourseCollection(departmentController.findDepartment(departmentID).getCourseCollection());
                        departmentController.edit(department);
                        gen.write("message", "Discipline Edited");
                    } catch (Exception ex) {
                        gen.write("message", "problem with database");
                    }
                } else if (reqType.equals("editDept")) {
                    gen.write("message", "Enable to Edit Discipline");
                }

                gen.writeStartArray("departments");
                for (Department depart : departmentController.findDepartmentEntities()) {
                    gen.writeStartObject();
                    gen.write("departmentID", depart.getDepartmentId());
                    gen.write("departmentName", depart.getDepartmentName());
                    gen.write("departmentDesc", depart.getDepartmentDescription());
                    gen.writeEnd();
                }
                gen.writeEnd();
                gen.writeEnd();
                gen.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
