/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.helper.SchedulerHelper;
import com.scheduler.helper.SectionLabConflicts;
import com.scheduler.persistence.Availability;
import com.scheduler.persistence.AvailabilityPK;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.AvailabilityJpaController;
import com.scheduler.persistence.controller.LabJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class LabModifyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            if(reqType.equals("delete")){
                String labID = request.getParameter("labID");
                LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                AvailabilityJpaController availabilityController = new AvailabilityJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                Lab lab = labController.findLab(labID);
                int seatsOccupied = lab.getOccupied();
                for(TimeSlots timeSlot: lab.getTimeSlotsCollection()){
                    AvailabilityPK aPK = new AvailabilityPK(timeSlot.getTimeSlotId(), lab.getRoom().getRoomPK().getBuildingId(), lab.getRoom().getRoomPK().getRoomNumber());
                    Availability availability = availabilityController.findAvailability(aPK);
                    if((availability.getSeatsAvailability()-seatsOccupied)<=0){
                        //availability.setSeatsAvailability(availability.getSeatsAvailability()-seatsOccupied);
                        availabilityController.destroy(aPK);
                    }else{
                        availability.setSeatsAvailability(availability.getSeatsAvailability()-seatsOccupied);
                        availabilityController.edit(availability);
                    }
                    
                }
                labController.destroy(lab.getLabFullId());
                
            } else if(reqType.equals("getLabDetails")){
                
                String labID = request.getParameter("labID");
                System.out.println("In get Section Detais: "+labID);
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                Lab lab = labController.findLab(labID);
                gen.writeStartArray("labDetails");
                    if (lab!=null) {
                        gen.writeStartObject();
                        gen.write("labFullID", lab.getLabFullId());
                        gen.write("departmentID", lab.getSectionFullId().getProfessorCourse().getProfessorCoursePK().getDepartrmentId());
                        gen.write("departmentName", lab.getSectionFullId().getProfessorCourse().getCourse().getDepartment().getDepartmentName());
                        gen.write("courseID", lab.getSectionFullId().getProfessorCourse().getProfessorCoursePK().getCourseId());
                        gen.write("courseName", lab.getSectionFullId().getProfessorCourse().getCourse().getCourseName());
                        gen.write("sectionID", lab.getSectionFullId().getSectionId());
                        gen.write("professorID", lab.getSectionFullId().getProfessorCourse().getProfessor().getProfessorId());
                        gen.write("professorName", lab.getSectionFullId().getProfessorCourse().getProfessor().getFirstName() + " " + lab.getSectionFullId().getProfessorCourse().getProfessor().getLastName());
                        gen.write("buildingID", lab.getRoom().getRoomPK().getBuildingId());
                        gen.write("roomID", lab.getRoom().getRoomPK().getRoomNumber());
                        gen.write("seats", lab.getOccupied());
                        SchedulerHelper schedulerHelper = new SchedulerHelper();
                        gen.write("timeSlots", schedulerHelper.getTimeSlotsString(lab.getTimeSlotsCollection()));
                        gen.writeEnd();
                    }else{
                        gen.write("message", "No labs Found");
                    }
                    gen.writeEnd();
                gen.writeEnd();
                gen.close();
            } else if (reqType.equals("editLab")) {
                String labFullID = request.getParameter("labFullID");
                String buildingID = request.getParameter("buildingID");
                int room = Integer.parseInt(request.getParameter("room"));
                int seats = Integer.parseInt(request.getParameter("seats"));
                String st = request.getParameter("startTime");
                String en = request.getParameter("endTime");
                String sectionDay[] = request.getParameterValues("sectionDay");
                String[] startTime = new String[sectionDay.length];
                String[] endTime = new String[sectionDay.length];
                for (int i = 0; i < sectionDay.length; i++) {
                    startTime[i] = st;
                    endTime[i] = en;
                }
                String allow = request.getParameter("allow");
                SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                Lab lab = labController.findLab(labFullID);
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                SchedulerHelper schedulerHelper = new SchedulerHelper();
                HashMap hMap = schedulerHelper.refineEditLab(lab, buildingID, room, seats, startTime, endTime, sectionDay, allow);
                String allowOrDeny = "Allow";
                System.out.println("allow string: " + allow);
                if (!allow.equalsIgnoreCase("deny") || allow.equalsIgnoreCase("allow")) {
                    gen.writeStartArray("scheduler");
                    if (hMap.containsKey("conflicts")) {
                        for (SectionLabConflicts secLabConflicts : (List<SectionLabConflicts>) (Object) hMap.get("conflicts")) {
                            if (secLabConflicts.getConflict().equalsIgnoreCase("Hard Conflict")) {
                                allowOrDeny = "Deny";
                            }
                            if (!secLabConflicts.isIsLab()) {
                                Section sec = sectionController.findSection(secLabConflicts.getID());
                                gen.writeStartObject();
                                gen.write("sectionLabFullID", sec.getSectionFullId());
                                gen.write("professor", sec.getProfessorCourse().getProfessor().getLastName() + " " + sec.getProfessorCourse().getProfessor().getFirstName());
                                gen.write("buildingRoom", sec.getRoom().getBuilding().getBuildingId() + "" + sec.getRoom().getRoomPK().getRoomNumber());
                                gen.write("conflictType", secLabConflicts.getConflict());

                                gen.write("timeSlots", schedulerHelper.getTimeSlotsString(sec.getTimeSlotsCollection()));
                                gen.writeEnd();
                            } else {
                                Lab labObj = labController.findLab(secLabConflicts.getID());
                                gen.writeStartObject();
                                gen.write("sectionLabFullID", labObj.getLabFullId());
                                gen.write("professor", labObj.getSectionFullId().getProfessorCourse().getProfessor().getLastName() + " " + labObj.getSectionFullId().getProfessorCourse().getProfessor().getFirstName());
                                gen.write("buildingRoom", labObj.getRoom().getBuilding().getBuildingId() + "" + labObj.getRoom().getRoomPK().getRoomNumber());
                                gen.write("conflictType", secLabConflicts.getConflict());

                                gen.write("timeSlots", schedulerHelper.getTimeSlotsString(lab.getTimeSlotsCollection()));
                                gen.writeEnd();
                            }
                        }
                    }
                    gen.writeEnd();
                }
                List<SectionLabConflicts> sectionLabConflictsList = (List<SectionLabConflicts>) hMap.get("conflicts");
                if (!sectionLabConflictsList.isEmpty() && !allow.equalsIgnoreCase("allow")) {
                    gen.write("allowOrDeny", allowOrDeny);
                    gen.write("message", "Conflicts detected with");
                    gen.writeEnd();
                } else if (hMap.containsKey("availability")) {
                    allowOrDeny = "Deny";
                    gen.write("allowOrDeny", allowOrDeny);
                    gen.write("message", (String) hMap.get("availability"));
                    gen.writeEnd();
                } else {
                    gen.write("message", "Lab Edited Successfully");
                    gen.writeEnd();
                }
                gen.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(LabModifyServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
