/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.servlets;

import com.scheduler.helper.SchedulerHelper;
import com.scheduler.helper.SectionLabConflicts;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.RoomPK;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.LabJpaController;
import com.scheduler.persistence.controller.RoomJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S519479
 */
public class LabServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String reqType = request.getParameter("reqType");
            int departmentID = Integer.parseInt(request.getParameter("departmentID"));
            int course = Integer.parseInt(request.getParameter("course"));
            String sectionFullID = request.getParameter("sectionFullID");
            String buildingID = request.getParameter("buildingID");
            int room = Integer.parseInt(request.getParameter("room"));
            int seats = Integer.parseInt(request.getParameter("seats"));
            String st = request.getParameter("startTime");
            String en = request.getParameter("endTime");
            String sectionDay[] = request.getParameterValues("sectionDay");
            String[] startTime = new String[sectionDay.length];
            String[] endTime = new String[sectionDay.length];
            for(int i=0;i<sectionDay.length;i++){
                startTime[i] = st;
                endTime[i] = en;
            }
            String allow = request.getParameter("allow");
            String sectionIDString = "";
            if (reqType.equals("insertLab")) { 
                SchedulerHelper schedulerHelper = new SchedulerHelper();
                SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                Room roomObj = (new RoomJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"))).findRoom(new RoomPK(buildingID, room));
                List<TimeSlots> requestedTimeSlots = schedulerHelper.findTimeSlots(startTime, endTime, sectionDay);
                String labFullID = "*"+sectionFullID+"*";
                Section secObj = sectionController.findSection(sectionFullID);
                JsonGenerator gen = Json.createGenerator(out);
                gen.writeStartObject();
                if (labController.findLab(labFullID) == null) {
                    HashMap hMap = schedulerHelper.refineLab(labFullID, secObj, secObj.getProfessorCourse(), roomObj, seats, requestedTimeSlots, allow);
                    String allowOrDeny = "Allow";
                    System.out.println("allow string: "+allow);
                    if (!allow.equalsIgnoreCase("deny") || allow.equalsIgnoreCase("allow")) {
                        gen.writeStartArray("scheduler");
                        if (hMap.containsKey("conflicts")) {
                            for (SectionLabConflicts secLabConflicts : (List<SectionLabConflicts>) (Object) hMap.get("conflicts")) {
                                if(secLabConflicts.getConflict().equalsIgnoreCase("Hard Conflict"))
                                    allowOrDeny="Deny";
                                if (!secLabConflicts.isIsLab()) {
                                    Section sec = sectionController.findSection(secLabConflicts.getID());
                                    gen.writeStartObject();
                                    gen.write("sectionLabFullID", sec.getSectionFullId());
                                    gen.write("professor", sec.getProfessorCourse().getProfessor().getLastName() + " " + sec.getProfessorCourse().getProfessor().getFirstName());
                                    gen.write("buildingRoom", sec.getRoom().getBuilding().getBuildingId() +""+sec.getRoom().getRoomPK().getRoomNumber());
                                    gen.write("conflictType", secLabConflicts.getConflict());

                                    String slots = "", start = "", end = "";
                                    Collection<TimeSlots> s = sec.getTimeSlotsCollection();
                                    for (TimeSlots timeSlots : s) {
                                        if (!slots.contains(timeSlots.getDay())) {
                                            slots += timeSlots.getDay() + " ";

                                        }
                                        if (start.isEmpty()) {
                                            start = timeSlots.getStartTime();
                                        } else {
                                            System.out.println(timeSlots.getStartTime());
                                            int startInt = Integer.parseInt(timeSlots.getStartTime());
                                            if (startInt < Integer.parseInt(start)) {
                                                start = timeSlots.getStartTime();
                                            }
                                        }
                                        if (end.isEmpty()) {
                                            end = timeSlots.getEndTime();
                                        } else {
                                            int startInt = Integer.parseInt(timeSlots.getEndTime());
                                            if (startInt > Integer.parseInt(start)) {
                                                end = timeSlots.getEndTime();
                                            }
                                        }

                                    }
                                    gen.write("timeSlots", slots + " " + start + " " + end);
                                    gen.writeEnd();
                                }else{
                                    Lab lab = labController.findLab(secLabConflicts.getID());
                                    gen.writeStartObject();
                                    gen.write("sectionLabFullID", lab.getLabFullId());
                                    gen.write("professor", lab.getSectionFullId().getProfessorCourse().getProfessor().getLastName() + " " + lab.getSectionFullId().getProfessorCourse().getProfessor().getFirstName());
                                    gen.write("buildingRoom", lab.getRoom().getBuilding().getBuildingId() +""+lab.getRoom().getRoomPK().getRoomNumber());
                                    gen.write("conflictType", secLabConflicts.getConflict());

                                    String slots = "", start = "", end = "";
                                    Collection<TimeSlots> s = lab.getTimeSlotsCollection();
                                    for (TimeSlots timeSlots : s) {
                                        if (!slots.contains(timeSlots.getDay())) {
                                            slots += timeSlots.getDay() + " ";

                                        }
                                        if (start.isEmpty()) {
                                            start = timeSlots.getStartTime();
                                        } else {
                                            System.out.println(timeSlots.getStartTime());
                                            int startInt = Integer.parseInt(timeSlots.getStartTime());
                                            if (startInt < Integer.parseInt(start)) {
                                                start = timeSlots.getStartTime();
                                            }
                                        }
                                        if (end.isEmpty()) {
                                            end = timeSlots.getEndTime();
                                        } else {
                                            int startInt = Integer.parseInt(timeSlots.getEndTime());
                                            if (startInt > Integer.parseInt(start)) {
                                                end = timeSlots.getEndTime();
                                            }
                                        }

                                    }
                                    gen.write("timeSlots", slots + " " + start + " " + end);
                                    gen.writeEnd();
                                }
                            }
                        }
                        gen.writeEnd();
                    }
                    List<SectionLabConflicts> sectionLabConflictsList = (List<SectionLabConflicts>) hMap.get("conflicts");
                    if (!sectionLabConflictsList.isEmpty() && !allow.equalsIgnoreCase("allow")) {
                        gen.write("allowOrDeny", allowOrDeny);
                        gen.write("message", "Conflicts detected with");
                        gen.writeEnd();
                    } else if (hMap.containsKey("availability")){
                        allowOrDeny = "Deny";
                        gen.write("allowOrDeny", allowOrDeny);
                        gen.write("message", (String)hMap.get("availability"));
                        gen.writeEnd();
                    }else {
                        gen.write("message", "Lab Created Successfully");
                        gen.writeEnd();
                    }
                    
                } else {
                    sectionIDString = "Lab already Present";
                    gen.write("message", "This lab is already present in database");
                    gen.writeEnd();
                }
                //gen.writeEnd();

                
                gen.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
