/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.helper;

/**
 *
 * @author S519479
 */
public class ReportHelperBean {
    
    String professorName;
    String professorID;
    String professorCode;
    String sectionFullID;
    String buildingRoom;

    public String getProfessorName() {
        return professorName;
    }

    public void setProfessorName(String professorName) {
        this.professorName = professorName;
    }

    public String getProfessorID() {
        return professorID;
    }

    public void setProfessorID(String professorID) {
        this.professorID = professorID;
    }

    public String getProfessorCode() {
        return professorCode;
    }

    public void setProfessorCode(String professorCode) {
        this.professorCode = professorCode;
    }

    public String getSectionFullID() {
        return sectionFullID;
    }

    public void setSectionFullID(String sectionFullID) {
        this.sectionFullID = sectionFullID;
    }

    public String getBuildingRoom() {
        return buildingRoom;
    }

    public void setBuildingRoom(String buildingRoom) {
        this.buildingRoom = buildingRoom;
    }

        
}
