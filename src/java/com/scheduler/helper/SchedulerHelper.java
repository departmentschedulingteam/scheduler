/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.helper;

import com.scheduler.persistence.Availability;
import com.scheduler.persistence.AvailabilityPK;
import com.scheduler.persistence.CoursePK;
import com.scheduler.persistence.Lab;
import com.scheduler.persistence.ProfessorCourse;
import com.scheduler.persistence.ProfessorCoursePK;
import com.scheduler.persistence.Room;
import com.scheduler.persistence.RoomPK;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.AvailabilityJpaController;
import com.scheduler.persistence.controller.LabJpaController;
import com.scheduler.persistence.controller.ProfessorCourseJpaController;
import com.scheduler.persistence.controller.RoomJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import com.scheduler.persistence.controller.TimeSlotsJpaController;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Persistence;

/**
 *
 * @author S519479
 */
public class SchedulerHelper {

    public HashMap refineSection(String sectionFullID, int departmentID, int course, int sectionID, String professorID, String buildingID,
            int room, int seats, String[] startTime, String[] endTime, String[] sectionDay, String allow) {
        HashMap hMap = new HashMap();
        List<TimeSlots> requestedTimeSlots = findTimeSlots(startTime, endTime, sectionDay);
        Room roomObj = (new RoomJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"))).findRoom(new RoomPK(buildingID, room));
        ProfessorCourse professorCourseObj = (new ProfessorCourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"))).findProfessorCourse(new ProfessorCoursePK(departmentID, course, professorID));
        List<SectionLabConflicts> sectionLabConflictsList = getSectionLabConflictsList(requestedTimeSlots, roomObj, professorCourseObj, false);

        if (sectionLabConflictsList.isEmpty() || allow.equalsIgnoreCase("allow")) {
            if (checkAvailableSeats(requestedTimeSlots, roomObj, seats) >= seats) {
                try {
                    insertIntoSection(sectionFullID, sectionID, professorCourseObj, roomObj, seats, requestedTimeSlots);
                    insertIntoAvailability(requestedTimeSlots, roomObj, seats);
                } catch (Exception ex) {
                    Logger.getLogger(SchedulerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("No sufficient seats available");
                System.out.println("Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
                hMap.put("availability", "Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
            }
        }
        hMap.put("conflicts", sectionLabConflictsList);
        return hMap;
    }
    
    public List<TimeSlots> filterEditableTimeSlots(List<TimeSlots> newTimeSlots, List<TimeSlots> oldTimeSlots){
        List<TimeSlots> newList = new ArrayList<TimeSlots>();
        for(TimeSlots tSlot : newTimeSlots){
            if(!oldTimeSlots.contains(tSlot)){
                newList.add(tSlot);
            }
        }
        return newList;
    }
    
    public void editSectionAvailability(Section section) throws NonexistentEntityException, Exception{
        AvailabilityJpaController availabilityController = new AvailabilityJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        for (TimeSlots timeSlot : section.getTimeSlotsCollection()) {
            AvailabilityPK aPK = new AvailabilityPK(timeSlot.getTimeSlotId(), section.getRoom().getRoomPK().getBuildingId(), section.getRoom().getRoomPK().getRoomNumber());
            Availability availability = availabilityController.findAvailability(aPK);
            if ((availability.getSeatsAvailability() - section.getOccupied()) <= 0) {
                availabilityController.destroy(aPK);
            } else {
                availability.setSeatsAvailability(availability.getSeatsAvailability() - section.getOccupied());
                availabilityController.edit(availability);
            }
        }
    }
    
    public void editSection(Section section, Room roomObj, int seats, List<TimeSlots> requestedTimeSlots) throws Exception{
        section.setRoom(roomObj);
        section.setOccupied(seats);
        section.setTimeSlotsCollection(requestedTimeSlots);
        SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        sectionController.edit(section);
    }
    
    public HashMap refineEditSection(Section section, String buildingID, int room, int seats, String[] startTime, String[] endTime, String[] sectionDay, String allow) {
        HashMap hMap = new HashMap();
        List<TimeSlots> requestedTimeSlots = findTimeSlots(startTime, endTime, sectionDay);
        requestedTimeSlots = filterEditableTimeSlots(requestedTimeSlots, (List<TimeSlots>) section.getTimeSlotsCollection());
        Room roomObj = (new RoomJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"))).findRoom(new RoomPK(buildingID, room));
        List<SectionLabConflicts> sectionLabConflictsList = getSectionLabConflictsList(requestedTimeSlots, roomObj, section.getProfessorCourse(), false);

        if (sectionLabConflictsList.isEmpty() || allow.equalsIgnoreCase("allow")) {
            if (checkAvailableSeats(requestedTimeSlots, roomObj, seats) >= seats) {
                try {
                    editSectionAvailability(section);
                    requestedTimeSlots = findTimeSlots(startTime, endTime, sectionDay);
                    editSection(section, roomObj, seats, requestedTimeSlots);
                    insertIntoAvailability(requestedTimeSlots, roomObj, seats);
                } catch (Exception ex) {
                    Logger.getLogger(SchedulerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("No sufficient seats available");
                System.out.println("Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
                hMap.put("availability", "Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
            }
        }
        hMap.put("conflicts", sectionLabConflictsList);
        return hMap;
    }

    public HashMap refineLab(String labFullID, Section sectionObj, ProfessorCourse professorCourseObj, Room roomObj, int seats,
            List<TimeSlots> requestedTimeSlots, String allow) {
        HashMap hMap = new HashMap();
        List<SectionLabConflicts> sectionLabConflictsList = getSectionLabConflictsList(requestedTimeSlots, roomObj, professorCourseObj, true);

        if (sectionLabConflictsList.isEmpty() || allow.equalsIgnoreCase("allow")) {
            if (checkAvailableSeats(requestedTimeSlots, roomObj, seats) >= seats) {
                try {
                    insertIntoLab(labFullID, sectionObj, roomObj, seats, requestedTimeSlots);
                    insertIntoAvailability(requestedTimeSlots, roomObj, seats);
                } catch (Exception ex) {
                    Logger.getLogger(SchedulerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("No sufficient seats available");
                System.out.println("Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
                hMap.put("availability", "Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
            }
        }
        hMap.put("conflicts", sectionLabConflictsList);
        return hMap;
    }
    
    public void editLabAvailability(Lab lab) throws NonexistentEntityException, Exception{
        AvailabilityJpaController availabilityController = new AvailabilityJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        for (TimeSlots timeSlot : lab.getTimeSlotsCollection()) {
            AvailabilityPK aPK = new AvailabilityPK(timeSlot.getTimeSlotId(), lab.getRoom().getRoomPK().getBuildingId(), lab.getRoom().getRoomPK().getRoomNumber());
            Availability availability = availabilityController.findAvailability(aPK);
            if ((availability.getSeatsAvailability() - lab.getOccupied()) <= 0) {
                availabilityController.destroy(aPK);
            } else {
                availability.setSeatsAvailability(availability.getSeatsAvailability() - lab.getOccupied());
                availabilityController.edit(availability);
            }
        }
    }
    
    public void editLab(Lab lab, Room roomObj, int seats, List<TimeSlots> requestedTimeSlots) throws Exception{
        lab.setRoom(roomObj);
        lab.setOccupied(seats);
        lab.setTimeSlotsCollection(requestedTimeSlots);
        LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        labController.edit(lab);
    }
    
    public HashMap refineEditLab(Lab lab, String buildingID, int room, int seats, String[] startTime, String[] endTime, String[] sectionDay, String allow) {
        HashMap hMap = new HashMap();
        List<TimeSlots> requestedTimeSlots = findTimeSlots(startTime, endTime, sectionDay);
        requestedTimeSlots = filterEditableTimeSlots(requestedTimeSlots, (List<TimeSlots>) lab.getTimeSlotsCollection());
        Room roomObj = (new RoomJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"))).findRoom(new RoomPK(buildingID, room));
        List<SectionLabConflicts> sectionLabConflictsList = getSectionLabConflictsList(requestedTimeSlots, roomObj, lab.getSectionFullId().getProfessorCourse(), false);

        if (sectionLabConflictsList.isEmpty() || allow.equalsIgnoreCase("allow")) {
            if (checkAvailableSeats(requestedTimeSlots, roomObj, seats) >= seats) {
                try {
                    editLabAvailability(lab);
                    requestedTimeSlots = findTimeSlots(startTime, endTime, sectionDay);
                    editLab(lab, roomObj, seats, requestedTimeSlots);
                    insertIntoAvailability(requestedTimeSlots, roomObj, seats);
                } catch (Exception ex) {
                    Logger.getLogger(SchedulerHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("No sufficient seats available");
                System.out.println("Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
                hMap.put("availability", "Available seats:- " + checkAvailableSeats(requestedTimeSlots, roomObj, seats));
            }
        }
        hMap.put("conflicts", sectionLabConflictsList);
        return hMap;
    }

    private void insertIntoSection(String sectionFullID, int sectionID, ProfessorCourse professorCourseObj, Room roomObj,
            int seats, List<TimeSlots> requestedTimeSlots) {
        SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        Section section = new Section();
        section.setSectionFullId(sectionFullID);
        section.setSectionId(sectionID);
        section.setProfessorCourse(professorCourseObj);
        section.setRoom(roomObj);
        section.setOccupied(seats);
        section.setTimeSlotsCollection(requestedTimeSlots);
        try {
            sectionController.create(section);
        } catch (Exception ex) {
            Logger.getLogger(SchedulerHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void insertIntoAvailability(List<TimeSlots> requestedTimeSlots, Room roomObj, int seats) throws Exception {
        AvailabilityJpaController availabilityController = new AvailabilityJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        for (TimeSlots timeSlot : requestedTimeSlots) {
            Availability availability = availabilityController.findAvailability(new AvailabilityPK(timeSlot.getTimeSlotId(), roomObj.getRoomPK().getBuildingId(), roomObj.getRoomPK().getRoomNumber()));
            if (availability != null) {
                if ((roomObj.getCapacity() - availability.getSeatsAvailability()) >= seats) {
                    availability.setSeatsAvailability(availability.getSeatsAvailability() + seats);
                    availabilityController.edit(availability);
                } else {
                    System.out.println("Exception in insertIntoAvailability: exceeds available seats");
                    throw new Exception();
                }
            } else {
                availability = new Availability();
                if (roomObj.getCapacity() >= seats) {
                    availability.setSeatsAvailability(seats);
                    availability.setTimeSlots(timeSlot);
                    availability.setRoom(roomObj);
                    availability.setAvailabilityPK(new AvailabilityPK(timeSlot.getTimeSlotId(), roomObj.getRoomPK().getBuildingId(), roomObj.getRoomPK().getRoomNumber()));

                    availabilityController.create(availability);
                } else {
                    System.out.println("Exception in insertIntoAvailability: exceeds capacity");
                    throw new Exception();
                }
            }
        }
    }

    private int checkAvailableSeats(List<TimeSlots> requestedTimeSlots, Room roomObj, int seats) {
        AvailabilityJpaController availabilityController = JpaControllerHelper.availabilityController;
        for (TimeSlots timeSlot : requestedTimeSlots) {
            Availability availability = availabilityController.findAvailability(new AvailabilityPK(timeSlot.getTimeSlotId(), roomObj.getRoomPK().getBuildingId(), roomObj.getRoomPK().getRoomNumber()));
            if (availability != null) {
                return roomObj.getCapacity() - availability.getSeatsAvailability();
            } else {
                return roomObj.getCapacity();
            }
        }
        return roomObj.getCapacity();
    }

    private boolean gradUgradCourse(ProfessorCourse fromDBSection, ProfessorCourse current) {

        int currentValue = 0;
        ArrayList<Integer> hardCodeCourses = new ArrayList<>();
        hardCodeCourses.add(443);
        hardCodeCourses.add(643);
        hardCodeCourses.add(460);
        hardCodeCourses.add(660);
        hardCodeCourses.add(644);
        hardCodeCourses.add(444);
        if (hardCodeCourses.contains(current.getCourse().getCoursePK().getCourseId())) {
            if (current.getCourse().getCoursePK().getCourseId() > 400 && current.getCourse().getCoursePK().getCourseId() < 500) {
                currentValue = current.getCourse().getCoursePK().getCourseId() + 200;
            } else if (current.getCourse().getCoursePK().getCourseId() > 600 && current.getCourse().getCoursePK().getCourseId() < 700) {
                currentValue = current.getCourse().getCoursePK().getCourseId() - 200;
            }

            if ((fromDBSection.getCourse()
                    .getCoursePK().getCourseId() == currentValue)
                    && (fromDBSection.getCourse().getCoursePK().getDepartmentId() == current.getCourse().getCoursePK().getDepartmentId())
                    && (fromDBSection.getProfessor().getProfessorId().equalsIgnoreCase(current.getProfessor().getProfessorId()))) {
                System.out.println("returning true");
                return true;
            } else {
                System.out.println("returning false");
                return false;
            }
        } else {
            return false;
        }

    }

    private List<SectionLabConflicts> getSectionLabConflictsList(List<TimeSlots> requestedTimeSlots, Room room, ProfessorCourse professorCourse, boolean isLab) {
        List<Section> sectionCompleteList = (new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"))).findSectionEntities();
        List<Lab> labCompleteList = (new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"))).findLabEntities();
        List<SectionLabConflicts> sectionLabConflictsList = new ArrayList<>();
        SectionLabConflicts sectionLabConflicts;
        for (Section section : sectionCompleteList) {
            gradUgradCourse(section.getProfessorCourse(), professorCourse);
            if (section.getRoom().equals(room)) {
                if (checkCommonList(section.getTimeSlotsCollection(), requestedTimeSlots) && section.getProfessorCourse().equals(professorCourse)) {
                    if (isLab) {
                        sectionLabConflicts = new SectionLabConflicts(section.getSectionFullId(), "Hard Conflict", false);
                    } else {
                        sectionLabConflicts = new SectionLabConflicts(section.getSectionFullId(), "Soft Conflict", false);
                    }
                    sectionLabConflictsList.add(sectionLabConflicts);
                } else if (!Collections.disjoint(section.getTimeSlotsCollection(), requestedTimeSlots) && gradUgradCourse(section.getProfessorCourse(), professorCourse)) {
                    sectionLabConflicts = new SectionLabConflicts(section.getSectionFullId(), "Soft Conflict", false);
                    sectionLabConflictsList.add(sectionLabConflicts);
                } else if (!Collections.disjoint(section.getTimeSlotsCollection(), requestedTimeSlots)){
                    sectionLabConflicts = new SectionLabConflicts(section.getSectionFullId(), "Hard Conflict", false);
                    sectionLabConflictsList.add(sectionLabConflicts);
                }
            }
        }
        for (Lab lab : labCompleteList) {
            if (lab.getRoom().equals(room)) {
                if (checkCommonList(lab.getTimeSlotsCollection(), requestedTimeSlots) && lab.getSectionFullId().getProfessorCourse().equals(professorCourse)) {
                    if (!isLab) {
                        sectionLabConflicts = new SectionLabConflicts(lab.getLabFullId(), "Hard Conflict", true);
                    } else {
                        sectionLabConflicts = new SectionLabConflicts(lab.getLabFullId(), "Soft Conflict", true);
                    }
                    sectionLabConflictsList.add(sectionLabConflicts);
                } else if (!Collections.disjoint(lab.getTimeSlotsCollection(), requestedTimeSlots) && gradUgradCourse(lab.getSectionFullId().getProfessorCourse(), professorCourse)) {
                    sectionLabConflicts = new SectionLabConflicts(lab.getLabFullId(), "Soft Conflict", false);
                    sectionLabConflictsList.add(sectionLabConflicts);
                } else if (!Collections.disjoint(lab.getTimeSlotsCollection(), requestedTimeSlots)) {
                    sectionLabConflicts = new SectionLabConflicts(lab.getLabFullId(), "Hard Conflict", true);
                    sectionLabConflictsList.add(sectionLabConflicts);
                }
            }
        }
        return sectionLabConflictsList;
    }

    private boolean checkCommonList(Collection a, Collection b) {
        return a.containsAll(b) && b.containsAll(a);
    }

    public List<TimeSlots> findTimeSlots(String[] startTime, String[] endTime, String[] sectionDay) {
        int[] startTimeInt = new int[startTime.length];
        int[] endTimeInt = new int[endTime.length];
        for (int i = 0; i < startTime.length; i++) {
            startTime[i] = startTime[i].replace(":", "");
            startTimeInt[i] = Integer.parseInt(startTime[i]);
        }
        for (int i = 0; i < endTime.length; i++) {
            endTime[i] = endTime[i].replace(":", "");
            endTimeInt[i] = Integer.parseInt(endTime[i]);
        }
        List<TimeSlots> requestedTimeSlots = getRequestedTimeSlots(startTimeInt, endTimeInt, sectionDay);
        return requestedTimeSlots;
    }

    private List<TimeSlots> getRequestedTimeSlots(int[] startTime, int[] endTime, String[] sectionDay) {

        List<TimeSlots> timeSlotsList = new ArrayList<>();
        TimeSlotsJpaController timeSlotsController = new TimeSlotsJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));

        if (startTime.length == endTime.length && startTime.length == sectionDay.length) {
            for (int i = 0; i < startTime.length; i++) {
                for (TimeSlots tim : timeSlotsController.findTimeSlotsEntities()) {
                    //System.out.println("st value: "+st+"et value :"+et+" start time: "+tim.getStartTime()+ " end time : "+tim.getEndTime()+" timday: "+tim.getDay()+" day : "+day);
                    if ((Integer.parseInt(tim.getStartTime()) >= startTime[i]) && (Integer.parseInt(tim.getEndTime()) <= endTime[i])
                            && tim.getDay().equals(sectionDay[i])) {
                        timeSlotsList.add(tim);
                    }
                }
            }
        }

        return timeSlotsList;
    }

    private void insertIntoLab(String labFullID, Section sectionObj, Room roomObj, int seats, List<TimeSlots> requestedTimeSlots) {
        LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        Lab lab = new Lab();
        lab.setLabFullId(labFullID);
        lab.setSectionFullId(sectionObj);
        lab.setRoom(roomObj);
        lab.setOccupied(seats);
        lab.setTimeSlotsCollection(requestedTimeSlots);
        try {
            labController.create(lab);

        } catch (Exception ex) {
            Logger.getLogger(SchedulerHelper.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getTimeSlotsString(Collection<TimeSlots> s) {
        String slots = "", start = "", end = "";
        for (TimeSlots timeSlots : s) {
            if (!slots.contains(timeSlots.getDay())) {
                slots += timeSlots.getDay() + " ";

            }
            if (start.isEmpty()) {
                start = timeSlots.getStartTime();
            } else {
                int startInt = Integer.parseInt(timeSlots.getStartTime());
                if (startInt < Integer.parseInt(start)) {
                    start = timeSlots.getStartTime();
                }
            }
            if (end.isEmpty()) {
                end = timeSlots.getEndTime();
            } else {
                int startInt = Integer.parseInt(timeSlots.getEndTime());
                if (startInt > Integer.parseInt(start)) {
                    end = timeSlots.getEndTime();
                }
            }

        }
        return slots + "" + start + " " + end;
    }

}
