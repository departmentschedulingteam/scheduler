/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.helper;

import com.scheduler.persistence.Lab;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.TimeSlotsJpaController;
import java.util.ArrayList;
import java.util.HashMap;
import javax.persistence.Persistence;

/**
 *
 * @author S519479
 */
public class ReportHelper {
    
    public ArrayList<HashMap> getReport(){
        ArrayList arr = new ArrayList();
        HashMap tMap = null;
        String timeSlots[] = {"08:00 - 08:30","08:30 - 09:00","09:00 - 09:30","09:30 - 10:00",
        "10:00 - 10:30","10:30 - 11:00","11:00 - 11:30","11:30 - 12:00","12:00 - 12:30","12:30 - 13:00",
        "13:00 - 13:30","13:30 - 14:00","14:00 - 14:30","14:30 - 15:00","15:00 - 15:30","15:30 - 16:00",
        "16:00 - 16:30","16:30 - 17:00","17:00 - 17:30","17:30 - 18:00","18:00 - 18:30","18:30 - 19:00",
        "19:00 - 19:30","19:30 - 20:00","20:00 - 20:30"};
        for(int i=0;i<timeSlots.length;i++){
            tMap = new HashMap();
            tMap.put(timeSlots[i], getTimeSlotArrayObjectsWithData((i*7)+1, (i+1)*7));
            arr.add(tMap);
        }
        return arr;
    }
    
    public ArrayList<ArrayList> getTimeSlotArrayObjectsWithData(int start, int end){
        ArrayList arr = new ArrayList();
        for(int i=start;i<=end;i++){
            arr.add(getPresiceData(i));
        }
        return arr;
    }
    
    public ArrayList<ReportHelperBean> getPresiceData(int id){
        ArrayList arr = new ArrayList();
        ReportHelperBean rBean = null;
        TimeSlotsJpaController timeSlotsController = new TimeSlotsJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        if (timeSlotsController.findTimeSlots(id) != null) {
            for (Section section : timeSlotsController.findTimeSlots(id).getSectionCollection()) {
                rBean = new ReportHelperBean();
                rBean.setBuildingRoom(section.getRoom().getRoomPK().getBuildingId() + "" + section.getRoom().getRoomPK().getRoomNumber());
                rBean.setProfessorCode(section.getProfessorCourse().getProfessor().getProfessorInitials());
                rBean.setProfessorID(section.getProfessorCourse().getProfessor().getProfessorId());
                rBean.setProfessorName(section.getProfessorCourse().getProfessor().getFirstName() + "" + section.getProfessorCourse().getProfessor().getLastName());
                rBean.setSectionFullID(section.getSectionFullId());
                arr.add(rBean);
            }
        }
        if (timeSlotsController.findTimeSlots(id) != null) {
            for (Lab lab : timeSlotsController.findTimeSlots(id).getLabCollection()) {
                rBean = new ReportHelperBean();
                rBean.setBuildingRoom(lab.getRoom().getRoomPK().getBuildingId() + "" + lab.getRoom().getRoomPK().getRoomNumber());
                rBean.setProfessorCode(lab.getSectionFullId().getProfessorCourse().getProfessor().getProfessorInitials());
                rBean.setProfessorID(lab.getSectionFullId().getProfessorCourse().getProfessor().getProfessorId());
                rBean.setProfessorName(lab.getSectionFullId().getProfessorCourse().getProfessor().getFirstName() + "" + lab.getSectionFullId().getProfessorCourse().getProfessor().getLastName());
                rBean.setSectionFullID(lab.getLabFullId());
                arr.add(rBean);
            }
        }
        return arr;
    }
    
}
