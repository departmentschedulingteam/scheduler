/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.helper;

import com.scheduler.persistence.controller.AvailabilityJpaController;
import com.scheduler.persistence.controller.BuildingJpaController;
import com.scheduler.persistence.controller.CourseJpaController;
import com.scheduler.persistence.controller.DepartmentJpaController;
import com.scheduler.persistence.controller.LabJpaController;
import com.scheduler.persistence.controller.LegacyReportJpaController;
import com.scheduler.persistence.controller.ProfessorCourseJpaController;
import com.scheduler.persistence.controller.ProfessorJpaController;
import com.scheduler.persistence.controller.RoomJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import com.scheduler.persistence.controller.TimeSlotsJpaController;
import javax.persistence.Persistence;

/**
 *
 * @author S519479
 */
public class JpaControllerHelper {
    
    public static AvailabilityJpaController availabilityController = new AvailabilityJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static BuildingJpaController buildingController = new BuildingJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static CourseJpaController courseController = new CourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static DepartmentJpaController departmentController = new DepartmentJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static LegacyReportJpaController legacyReportController = new LegacyReportJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static ProfessorCourseJpaController professorCourseController = new ProfessorCourseJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static ProfessorJpaController professorController = new ProfessorJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static RoomJpaController roomController = new RoomJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
    public static TimeSlotsJpaController timeSlotsController = new TimeSlotsJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
}
