/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.helper;

/**
 *
 * @author S519479
 */
public class SectionLabConflicts {

    public SectionLabConflicts(String ID, String conflict, boolean isLab) {
        this.ID = ID;
        this.conflict = conflict;
        this.isLab = isLab;
    }    
    
    private String ID;
    private String conflict;
    private boolean isLab;

    public boolean isIsLab() {
        return isLab;
    }

    public void setIsLab(boolean isLab) {
        this.isLab = isLab;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getConflict() {
        return conflict;
    }

    public void setConflict(String conflict) {
        this.conflict = conflict;
    }
    
}
