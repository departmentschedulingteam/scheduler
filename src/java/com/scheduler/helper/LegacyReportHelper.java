/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.helper;

import com.scheduler.persistence.Lab;
import com.scheduler.persistence.LegacyReport;
import com.scheduler.persistence.LegacyReportPK;
import com.scheduler.persistence.Section;
import com.scheduler.persistence.TimeSlots;
import com.scheduler.persistence.controller.LabJpaController;
import com.scheduler.persistence.controller.LegacyReportJpaController;
import com.scheduler.persistence.controller.SectionJpaController;
import com.scheduler.persistence.controller.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Persistence;

/**
 *
 * @author S519479
 */
public class LegacyReportHelper {
    
    public void saveReportToLegacy(int year, String semester, String reportName){
        LegacyReportJpaController legacyController = new LegacyReportJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        SectionJpaController sectionController = new SectionJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        LabJpaController labController = new LabJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        LegacyReport legacy = null;
        List<LegacyReport> legacyReportList = new ArrayList();
        for(Section section : sectionController.findSectionEntities()){
            for(TimeSlots timeSlot : section.getTimeSlotsCollection()){
                legacy = new LegacyReport();
                legacy.setLegacyReportPK((new LegacyReportPK(year, semester, reportName, timeSlot.getTimeSlotId(), section.getSectionFullId())));
                legacy.setDepartmentId(section.getProfessorCourse().getCourse().getDepartment().getDepartmentId());
                legacy.setDepartmentName(section.getProfessorCourse().getCourse().getDepartment().getDepartmentName());
                legacy.setCourseId(section.getProfessorCourse().getCourse().getCoursePK().getCourseId());
                legacy.setCourseName(section.getProfessorCourse().getCourse().getCourseName());
                legacy.setProfessorId(section.getProfessorCourse().getProfessor().getProfessorId());
                legacy.setProfessorFirstName(section.getProfessorCourse().getProfessor().getFirstName());
                legacy.setProfessorLastName(section.getProfessorCourse().getProfessor().getLastName());
                legacy.setProfessorInitials(section.getProfessorCourse().getProfessor().getProfessorInitials());
                legacy.setBuildingId(section.getRoom().getBuilding().getBuildingId());
                legacy.setBuildingName(section.getRoom().getBuilding().getBuildingName());
                legacy.setRoomCapacity(String.valueOf(section.getRoom().getCapacity()));
                legacy.setRoomNumber(String.valueOf(section.getRoom().getRoomPK().getRoomNumber()));
                legacy.setTimeSlots(timeSlot);
                legacyReportList.add(legacy);
            }
        }
        for(Lab lab : labController.findLabEntities()){
            for(TimeSlots timeSlot : lab.getTimeSlotsCollection()){
                legacy = new LegacyReport();
                legacy.setLegacyReportPK((new LegacyReportPK(year, semester, reportName, timeSlot.getTimeSlotId(), lab.getLabFullId())));
                legacy.setDepartmentId(lab.getSectionFullId().getProfessorCourse().getCourse().getDepartment().getDepartmentId());
                legacy.setDepartmentName(lab.getSectionFullId().getProfessorCourse().getCourse().getDepartment().getDepartmentName());
                legacy.setCourseId(lab.getSectionFullId().getProfessorCourse().getCourse().getCoursePK().getCourseId());
                legacy.setCourseName(lab.getSectionFullId().getProfessorCourse().getCourse().getCourseName());
                legacy.setProfessorId(lab.getSectionFullId().getProfessorCourse().getProfessor().getProfessorId());
                legacy.setProfessorFirstName(lab.getSectionFullId().getProfessorCourse().getProfessor().getFirstName());
                legacy.setProfessorLastName(lab.getSectionFullId().getProfessorCourse().getProfessor().getLastName());
                legacy.setProfessorInitials(lab.getSectionFullId().getProfessorCourse().getProfessor().getProfessorInitials());
                legacy.setBuildingId(lab.getRoom().getBuilding().getBuildingId());
                legacy.setBuildingName(lab.getRoom().getBuilding().getBuildingName());
                legacy.setRoomCapacity(String.valueOf(lab.getRoom().getCapacity()));
                legacy.setRoomNumber(String.valueOf(lab.getRoom().getRoomPK().getRoomNumber()));
                legacy.setTimeSlots(timeSlot);
                legacyReportList.add(legacy);
            }
        }
        for(LegacyReport legacyReport : legacyReportList){
            try {
                legacyController.create(legacyReport);
            } catch (Exception ex) {
                Logger.getLogger(LegacyReportHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void deleteLegacyReport(int year, String semester, String reportName){
        LegacyReportJpaController legacyController = new LegacyReportJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
        for(LegacyReport legacy : legacyController.findLegacyReportYearSemesterReportName(year, semester, reportName)){
            try {
                legacyController.destroy(legacy.getLegacyReportPK());
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(LegacyReportHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
