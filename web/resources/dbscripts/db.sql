-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: newscheduler
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `availability`
--

DROP TABLE IF EXISTS `availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `availability` (
  `time_slot_id` int(11) NOT NULL,
  `building_id` varchar(45) NOT NULL,
  `room_number` int(11) NOT NULL,
  `seats_availability` int(11) DEFAULT NULL,
  PRIMARY KEY (`time_slot_id`,`building_id`,`room_number`),
  KEY `availability_room_idx` (`building_id`,`room_number`),
  CONSTRAINT `availability_room` FOREIGN KEY (`building_id`, `room_number`) REFERENCES `room` (`building_id`, `room_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `availability_time_slots` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slots` (`time_slot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `building`
--

DROP TABLE IF EXISTS `building`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `building` (
  `building_id` varchar(45) NOT NULL,
  `building_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`building_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `department_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_name` varchar(45) DEFAULT NULL,
  `lab_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`course_id`,`department_id`),
  KEY `course_department_idx` (`department_id`),
  CONSTRAINT `course_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(45) DEFAULT NULL,
  `department_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lab`
--

DROP TABLE IF EXISTS `lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lab` (
  `lab_full_id` varchar(45) NOT NULL,
  `section_full_id` varchar(45) DEFAULT NULL,
  `building_id` varchar(45) DEFAULT NULL,
  `room_number` int(11) DEFAULT NULL,
  `occupied` int(11) DEFAULT NULL,
  PRIMARY KEY (`lab_full_id`),
  KEY `lab_section_idx` (`section_full_id`),
  KEY `lab_room_idx` (`building_id`,`room_number`),
  CONSTRAINT `lab_room` FOREIGN KEY (`building_id`, `room_number`) REFERENCES `room` (`building_id`, `room_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lab_section` FOREIGN KEY (`section_full_id`) REFERENCES `section` (`section_full_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lab_slot`
--

DROP TABLE IF EXISTS `lab_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lab_slot` (
  `lab_full_id` varchar(45) NOT NULL,
  `time_slot_id` int(11) NOT NULL,
  PRIMARY KEY (`lab_full_id`,`time_slot_id`),
  KEY `lab_slot_time_slots_idx` (`time_slot_id`),
  CONSTRAINT `lab_slot_lab` FOREIGN KEY (`lab_full_id`) REFERENCES `lab` (`lab_full_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lab_slot_time_slots` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slots` (`time_slot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `legacy_report`
--

DROP TABLE IF EXISTS `legacy_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_report` (
  `year` int(11) NOT NULL,
  `semester` varchar(45) NOT NULL,
  `report_name` varchar(45) NOT NULL,
  `time_slot_id` int(11) NOT NULL,
  `section_or_lab_full_id` varchar(45) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `department_name` varchar(45) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `course_name` varchar(45) DEFAULT NULL,
  `professor_id` varchar(45) DEFAULT NULL,
  `professor_first_name` varchar(45) DEFAULT NULL,
  `professor_last_name` varchar(45) DEFAULT NULL,
  `professor_initials` varchar(45) DEFAULT NULL,
  `building_id` varchar(45) DEFAULT NULL,
  `building_name` varchar(45) DEFAULT NULL,
  `room_number` varchar(45) DEFAULT NULL,
  `room_capacity` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`year`,`semester`,`report_name`,`time_slot_id`,`section_or_lab_full_id`),
  KEY `legacy_report_time_slots_idx` (`time_slot_id`),
  CONSTRAINT `legacy_report_time_slots` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slots` (`time_slot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `professor`
--

DROP TABLE IF EXISTS `professor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor` (
  `professor_id` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `professor_initials` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`professor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `professor_course`
--

DROP TABLE IF EXISTS `professor_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor_course` (
  `departrment_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `professor_id` varchar(45) NOT NULL,
  PRIMARY KEY (`departrment_id`,`course_id`,`professor_id`),
  KEY `professor_course_professor_idx` (`professor_id`),
  CONSTRAINT `professor_course_course` FOREIGN KEY (`departrment_id`, `course_id`) REFERENCES `course` (`department_id`, `course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `professor_course_professor` FOREIGN KEY (`professor_id`) REFERENCES `professor` (`professor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `building_id` varchar(45) NOT NULL,
  `room_number` int(11) NOT NULL,
  `lab_type` varchar(45) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  PRIMARY KEY (`building_id`,`room_number`),
  CONSTRAINT `room_building` FOREIGN KEY (`building_id`) REFERENCES `building` (`building_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `section_full_id` varchar(45) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `professor_id` varchar(45) DEFAULT NULL,
  `building_id` varchar(45) DEFAULT NULL,
  `room_number` int(11) DEFAULT NULL,
  `occupied` int(11) DEFAULT NULL,
  PRIMARY KEY (`section_full_id`),
  KEY `section_professor_course_idx` (`department_id`,`course_id`,`professor_id`),
  KEY `section_room_idx` (`building_id`,`room_number`),
  CONSTRAINT `section_professor_course` FOREIGN KEY (`department_id`, `course_id`, `professor_id`) REFERENCES `professor_course` (`departrment_id`, `course_id`, `professor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `section_room` FOREIGN KEY (`building_id`, `room_number`) REFERENCES `room` (`building_id`, `room_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `section_slot`
--

DROP TABLE IF EXISTS `section_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section_slot` (
  `section_full_id` varchar(45) NOT NULL,
  `time_slot_id` int(11) NOT NULL,
  PRIMARY KEY (`section_full_id`,`time_slot_id`),
  KEY `section_slot_time_slots_idx` (`time_slot_id`),
  CONSTRAINT `section_slot_section` FOREIGN KEY (`section_full_id`) REFERENCES `section` (`section_full_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `section_slot_time_slots` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slots` (`time_slot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_slots`
--

DROP TABLE IF EXISTS `time_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_slots` (
  `time_slot_id` int(11) NOT NULL,
  `day` varchar(45) DEFAULT NULL,
  `start_time` varchar(45) DEFAULT NULL,
  `end_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`time_slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-01 17:00:05
