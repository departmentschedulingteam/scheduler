/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

map = new Object();
secondCheck = "";
$(document).ready(function () {

    $('#datesTable').on('click', '.removeitem', deleteItem);
    $('#detailsDiv').hide();
    $(getDepartmentList);
    $(getBuildingList);
    $(loadTime(0));
    $('#startTime').on('change',loadEndTime);
    $("#departmentID").change(function () {
        getCourseList($(this).val());
    });
    $("#course").change(function () {
        professorLoad($(this).val());
    });
    $("#building").change(function () {
        getRooms($(this).val());
    });
    $("#buildingLab").change(function () {
        getRoomsLab($(this).val());
    });
    $("#submit").click(function () {
        insertSchedule();
    });
    $("#allow").click(function () {
        secondCheck = "allow";

        insertSchedule();
    });
    $("#deny").click(function () {
        location.reload();

    });
});



function insertSchedule() {
    var departmentID = $("#departmentID").val();
    var course = $("#course").val();
    var sectionID = $("#sectionNumber").val();
    var professorID = $("#professor").val();
    var buildingID = $("#building").val();
    var room = $("#room").val();
    var seats = $("#seats").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();

    var roomsText = $("#room option:selected").text();
    var arr = roomsText.split("/");
    var x = parseInt(arr[2]);
    var re = /^([0-9]*)$/i;
    if (!re.test(sectionID)) {
        alert("Enter only numeric values for section id");
        return;
    }
    if (!re.test(seats)) {
        alert("Enter only numeric values for seats");
        return;
    }
    if (seats<=0) {
        alert("Enter only positive values for seats");
        return;
    }
    if (seats > x) {
        alert("Number of seats should be less than room capacity");
        return;
    }

    var sectionDayChecked = $("input[name='sectionDay']:checked").map(function () {
        return $(this).val();
    }).get();
    var sectionDayCheckedString = "";
    for (i = 0; i < sectionDayChecked.length; i++) {
        sectionDayCheckedString += "&sectionDay=" + sectionDayChecked[i];
    }
    var labChecked = "N";
    if ($("#labCheck").is(':checked')) {
        labChecked = "Y";
        var buildingLab = $("#buildingLab").val();
        var roomLab = $("#roomLab").val();
        var seatLab = $("#seatLab").val();
        var startTimeLab = $("#startTimeLab").val();
        var endTimeLab = $("#endTimeLab").val();
        var labDayChecked = $("input[name='labDay']:checked").map(function () {

            return $(this).val();
        }).get();
        var labDayCheckedString = "";
        for (i = 0; i < labDayChecked.length; i++) {
            labDayCheckedString += "&labDay=" + labDayChecked[i];
        }
    }
    if (labChecked === "N") {
        $.getJSON("SchedulerServlet?reqType=insertSection&departmentID=" + departmentID + "&course=" + course + "&sectionID=" + sectionID +
                "&professorID=" + professorID + "&buildingID=" + buildingID + "&room=" + room + "&seats=" + seats + "&startTime=" + startTime +
                "&endTime=" + endTime + "" + sectionDayCheckedString + "&allow=" + secondCheck + "&labChecked=" + labChecked, displaySection);
    } else {
        $.getJSON("SchedulerServlet?reqType=insertSection&departmentID=" + departmentID + "&course=" + course + "&sectionID=" + sectionID +
                "&professorID=" + professorID + "&buildingID=" + buildingID + "&room=" + room + "&seats=" + seats + "&startTime=" + startTime +
                "&endTime=" + endTime + "" + sectionDayCheckedString + "&allow=" + secondCheck + "&labChecked=" + labChecked + "&buildingLab=" +
                buildingLab + "&roomLab=" + roomLab + "&seatLab=" + seatLab + "&startTimeLab=" + startTimeLab + "&endTimeLab=" + endTimeLab +
                labDayCheckedString, displaySection);
    }
}

function displaySection(sectionList) {
    $('#messageScheduler').html(sectionList.message);
    $('#displaySection').find("tr:gt(0)").remove();
    // $('#detailsDiv').show();
    var message = sectionList.message;
    var allowOrDeny = sectionList.allowOrDeny;
    if (message === "Section Created Successfully") {
        $('#detailsDiv').hide();
    }

    if (allowOrDeny === "Deny") {
        $('#detailsDiv').show();
        $('#allow').hide();
        $('#deny').hide();
    }
    if (allowOrDeny === "Allow") {
        $('#detailsDiv').show();
        $('#allow').show();
        $('#deny').show();
    }


    if (secondCheck === "allow") {
        $('#detailsDiv').hide();
    }
    else {
        $.each(sectionList.scheduler,
                function () {
                    $('#displaySection').append(
                            "<tr>"
                            + "<td>" + this.sectionLabFullID + "</td>"
                            + "<td>" + this.professor + "</td>"
                            + "<td>" + this.buildingRoom + "</td>"
                            + "<td>" + this.timeSlots + "</td>"
                            + "<td>" + this.conflictType + "</td>"
                            + "</tr>");
                });
    }
}

function professorLoad() {
    var courseID = $('#course').val();
    var deptId = $('#departmentID').val();
    $.getJSON("ProfessorCourseServlet?reqType=getProfessorList&courseID=" + courseID + "&deptId=" + deptId, displayProfessors);
}

function displayProfessors(professorList) {
    $("#professor").empty().append($("<option></option>").val("select").html("Please select Professor"));
    $.each(professorList.professors,
            function () {
                $('#professor').append($("<option></option>").val(this['professorID']).html(this['professorID']+"-"+this['professorName']));
            });
}

function getRoomsLab() {
    var buildingID = $('#buildingLab').val();
    $.getJSON("RoomServlet?reqType=getRoomList&buildingID=" + buildingID, displayRoomsLab);
}

function displayRoomsLab(roomList) {
    $("#roomLab").empty().append($("<option></option>").val("select").html("Please select Room"));
    $.each(roomList.rooms,
            function () {
                $('#roomLab').append($("<option></option>").val(this['roomNumber']).html(this['roomNumber'] + "/" +
                        this['labType'] + "/" + this['capacity']));
            });
}

function getRooms() {
    var buildingID = $('#building').val();
    $.getJSON("RoomServlet?reqType=getRoomList&buildingID=" + buildingID, displayRooms);
}

function displayRooms(roomList) {
    $("#room").empty().append($("<option></option>").val("select").html("Please select Room"));
    $.each(roomList.rooms,
            function () {
                $('#room').append($("<option></option>").val(this['roomNumber']).html(this['roomNumber'] + "/" +
                        this['labType'] + "/" + this['capacity']));
            });
}


function checkLab(cou) {
    if (map[cou] == "lab") {
        $("#labCheck").prop("disabled", false);
    } else {
        $("#labCheck").prop("disabled", true);
    }
}

function getBuildingList() {
    $.getJSON("BuildingServlet?reqType=getBuildingList", displayBuilding);
}

function displayBuilding(buildingList) {
    $("#building").empty().append($("<option></option>").val("select").html("Please select Building"));
    $("#buildingLab").empty().append($("<option></option>").val("select").html("Please select Building"));
    $.each(buildingList.buildings,
            function () {
                $('#building').append($("<option></option>").val(this['buildingID']).html(this['buildingName']));
                $('#buildingLab').append($("<option></option>").val(this['buildingID']).html(this['buildingName']));
            });
}

function getCourseList(dept) {
    $.getJSON("CourseServlet?reqType=getCourseList&departmentID=" + dept, displayCourse);
}

function displayCourse(courseList) {
    $("#course").empty().append($("<option></option>").val("select").html("Please select Course"));
    map = new Object();
    $.each(courseList.courses,
            function () {
                $('#course').append($("<option></option>").val(this['courseID']).html(this['courseID']+"-"+this['courseName']));
                map[this['courseID']] = (this['labType'] == "windows" || this['labType'] == "mac" ? "lab" : "NA");
            });
}

function getDepartmentList() {
    $.getJSON("DepartmentServlet?reqType=getDepartmentList", displayDepartment);
}

function displayDepartment(departmentList) {
    $("#departmentID").empty().append($("<option></option>").val("select").html("Please select Discipline"));
    $.each(departmentList.departments,
            function () {
                $('#departmentID').append($("<option></option>").val(this['departmentID']).html(this['departmentName']));
            });
}

function checkCourseSection() {
    var sectionNumber = document.getElementById("sectionNumber");
    var course = document.getElementById("course");
    var department = document.getElementById("department");
    var room = document.getElementById("room");
    var building = document.getElementById("building");
    var professor = document.getElementById("professor");
    var seats = document.getElementById("seats");
    var startTime = document.getElementById("startTime");
    var endTime = document.getElementById("endTime");
    if (sectionNumber.value.length <= 0)
    {
        alert("Enter section number");
    }
    if (course.value === "select")
    {
        alert("Select the course");
    }
    if (department.value === "select")
    {
        alert("Select the department");
    }
    if (room.value === "select")
    {
        alert("select the room");
    }
    if (building.value === "select")
    {
        alert("Select the building");
    }
    if (professor.value === "select")
    {
        alert("Select the professor");
    }
    if (seats.value.length <= 0)
    {
        alert("Enter the seats");
    }
    if (startTime.value === "select")
    {
        alert("Select the start time");
    }
    if (endTime.value === "select")
    {
        alert("Select the end time");
    }
}

function attach() {
    var rowCount = $('#datesTable tr').length;

    $(loadTime(rowCount));
    if ($('input:radio[name=classInsert]:checked').val() === "section") {
        $('#datesTable').append(str);
    } else {
        $('#datesTableLab').append(str);
    }
}

function loadTime(count) {

    var timeSlots = ["08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00",
        "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00",
        "19:30", "20:00", "20:30"];
    $.each(timeSlots, function (val, text) {
        $("#startTime").append($('<option></option>').val(text).html(text));

    });
}


function loadEndTime() {
    var startTimeVal = $('#startTime').val();
    
    var timeSlots = ["08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00",
        "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00",
        "19:30", "20:00", "20:30"];
    
    var newTimeSlots = timeSlots.slice(jQuery.inArray(startTimeVal, timeSlots)+1, timeSlots.length);
    $("#endTime").children().remove();
    $.each(newTimeSlots, function (val, text) {


        $("#endTime").append($('<option></option>').val(text).html(text));

    });

}
function deleteItem() {
    $(this).parent().parent().remove();
}

