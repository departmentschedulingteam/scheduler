$(document).ready(function () {

    $(getDepartmentList);
    var x = $.cookie("values");
    $('#cancel').click(redirectToCourse);
    x1 = x.split(",");
    $('#courseID').val(x1[0]);

    $('#courseID').prop('disabled', 'disabled');
    var lab = x1[1];
    $(getDepartmentList);
    if (lab === "pc") {
        $('#pc').attr('checked', true);
    } else if (lab === "mac") {
        $('#mac').attr('checked', true);
    } else {
        $('#NA').attr('checked', true);
    }
    $('#courseName').val(x1[2]);
    deptIdFromCookie = x1[3];
    $('#save').click(
            function () {

                var courseId = jQuery.trim($("#courseID").val());
                var courseName = jQuery.trim($("#courseName").val());
                var isOk = false;
                if ($("input[name='labType']:checked").length <= 0) {
                    $('#labTypeMessage').show();
                    $('#labTypeMessage').html("*Select type of lab");
                    return;
                }
                else {
                    $('#labTypeMessage').hide();
                    isOk = true;
                }
                var re = /^([a-zA-Z0-9 ]*)$/i;
                if (courseName.length === 0 || !re.test(courseName)) {
                    $('#courseNameMessage').show();
                    $('#courseNameMessage').html("Enter name, only alphanumeric");
                    return;
                }
                else {
                    $('#courseNameMessage').hide();
                    isOk = true;
                }
                if (isOk)
                    submitChanges();
            });
});
function getDepartmentList() {

    $.getJSON("DepartmentServlet?reqType=getDepartmentList", displayDepartment);
}

function displayDepartment(departmentList) {

    $("#departmentID").empty().append($("<option></option>").val("select").html("Please select Discipline"));
    $.each(departmentList.departments,
            function () {
                //alert(parseInt(this['departmentID']) === parseInt(deptIdFromCookie));
                if (parseInt(this['departmentID']) === parseInt(deptIdFromCookie)) {
                    $('#departmentID').append($("<option></option>").val(this['departmentID']).attr("selected", "selected").html(this['departmentName']));
                    //$('#departmentID option[value=""]')
                } else {
                    $('#departmentID').append($("<option></option>").val(this['departmentID']).html(this['departmentName']));
                }
            });
    $('#departmentID').prop('disabled', 'disabled');
}
function redirectToCourse() {
    window.location.href = "Course.jsp";
}
function submitChanges() {

    var courseID = $('#courseID').val();
    var departmentID = $('#departmentID').val();
    var labType = $('input:radio[name=labType]:checked').val();
    var courseName = $('#courseName').val();
    $.getJSON("CourseServlet?courseID=" + courseID + "&departmentID=" + departmentID + "&labType=" + labType + "&courseName=" + courseName + "&reqType=editing", displayMessage);
}

function displayMessage(courseMessage) {

    if (courseMessage.message === "Y") {
        alert("Course Edited Successfully, you will be re-directed to add course page");
        window.location.href = "Course.jsp";
    } else {
        $('#message').html("problem with Database");
    }



}

