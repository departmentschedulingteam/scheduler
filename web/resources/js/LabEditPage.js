/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


map = new Object();
secondCheck = "";
$(document).ready(function () {
    // $('.Add').click(attach);
    $('#datesTable').on('click', '.removeitem', deleteItem);
    $('#detailsDiv').hide();
    //$(getDepartmentList);
    //$(getBuildingList);
    $(loadTime(0));
    $('#startTime').on('change', loadEndTime);
    addValues();

    $("#building").change(function () {
        getRooms($(this).val());
    });
    $("#submit").click(function () {
        insertSchedule();
    });
    $("#allow").click(function () {
        secondCheck = "allow";
        insertSchedule();
    });
    $("#deny").click(function () {
        location.reload();
    });
});

function addValues() {
    var x = $.cookie("labDetails");

    values = x.split("_");

//    sectionFullID = this.sectionFullID;
//    deptID = this.departmentID;
//    deptName = this.departmentName;
//    cour = this.courseID;
//    courName = this.courseName;
//    sec = this.sectionID;
//    profID = this.professorID;
//    profNa = this.professorName;
//    buil = this.buildingID;
//    roo = this.roomID;
//    seats = this.seats;
//    timeS = this.timeSlots;

    $("#departmentID").val(values[1] + "-" + values[2]);
    $("#departmentID").prop('disabled', 'disabled');
    $("#course").val(values[3] + "-" + values[4]);
    $("#course").prop('disabled', 'disabled');
    $("#sectionNumber").val(values[5]);
    $("#sectionNumber").prop('disabled', 'disabled');
    $("#professor").val(values[6] + "-" + values[7]);
    $("#professor").prop('disabled', 'disabled');

    getBuildingList();
    getRooms(values[8]);
    $("#seats").val(values[10]);
    string = values[11];
    $(loadTimes);

}


function insertSchedule() {
    var buildingID = $("#building").val();
    var room = $("#room").val();
    var seats = $("#seats").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();

    var roomsText = $("#room option:selected").text();
    var arr = roomsText.split("/");
    var x = parseInt(arr[2]);
    var re = /^([0-9]*)$/i;
    if (!re.test(seats)) {
        alert("Enter only numeric values for seats");
        return;
    }
    if (seats > x) {
        alert("Number of seats shuold be less than room capacity");
        return;
    }

    var sectionDayChecked = $("input[name='sectionDay']:checked").map(function () {
        return $(this).val();
    }).get();
    var sectionDayCheckedString = "";
    for (i = 0; i < sectionDayChecked.length; i++) {
        sectionDayCheckedString += "&sectionDay=" + sectionDayChecked[i];
    }
    var x = $.cookie("labDetails");
    values = x.split("_");
    labFullID = values[0];
    $.getJSON("LabModifyServlet?reqType=editLab&labFullID=" + labFullID + "&buildingID=" + buildingID + "&room=" +
            room + "&seats=" + seats + "&startTime=" + startTime + "&endTime=" + endTime + "" +
            sectionDayCheckedString + "&allow=" + secondCheck, displaySection);
}

function displaySection(sectionList) {
    $('#messageScheduler').html(sectionList.message + ", you will be redirected to section edit page");
    if(sectionList.message==="Lab Edited Successfully"){
        window.setTimeout(function(){
            location.href="SectionEdit.jsp";
        },2000);
    }
    $('#displaySection').find("tr:gt(0)").remove();
    var message = sectionList.message;
    var allowOrDeny = sectionList.allowOrDeny;
    if (message === "Data Entered") {
        $('#detailsDiv').hide();
    }

    if (allowOrDeny === "Deny") {
        $('#detailsDiv').show();
        $('#allow').hide();
        $('#deny').hide();
    }
    if (allowOrDeny === "Allow") {
        $('#detailsDiv').show();
        $('#allow').show();
        $('#deny').show();
    }


    if (secondCheck === "allow") {
        $('#detailsDiv').hide();
    }
    else {
        $.each(sectionList.scheduler,
                function () {
                    $('#displaySection').append(
                            "<tr>"
                            + "<td>" + this.sectionLabFullID + "</td>"
                            + "<td>" + this.professor + "</td>"
                            + "<td>" + this.buildingRoom + "</td>"
                            + "<td>" + this.timeSlots + "</td>"
                            + "<td>" + this.conflictType + "</td>"
                            + "</tr>");
                });
    }
}

function getRooms(buildingID) {
    //var buildingID = $('#building').val();
    $.getJSON("RoomServlet?reqType=getRoomList&buildingID=" + buildingID, displayRooms);
}

function displayRooms(roomList) {
    $("#room").empty().append($("<option></option>").val("select").html("Please select Room"));
    var x = $.cookie("labDetails");
    values = x.split("_");
    room = values[9];
    $.each(roomList.rooms,
            function () {
                if (parseInt(this['roomNumber']) === parseInt(room)) {
                    $('#room').append($("<option></option>").val(this['roomNumber']).attr("selected", "selected").html(this['roomNumber'] + "/" +
                            this['labType'] + "/" + this['capacity']));
                } else {
                    $('#room').append($("<option></option>").val(this['roomNumber']).html(this['roomNumber'] + "/" +
                            this['labType'] + "/" + this['capacity']));
                }
            });
}

function getBuildingList() {
    $.getJSON("BuildingServlet?reqType=getBuildingList", displayBuilding);
}

function displayBuilding(buildingList) {
    $("#building").empty().append($("<option></option>").val("select").html("Please select Building"));
    var x = $.cookie("labDetails");
    values = x.split("_");
    building = values[8];
    $.each(buildingList.buildings,
            function () {
                if (this['buildingID'] === building) {
                    $('#building').append($("<option></option>").val(this['buildingID']).attr("selected", "selected").html(this['buildingName']));
                } else {
                    $('#building').append($("<option></option>").val(this['buildingID']).html(this['buildingName']));
                }
            });
}


function loadTimes() {

    var s = string.split(" ");

    $.each(s, function (i, x) {
        if (x === "M") {
            $('#M').prop("checked", "true");
        }
        if (x === "T") {
            $('#T').prop("checked", "true");
        }
        if (x === "W") {
            $('#W').prop("checked", "true");
        }
        if (x === "Th") {
            $('#Th').prop("checked", "true");
        }
        if (x === "F") {
            $('#F').prop("checked", "true");
        }
        if (x === "Sa") {
            $('#Sa').prop("checked", "true");
        }

    });

    startTime = s[s.length - 2];
    endTime = s[s.length - 1];

    var timeSlots = ["08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00",
        "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00",
        "19:30", "20:00", "20:30"];
    parseStart = "", parseend = "";
    for (var i = 0; i < startTime.length; i++) {

        if (i === 2) {
            parseStart += ":" + startTime[i];
            parseend += ":" + endTime[i];
        } else {
            parseStart += startTime[i];
            parseend += endTime[i];

        }
    }
    $("#startTime").children().remove();
    $("#endTime").children().remove();
    $.each(timeSlots, function (val, text) {
        if (text === parseStart)
            $("#startTime").append($('<option></option>').val(text).html(text).prop("selected", "true"));
        else
            $("#startTime").append($('<option></option>').val(text).html(text));

    });

    $.each(timeSlots, function (val, text) {
        if (text === parseend)
            $("#endTime").append($('<option></option>').val(text).html(text).prop("selected", "true"));
        else
            $("#endTime").append($('<option></option>').val(text).html(text));

    });

}



function checkCourseSection() {
    var room = document.getElementById("room");
    var building = document.getElementById("building");
    var seats = document.getElementById("seats");
    var startTime = document.getElementById("startTime");
    var endTime = document.getElementById("endTime");
    if (room.value === "select")
    {
        alert("select the room");
    }
    if (building.value === "select")
    {
        alert("Select the building");
    }
    if (seats.value.length <= 0)
    {
        alert("Enter the seats");
    }
    if (startTime.value === "select")
    {
        alert("Select the start time");
    }
    if (endTime.value === "select")
    {
        alert("Select the end time");
    }
}

function attach() {
    var rowCount = $('#datesTable tr').length;

    $(loadTime(rowCount));
    if ($('input:radio[name=classInsert]:checked').val() === "section") {
        $('#datesTable').append(str);
    } else {
        $('#datesTableLab').append(str);
    }
}

function loadTime(count) {

    var timeSlots = ["08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00",
        "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00",
        "19:30", "20:00", "20:30"];
    $.each(timeSlots, function (val, text) {
        $("#startTime").append($('<option></option>').val(text).html(text));

    });
}


function loadEndTime() {
    var startTimeVal = $('#startTime').val();

    var timeSlots = ["08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00",
        "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00",
        "19:30", "20:00", "20:30"];

    var newTimeSlots = timeSlots.slice(jQuery.inArray(startTimeVal, timeSlots) + 1, timeSlots.length);
    $("#endTime").children().remove();
    $.each(newTimeSlots, function (val, text) {


        $("#endTime").append($('<option></option>').val(text).html(text));

    });

}
function deleteItem() {
    $(this).parent().parent().remove();
}