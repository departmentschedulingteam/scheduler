/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $(addCourses);
    $('#cancel').click(redirectToCourse);
    $('#viewProfessors').click(getProfessorList);
    $(addValues);
    $('#submit1').click(function () {
        var professorID = jQuery.trim($("#professorID").val());
        var firstName = jQuery.trim($("#firstName").val());
        var lastName = jQuery.trim($("#lastName").val());
        var professorCode = jQuery.trim($("#professorCode").val());
        var courses = jQuery.trim($("#addCourses").val());

        var isOk = false;
        var re = /^([a-zA-Z0-9 ]*)$/i;
        
        if (firstName.length === 0 || !re.test(firstName)) {
            $('#firstNameMessage').show();
            $('#firstNameMessage').html("*Enter name, only alphanumeric");
            return;
        } else {
            $('#firstNameMessage').hide();
            isOk = true;
        }
        if (lastName.length === 0 || !re.test(lastName)) {
            $('#lastNameMessage').show();
            $('#lastNameMessage').html("Enter last value,only alphanumeric");
            return;
        } else {
            $('#lastNameMessage').hide();
            isOk = true;
        }
        if (professorCode.length === 0 || !re.test(professorCode)) {
            $('#professorCodeMessage').show();
            $('#professorCodeMessage').html("Enter code, only alphanumeric");
            return;
        } else {
            $('#professorCodeMessage').hide();
            isOk = true;
        }

        if ($('input[type=checkbox]:checked').length === 0) {
            $('#coursesMessage').show();
            $('#coursesMessage').html("*This field is required");
            return;
        } else {
            $('#coursesMessage').hide();
            isOk = true;
        }

        if (isOk) {
            editNewProfessor();
        }
    });

}
);

function addCourses() {
    $.getJSON("CourseServlet?reqType=getFullCoursesList", addCoursesToList);
}
function redirectToCourse() {
    window.location.href = "Course.jsp";
}

function addValues() {
    var x = $.cookie("profvalues");

    values = x.split("_");

    $("#professorID").val(values[0]);
    $("#professorID").prop('disabled', 'disabled');
    $("#firstName").val(values[1]);
    $("#lastName").val(values[2]);
    $("#professorCode").val(values[3]);
    courseValues = values[4];
    $.cookie("values", null);


}

function addCoursesToList(coursesList) {
    $('#message').html(coursesList.message);
    $('#addCourses').empty();
    var list = courseValues.split(",");

    $.each(coursesList.courses,
            function () {
                var x = this.departmentID + "-" + this.courseID;
                for (var i = 0; i < list.length; i++) {
                    var ind = list[i].lastIndexOf("-");
                    if (x === list[i].substring(0, ind) && list[i].substring(ind + 1, list[i].length) === "R") {
                        $('#addCourses').append("<input type=\"checkbox\" name=\"courseList\" value=" +
                                this.departmentID + "-" + this.courseID + " checked disabled>" + this.departmentID + "-" + this.courseID + ":-" + this.courseName + "</input><br>");
                        return;
                    } else if (x === list[i].substring(0, ind) && list[i].substring(ind + 1, list[i].length) === "N") {
                        $('#addCourses').append("<input type=\"checkbox\" name=\"courseList\" value=" +
                                this.departmentID + "-" + this.courseID + " checked>" + this.departmentID + "-" + this.courseID + ":-" + this.courseName + "</input><br>");
                        return;
                    } else if (i === (list.length - 1)) {
                        $('#addCourses').append("<input type=\"checkbox\" name=\"courseList\" value=" +
                                this.departmentID + "-" + this.courseID + ">" + this.departmentID + "-" + this.courseID + ":-" + this.courseName + "</input><br>");
                    }
                }


            });
}

function getProfessorList() {
    $.getJSON("ProfessorServlet?reqType=getProfessorList", displayTable);
}

function editNewProfessor() {
    var professorID = $('#professorID').val();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var professorCode = $('#professorCode').val();
    var checkedValues = $("input[name='courseList']:checked").map(function () {
        //  alert($(this).val());
        return $(this).val();
    }).get();
    var checkedValuesString = "";
    for (i = 0; i < checkedValues.length; i++) {

        checkedValuesString += "&courses=" + checkedValues[i];
    }
    var surveyCode = "";

    $.getJSON("ProfessorServlet?professorID=" + professorID + "&firstName=" + firstName +
            "&lastName=" + lastName + "&professorCode=" + professorCode +
            checkedValuesString + "&surveyCode=" + surveyCode + "&reqType=editing", displayMessage);
}

function displayMessage(professorMessage) {

    if (professorMessage.message === "Y") {
        alert("Professor Edited Successfully, you will be re-directed to add Professor page");
        window.location.href = "Professor.jsp";
    } else {
        $('#message').html("problem with Database");
    }

}

function displayTable(professorList) {
    $('#message').html(professorList.message);
    $('#displayProfessors').empty();
    $('#displayProfessors').append("<tr>"
            + "<th>Professor ID</th>"
            + "<th>First Name</th>"
            + "<th>Last Name</th>"
            + "<th>Professor Code</th>"
            + "<th>Courses</th>"

            + "</tr>");
    $.each(professorList.professors,
            function () {

                $('#displayProfessors').append("<tr>"
                        + "<td>" + this.professorID + "</td>"
                        + "<td>" + this.firstName + "</td>"
                        + "<td>" + this.lastName + "</td>"
                        + "<td>" + this.professorCode + "</td>"
                        + "<td>" + this.courses + "</td>"

                        + "<td> <input type=\"button\" value=\"Edit\" onclick=\"makeEditable(this)\" class=\"button\"></td>"
                        + "<td> <input type=\"button\" value=\"Delete\" onclick=\"deleteBuild(this)\" class=\"button\"></td>"

                        + "</tr>");
            });
}
