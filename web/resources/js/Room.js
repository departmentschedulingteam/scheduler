/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $(getBuildingList);
    $('#tableValues').hide();
    $('input:radio[name=divType]').on('change', function () {
        var divType = $('input:radio[name=divType]:checked').val();
        if (divType === "dataEntry") {
            $('#dataEntry').show();
            $('#tableValues').hide();
            $('#messageInsert').html("");
            $('#roomTable').hide();
        } else {
            $('#dataEntry').hide();
            $('#tableValues').show();
            $('#messageEdit').html("");
            $(getBuildingList);
        }

    });


    $('#submit1').click(function () {
        var roomNumber = jQuery.trim($("#roomNumber").val());
        var roomCapacity = jQuery.trim($("#roomCapacity").val());
        var isOk = false;
        if ($('#displayBuildings').val() === "select")
        {
            $('#buildingMessage').show();
            $('#buildingMessage').html("*Select building");
            return;
        } else {
            $('#buildingMessage').hide();
            isOk = true;
        }
        var re = /^([0-9]*)$/i;
        if (roomNumber.length === 0 || !re.test(roomNumber)) {
            $('#roomMessage').show();
            $('#roomMessage').html("Enter only digits");
            return;
        } else {
            $('#roomMessage').hide();
            isOk = true;
        }

        if ($("input[name='labType']:checked").length <= 0) {
            $('#labTypeMessage').show();
            $('#labTypeMessage').html("*Select type of lab");
            return;
        }
        else {
            $('#labTypeMessage').hide();
            isOk = true;
        }

        if (roomCapacity.length === 0 || !re.test(roomNumber)) {
            $('#capacityMessage').show();
            $('#capacityMessage').html("*Enter only numbers");
            return;
        }
        else {
            $('#capacityMessage').hide();
            isOk = true;
        }
        if (isOk)
            insertNewRoom();
    });
});

function getBuildingList() {
    $.getJSON("BuildingServlet?reqType=getBuildingList", displayBuilding);
}

function displayBuilding(buildingList) {
    $("#displayBuildings").empty().append($("<option></option>").val("select").html("Please select Building"));
    $("#buildingList").empty().append($("<option></option>").val("select").html("Please select Building"));
    $.each(buildingList.buildings,
            function () {
                $('#displayBuildings').append($("<option></option>").val(this['buildingID']).html(this['buildingName']));
                $('#buildingList').append($("<option></option>").val(this['buildingID']).html(this['buildingName']));
            });
}

function getRooms() {
    var buildingID = $('#buildingList').val();
    $.getJSON("RoomServlet?reqType=getRoomList&buildingID=" + buildingID, displayRooms);
}

function displayRooms(RoomList) {
    $('#roomTable').show();
    var divType = $('input:radio[name=divType]:checked').val();
    if (divType === "dataEntry") {
        $('#messageInsert').html(RoomList.message);

    } else {
        $('#messageEdit').html(RoomList.message);
    }
    $('#roomTable').empty();
    $('#roomTable').append("<tr>"
            + "<th>Building ID</th>"
            + "<th>Room Number</th>"
            + "<th>Lab Type</th>"
            + "<th>Room Capacity</th>"
            + "</tr>");
    $.each(RoomList.rooms,
            function () {

                $('#roomTable').append("<tr>"
                        + "<td>" + this.buildingId + "</td>"
                        + "<td>" + this.roomNumber + "</td>"
                        + "<td>" + this.labType + "</td>"
                        + "<td>" + this.capacity + "</td>"

                        + "<td> <input type=\"button\" value=\"Edit\" onclick=\"makeEditable(this)\" class=\"btn btn-success\"></td>"
                        + "<td> <input type=\"button\" value=\"Delete\" onclick=\"deleteBuild(this)\" class=\"btn btn-danger\"></td>"

                        + "</tr>");
            });

}

function insertNewRoom() {
    var buildingID = $('#displayBuildings').val();
    var roomNumber = $('#roomNumber').val();
    var labType = $('input:radio[name=labType]:checked').val();
    var roomCapacity = $('#roomCapacity').val();
    $.getJSON("RoomServlet?buildingID=" + buildingID + "&roomNumber=" + roomNumber + "&labType=" + labType + "&roomCapacity=" + roomCapacity + "&reqType=inserting", displayMessage);
}

function displayMessage(roomMessage) {

    var divType = $('input:radio[name=divType]:checked').val();
    if (divType === "dataEntry") {
        $('#messageInsert').html(roomMessage.message);

    } else {
        $('#messageEdit').html(roomMessage.message);
    }
}

function makeEditable($this) {

    var isString = $($this).parent().closest('td').prev('td').find('input').length === 0 ? true : false;
    if (isString) {
        var value = $($this).parent().closest('td').prev('td').text();
        $($this).parent().closest('td').prev('td').html("<input type=\"text\" id=\"capacityEdited\" value=\"" + value + "\"/>");
        var labType = $($this).parent().closest('td').prev('td').prev('td').html();
        s1 = "", s2 = "", s3 = "";

        if (labType === "windows") {
            s1 = "<input type=\"radio\" name=\"labTypeEdited\" value=\"windows\" id=\"pcEdited\" checked=\"checked\">Windows<br>";
        } else {
            s1 = "<input type=\"radio\" name=\"labTypeEdited\" value=\"windows\" id=\"pcEdited\">Windows<br>";
        }
        if (labType === "mac") {
            s2 = "<input type=\"radio\" name=\"labTypeEdited\" value=\"mac\" id=\"macEdited\" checked=\"checked\">MAC<br>";
        } else {
            s2 = "<input type=\"radio\" name=\"labTypeEdited\" value=\"mac\" id=\"macEdited\">MAC<br>";
        }
        if (labType === "NA") {
            s3 = "<input type=\"radio\" name=\"labTypeEdited\" value=\"NA\" id=\"NAEdited\" checked=\"checked\">Not Lab <br>";
        } else {
            s3 = "<input type=\"radio\" name=\"labTypeEdited\" value=\"NA\" id=\"NAEdited\">Not Lab<br> ";
        }

        $($this).parent().closest('td').prev('td').prev('td').html(s1 + s2 + s3);
        $($this).val("Save");
    } else {
        if ($($this).val() !== "Edit") {
            $($this).on('click', editBuilding($('#capacityEdited').val(), $('input:radio[name=labTypeEdited]:checked').val(), $($this).parent().closest('td').prev('td').prev('td').prev('td').html(), $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').html()));
        }
    }
}

function editBuilding(capacity, labtype, roomId, buildingID) {
    var re = /^([0-9]*)$/i;
    if (re.test(capacity)) {
        $.getJSON("RoomServlet?buildingID=" + buildingID + "&roomNumber=" + roomId + "&labType=" + labtype + "&roomCapacity=" + capacity + "&reqType=editing", displayRooms);
    } else {
        $('#messageEdit').html("Enter only numbers for capacity");
    }
}

function deleteBuild($this) {
    var build = $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').prev('td').html();
    var room = $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').html();
    if (window.confirm("Are you sure you want to delete this room??") === true)
    {
        $.getJSON("RoomServlet?buildingID=" + build + "&roomNumber=" + room + "&reqType=delete", displayRooms);
    }
}


