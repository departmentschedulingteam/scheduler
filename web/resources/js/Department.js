/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#tableValues').hide();
    $('input:radio[name=divType]').on('change', function () {
        var divType = $('input:radio[name=divType]:checked').val();
        if (divType === "dataEntry") {
            $('#dataEntry').show();
            $('#tableValues').hide();
            $('#messageInsert').html("");
            $('#displayDepartments').hide();
        } else {
            $('#dataEntry').hide();
            $('#tableValues').show();
            $('#messageEdit').html("");
        }

    });
    $('#viewDepartments').click(getDepartmentList);
    $('#submit1').click(function () {
        var departmentName = jQuery.trim($("#departmentID").val());
        var disciplineName = jQuery.trim($("#departmentName").val());
        var disciplineCode = jQuery.trim($("#departmentDesc").val());
        var isOk = false;
        var re = /^([0-9]*)$/i;
        if (departmentName.length === 0 || !re.test(departmentName)) {
            $('#departmentIdMessage').show();
            $('#departmentIdMessage').html("Enter id, numeric only");
            return;
        } else {
            $('#departmentIdMessage').hide();
            isOk = true;
        }
        var re = /^([a-zA-Z0-9 ]*)$/i;
        if (disciplineName.length === 0 || !re.test(disciplineName)) {
            $('#disciplineNameMessage').show();
            $('#disciplineNameMessage').html("Enter name, alphanumeric");
            return;
        } else {
            $('#disciplineNameMessage').hide();
            isOk = true;
        }

        if (disciplineCode.length === 0 || !re.test(disciplineCode)) {
            $('#disciplineCodeMessage').show();
            $('#disciplineCodeMessage').html("Enter value, only alphanumeric");
            return;
        } else {
            $('#disciplineCodeMessage').hide();
            isOk = true;
        }
        if (isOk)
            insertNewDepartment();
    });
}
);
function getDepartmentList() {
    $.getJSON("DepartmentServlet?reqType=getDepartmentList", displayTable);
}

function insertNewDepartment() {
    var departmentID = $('#departmentID').val();
    var departmentName = $('#departmentName').val();
    var departmentDesc = $('#departmentDesc').val();
    $.getJSON("DepartmentServlet?departmentID=" + departmentID + "&departmentName=" + departmentName + "&departmentDesc=" + departmentDesc + "&reqType=inserting", displayTable);
}

function displayTable(departmentList) {
    $('#displayDepartments').show();
    var divType = $('input:radio[name=divType]:checked').val();
    if (divType === "dataEntry") {
        $('#messageInsert').html(departmentList.message);
    } else {
        $('#messageEdit').html(departmentList.message);
    }


    $('#displayDepartments').empty();
    $('#displayDepartments').append("<tr>"
            + "<th>Discipline ID</th>"
            + "<th>Discipline Name</th>"
            + "<th>Discipline Description</th>"
            + "</tr>");
    $.each(departmentList.departments,
            function () {

                $('#displayDepartments').append("<tr>"
                        + "<td>" + this.departmentID + "</td>"
                        + "<td>" + this.departmentName + "</td>"
                        + "<td>" + this.departmentDesc + "</td>"
                        + "<td> <input type=\"button\" value=\"Edit\" onclick=\"makeEditable(this)\" class=\"btn btn-success\"></td>"

                        + "<td> <input type=\"button\" value=\"Delete\" onclick=\"deleteBuild(this)\" class=\"btn btn-danger\"></td>"

                        + "</tr>");
            });
}


function makeEditable($this) {

    var isString = $($this).parent().closest('td').prev('td').find('input').length === 0 ? true : false;
    if (isString) {
        var deptdesc = $($this).parent().closest('td').prev('td').text();
        var deptname = $($this).parent().closest('td').prev('td').prev('td').text();
        $($this).parent().closest('td').prev('td').prev('td').html("<input type=\"text\" id=\"editedDeptName\" value=\"" + deptname + "\"/>");
        $($this).parent().closest('td').prev('td').html("<input type=\"text\" id=\"editedDeptDesc\" value=\"" + deptdesc + "\"/>");
        $($this).val("Save");
    } else {
        if ($($this).val() !== "Edit") {
            $($this).on('click', editBuilding($this, $('#editedDeptName').val(), $('#editedDeptDesc').val(), $($this).parent().closest('td').prev('td').prev('td').prev('td').text()));
        }
    }
}

function editBuilding($this, deptName, deptDesc, deptId) {
    if ($($this).val() === "Save") {
        var re = /^([a-zA-Z0-9 ]*)$/i;
        if (re.test(deptName) && re.test(deptDesc) && re.test(deptId)) {
            $.getJSON("DepartmentServlet?departmentID=" + deptId + "&departmentName=" + deptName + "&departmentDesc=" + deptDesc + "&reqType=editDept", displayTable);
        } else {
            $('#messageEdit').html("Enter fields with only alphanumeric characters only");
        }
    }
}
function deleteBuild($this) {
    var deptId = $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').text();
    if (window.confirm("Are you sure, you want to delete this Discipline??") == true) {
        $.getJSON("DepartmentServlet?departmentID=" + deptId + "&reqType=destroy", displayTable);
    }
}

