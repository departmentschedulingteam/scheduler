/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#tableValues').hide();
    $('input:radio[name=divType]').on('change', function () {
        var divType = $('input:radio[name=divType]:checked').val();
        if (divType === "dataEntry") {
            $('#dataEntry').show();
            $('#tableValues').hide();
            $('#messageInsert').html("");
            $('#displayBuildings').hide();
        } else {
            $('#dataEntry').hide();
            $('#tableValues').show();
            $('#messageEdit').html("");
        }
    });
    $('#viewBuildings').click(getBuildingList);
    $('#submit1').click(function () {
        var re = /^([a-zA-Z0-9 ]*)$/i;
        var buildingID = jQuery.trim($("#buildingID").val());
        var buildingName = jQuery.trim($("#buildingName").val());
        var isOk = false;

        if (buildingID.length === 0 || !re.test(buildingID)) {
            $('#idMessage').show();
            $('#idMessage').html("Enter value and without &$%#@-*");
            return;
        }
        else {
            $('#idMessage').hide();
            isOk = true;
        }

        if (buildingName.length === 0 || !re.test(buildingName)) {
            $('#nameMessage').show();
            $('#nameMessage').html("Enter value and without &$%#@-*");
            return;
        }
        else if (buildingName.length >= 1) {
            $('#nameMessage').hide();
            isOk = true;
        }
        if (isOk)
            insertNewBuilding();

    });
}
);

function getBuildingList() {
    $('#displayBuildings').show();
    $.getJSON("BuildingServlet?reqType=getBuildingList", displayTable);
}

function insertNewBuilding() {
    var buildId = $('#buildingID').val();
    var buildName = $('#buildingName').val();
    $.getJSON("BuildingServlet?buildingID=" + buildId + "&buildingName=" + buildName + "&reqType=inserting", displayTable);
}

function displayTable(buildingList) {

    var divType = $('input:radio[name=divType]:checked').val();

    if (divType === "dataEntry") {
        $('#messageInsert').html(buildingList.message);

    } else {
        $('#messageEdit').html(buildingList.message);
    }

    $('#displayBuildings').empty();
    $('#displayBuildings').append("<tr>"
            + "<th>Building Id</th>"
            + "<th>Building Name</th>"
            + "</tr>");
    $.each(buildingList.buildings,
            function () {

                $('#displayBuildings').append("<tr>"
                        + "<td>" + this.buildingID + "</td>"
                        + "<td>" + this.buildingName + " </td>"
                        + "<td> <input type=\"button\" value=\"Edit\" onclick=\"makeEditable(this)\" class=\"btn btn-success\"></td>"
                        + "<td> <input type=\"button\" value=\"Delete\" onclick=\"deleteBuild(this)\" class=\"btn btn-danger\"></td>"
                        + "</tr>");
            });
}

function makeEditable($this) {

    var isString = $($this).parent().closest('td').prev('td').find('input').length === 0 ? true : false;
    if (isString) {
        var value = $($this).parent().closest('td').prev('td').text();
        $($this).parent().closest('td').prev('td').html("<input type=\"text\" id=\"editedBuildingId\" value=\"" + value + "\"/>");
        $($this).val("Save");
    } else {
        if ($($this).val() !== "Edit") {
            $($this).on('click', editBuilding($this, $('#editedBuildingId').val(), $($this).parent().closest('td').prev('td').prev('td').text()));
        }
    }
}
function deleteBuild($this) {

    if (window.confirm("Are you sure you want to delete this building?") == true) {
        $.getJSON("BuildingServlet?buildingID=" + $($this).parent().closest('td').prev('td').prev('td').prev('td').text() + "&reqType=deleteBuilding", displayTable);
    }


}

function editBuilding($this, textVal, buildingId) {

    if ($($this).val() === "Save") {
        var re = /^([a-zA-Z0-9 ]*)$/i;

        if (!re.test(textVal)) {
            $('#messageEdit').html("Enter values and without !@#$%^&*_-~`<>?/{}");
        } else {

            $.getJSON("BuildingServlet?buildingID=" + buildingId + "&buildingName=" + textVal + "&reqType=editBuilding", displayTable);
        }
    }
}

