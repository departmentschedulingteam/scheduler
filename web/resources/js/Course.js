/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $(getDepartmentList);
    $('#tableValues').hide();
    $('input:radio[name=divType]').on('change', function () {
        var divType = $('input:radio[name=divType]:checked').val();

        if (divType === "dataEntry") {
            $('#dataEntry').show();
            $('#tableValues').hide();
            $('#messageInsert').html("");
            $('#courseTable').hide();
        } else {
            $('#dataEntry').hide();
            $('#tableValues').show();
            $('#messageEdit').html("");
            $(getDepartmentList);
        }
    });
    $('#submit1').click(function () {

        var courseId = jQuery.trim($("#courseID").val());
        var courseName = jQuery.trim($("#courseName").val());
        var isOk = false;
        var re = /^([0-9]{3})$/i;

        if (courseId.length === 0 || !re.test(courseId)) {
            $('#courseIdMessage').show();
            $('#courseIdMessage').html("*Enter Id, only numeric 3 digits");
            return;
        } else {
            $('#courseIdMessage').hide();
            isOk = true;
        }
        var re = /^([a-zA-Z0-9 ]*)$/i;
        if ($('#departmentID').val() === "select")
        {
            $('#departmentNameMessage').show();
            $('#departmentNameMessage').html("*Select Discipline");
            return;
        }
        else {
            $('#departmentNameMessage').hide();
            isOk = true;
        }
        if ($("input[name='labType']:checked").length <= 0) {
            $('#labTypeMessage').show();
            $('#labTypeMessage').html("*Select type of lab");
            return;
        }
        else {
            $('#labTypeMessage').hide();
            isOk = true;
        }
        if (courseName.length === 0 || !re.test(courseName)) {
            $('#courseNameMessage').show();
            $('#courseNameMessage').html("Enter name, only alphanumeric");
            return;
        }
        else {
            $('#courseNameMessage').hide();
            isOk = true;
        }
        if (isOk)
            insertNewCourse();
    });
});
function getDepartmentList() {
    $.getJSON("DepartmentServlet?reqType=getDepartmentList", displayDepartment);
}

function displayDepartment(departmentList) {
    $("#departmentID").empty().append($("<option></option>").val("select").html("Please select Discipline"));
    $("#departmentList").empty().append($("<option></option>").val("select").html("Please select Discipline"));
    $.each(departmentList.departments,
            function () {
                $('#departmentID').append($("<option></option>").val(this['departmentID']).html(this['departmentName']));
                $('#departmentList').append($("<option></option>").val(this['departmentID']).html(this['departmentName']));
            });
}

function getCourses() {
    var departmentID = $('#departmentList').val();
    if (departmentID === "select") {
        alert("Please select a department to view courses");
    } else {
        $.getJSON("CourseServlet?reqType=getCourseList&departmentID=" + departmentID, displayCourses);
    }
}

function displayCourses(CourseList) {
    $('#courseTable').show();
    $('#courseTable').empty();
    $('#courseTable').append("<tr>"
            + "<th>Department ID</th>"
            + "<th>Course ID</th>"
            + "<th>Lab Type</th>"
            + "<th>Course Name</th>"
            + "</tr>");
    $.each(CourseList.courses,
            function () {

                $('#courseTable').append("<tr>"
                        + "<td>" + this.deptID + "</td>"
                        + "<td>" + this.courseID + "</td>"
                        + "<td>" + this.labType + "</td>"
                        + "<td>" + this.courseName + "</td>"

                        + "<td> <input type=\"button\" value=\"Edit\" onclick=\"redirectToEditCourse(this)\" class=\"btn btn-success\"></td>"
                        + "<td> <input type=\"button\" value=\"Delete\" onclick=\"deleteCourse(this)\" class=\"btn btn-danger\"></td>"

                        + "</tr>");
            });
}

function insertNewCourse() {
    var courseID = $('#courseID').val();
    var departmentID = $('#departmentID').val();
    var labType = $('input:radio[name=labType]:checked').val();
    var courseName = $('#courseName').val();
    $.getJSON("CourseServlet?courseID=" + courseID + "&departmentID=" + departmentID + "&labType=" + labType + "&courseName=" + courseName + "&reqType=inserting", displayMessage);
}

function displayMessage(courseMessage) {
    var divType = $('input:radio[name=divType]:checked').val();
    if (divType === "dataEntry") {
        $('#messageInsert').html(courseMessage.message);

    } else {
        $('#messageEdit').html(courseMessage.message);
    }
}


function redirectToEditCourse($this) {
    var x = $($this).parent().closest('td').prev('td').prev('td').prev('td').html() + "," + $($this).parent().closest('td').prev('td').prev('td').html() + "," + $($this).parent().closest('td').prev('td').html() + "," + $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').html();

    $.cookie("values", x);

    window.location.href = "editCourse.jsp";
}

function deleteCourse($this) {
    if (window.confirm("Are you sure you want to delete this Course??") == true)
        $.getJSON("CourseServlet?courseID=" + $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').html() + "&departmentID=" + $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').prev('td').html() + "&reqType=delete", displayMessage);
}

