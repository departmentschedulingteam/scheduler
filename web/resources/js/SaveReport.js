/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#delete').click(deleteReport);
});

function deleteReport() {
    if (window.confirm("Do you want to delete the current courses assigned to professors sections, and labs.") === true)
    {
        $.getJSON("SaveReport?reqType=deleteReport", displayMessage);
    }
}

function displayMessage(sectionEditMessage) {
    alert("message");
    $('#message').html(sectionEditMessage.message);
}