/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#detailsDiv').hide();
    $(getDepartmentList);
    $("#departmentID").change(function () {
        getCourseList($(this).val());
    });
    $('#submit').click(function () {

        if ($('#departmentID').val() === "select")
        {
            $('#departmentNameMessage').show();
            $('#departmentNameMessage').html("*Select Discipline");
            return;
        }
        else {
            $('#departmentNameMessage').hide();
            isOk = true;
        }
        if ($('#course').val() === "select")
        {
            $('#courseNameMessage').show();
            $('#courseNameMessage').html("*Select Course");
            return;
        }
        else {
            $('#courseNameMessage').hide();
            isOk = true;
        }
        if (isOk)
            viewSectionLabLoad();
    });
});

function viewSectionLabLoad() {
    var courseID = $('#course').val();
    var deptId = $('#departmentID').val();
    $.getJSON("SectionModifyServlet?reqType=getSectionLabIDList&courseID=" + courseID+"&deptId="+deptId, displaySectionLab);
}

function displaySectionLab(list) {
    $('#message').html("");
    $('#message').html(list.message);
    $('#detailsDiv').hide();
    $('#displaySection').empty();
    $('#displaySection').append("<tr>"
            + "<th>Section/Lab Id</th>"
            + "<th>Professor Name</th>"
            + "<th>Building Room</th>"
            + "<th>Seats</th>"
            + "<th style=\"width: 200px\">Time Slots</th>"
            + "</tr>");
    $.each(list.sectionLabList,
            function () {
                if (this.type === "Lab") {
                    $('#displaySection').append("<tr>"
                            + "<td>" + this.sectionLabFullID + "</td>"
                            + "<td>" + this.professorName + "</td>"
                            + "<td>" + this.buildingRoom + "</td>"
                            + "<td>" + this.seats + "</td>"
                            + "<td>" + this.timeSlots + "</td>"
                            + "<td> <input type=\"button\" value=\"Edit\" onclick=\"redirectToEditLab('" + this.sectionLabFullID + "')\" class=\"btn btn-success\"></td>"
                            + "<td> <input type=\"button\"  value=\"Delete\" onclick=\"deleteLab('" + this.sectionLabFullID + "')\" class=\"btn btn-danger\"></td>"
                            + "</tr>");
                } else if (this.type === "NotLab" && this.hasLab === "YES") {
                    $('#displaySection').append("<tr>"
                            + "<td>" + this.sectionLabFullID + "</td>"
                            + "<td>" + this.professorName + "</td>"
                            + "<td>" + this.buildingRoom + "</td>"
                            + "<td>" + this.seats + "</td>"
                            + "<td>" + this.timeSlots + "</td>"
                            + "<td> <input type=\"button\" value=\"Edit\" onclick=\"redirectToEditSection('" + this.sectionLabFullID + "')\" class=\"btn btn-success\"></td>"
                            + "<td> <input type=\"button\" disabled value=\"Delete\" onclick=\"deleteSection('" + this.sectionLabFullID + "')\"class=\"btn btn-default\" style=\"background-color:grey;\" ></td>"
                            + "</tr>");
                } else {
                    $('#displaySection').append("<tr>"
                            + "<td>" + this.sectionLabFullID + "</td>"
                            + "<td>" + this.professorName + "</td>"
                            + "<td>" + this.buildingRoom + "</td>"
                            + "<td>" + this.seats + "</td>"
                            + "<td>" + this.timeSlots + "</td>"
                            + "<td> <input type=\"button\" value=\"Edit\" onclick=\"redirectToEditSection('" + this.sectionLabFullID + "')\" class=\"btn btn-success\"></td>"
                            + "<td> <input type=\"button\" value=\"Delete\" onclick=\"deleteSection('" + this.sectionLabFullID + "')\" class=\"btn btn-danger\"></td>"
                            + "</tr>");
                }
                $('#detailsDiv').show();
            });
}

function redirectToEditLab(labID) {
    var labFullID="", deptID="", deptName="", cour="", courName="", sec="", profID="", profNa="", buil="", roo="", seats="", timeS="";
    $.getJSON("LabModifyServlet?reqType=getLabDetails&labID=" + labID, function(labDetailsJSON){
        $.each(labDetailsJSON.labDetails, function(){
            labFullID = this.labFullID;
            deptID = this.departmentID;
            deptName = this.departmentName;
            cour = this.courseID;
            courName = this.courseName;
            sec = this.sectionID;
            profID = this.professorID;
            profNa = this.professorName;
            buil = this.buildingID;
            roo = this.roomID;
            seats = this.seats;
            timeS = this.timeSlots;
        });
        var labDetails = labFullID+"_"+deptID+"_"+deptName+"_"+cour+"_"+courName+"_"+sec+"_"+profID+"_"+profNa+"_"+buil+"_"+roo+"_"+seats+"_"+timeS;
    $.cookie("labDetails", labDetails);
    window.location.href = "LabEditPage.jsp";
    });
}

function deleteLab(labID) {
    if (window.confirm("Are you sure you want to delete this Lab?") === true)
        $.getJSON("LabModifyServlet?labID=" + labID + "&reqType=delete", deleteRefreshNow);
}

function redirectToEditSection(sectionID) {
    var sectionFullID="", deptID="", deptName="", cour="", courName="", sec="", profID="", profNa="", buil="", roo="", seats="", timeS="";
    $.getJSON("SectionModifyServlet?reqType=getSectionDetails&sectionID=" + sectionID, function(sectionDetailsJSON){
        $.each(sectionDetailsJSON.sectionDetails, function(){
            sectionFullID = this.sectionFullID;
            deptID = this.departmentID;
            deptName = this.departmentName;
            cour = this.courseID;
            courName = this.courseName;
            sec = this.sectionID;
            profID = this.professorID;
            profNa = this.professorName;
            buil = this.buildingID;
            roo = this.roomID;
            seats = this.seats;
            timeS = this.timeSlots;
        });
        var sectionDetails = sectionFullID+"_"+deptID+"_"+deptName+"_"+cour+"_"+courName+"_"+sec+"_"+profID+"_"+profNa+"_"+buil+"_"+roo+"_"+seats+"_"+timeS;
    $.cookie("sectionDetails", sectionDetails);
    window.location.href = "SectionEditPage.jsp";
    });
    
}

function deleteSection(sectionID) {
    if (window.confirm("Are you sure you want to delete this Section?") === true)
        $.getJSON("SectionModifyServlet?sectionID=" + sectionID + "&reqType=delete", deleteRefreshNow);
}

function deleteRefreshNow(deleteMessage){
    $('#message').html(deleteMessage.message);
    viewSectionLabLoad();
}

function displayMessage(sectionEditMessage) {
    $('#message').html(sectionEditMessage.message);
}

function getCourseList(dept) {
    $.getJSON("CourseServlet?reqType=getCourseList&departmentID=" + dept, displayCourse);
}

function displayCourse(courseList) {
    $("#course").empty().append($("<option></option>").val("select").html("Please select Course"));
    $.each(courseList.courses,
            function () {
                $('#course').append($("<option></option>").val(this['courseID']).html(this['courseName']));
            });
}

function getDepartmentList() {
    $.getJSON("DepartmentServlet?reqType=getDepartmentList", displayDepartment);
}

function displayDepartment(departmentList) {
    $("#departmentID").empty().append($("<option></option>").val("select").html("Please select Department"));
    $.each(departmentList.departments,
            function () {
                $('#departmentID').append($("<option></option>").val(this['departmentID']).html(this['departmentName']));
            });
}

