/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#tableValues').hide();
    $('input:radio[name=divType]').on('change', function () {
        var divType = $('input:radio[name=divType]:checked').val();

        if (divType === "dataEntry") {
            $('#dataEntry').show();
            $('#tableValues').hide();
            $('#messageInsert').html("");
            $('#displayProfessors').hide();
        } else {
            $('#dataEntry').hide();
            $('#tableValues').show();
            $('#messageEdit').html("");
        }

    });

    $(addCourses);
    $('#viewProfessors').click(getProfessorList);
    $('#submit1').click(function () {
        var professorID = jQuery.trim($("#professorID").val());
        var firstName = jQuery.trim($("#firstName").val());
        var lastName = jQuery.trim($("#lastName").val());
        var professorCode = jQuery.trim($("#professorCode").val());
        var courses = jQuery.trim($("#addCourses").val());

        var isOk = false;
        var re = /^([a-zA-Z0-9 ]*)$/i;
        if (professorID.length === 0 || !re.test(professorID)) {
            $('#professorIDMessage').show();
            $('#professorIDMessage').html("*Enter ID, only alphanumeric");
            return;
        } else {
            $('#professorIDMessage').hide();
            isOk = true;
        }
        if (firstName.length === 0 || !re.test(firstName)) {
            $('#firstNameMessage').show();
            $('#firstNameMessage').html("*Enter name, only alphanumeric");
            return;
        } else {
            $('#firstNameMessage').hide();
            isOk = true;
        }
        if (lastName.length === 0 || !re.test(lastName)) {
            $('#lastNameMessage').show();
            $('#lastNameMessage').html("Enter last value,only alphanumeric");
            return;
        } else {
            $('#lastNameMessage').hide();
            isOk = true;
        }
        if (professorCode.length === 0 || !re.test(professorCode)) {
            $('#professorCodeMessage').show();
            $('#professorCodeMessage').html("Enter code, only alphanumeric");
            return;
        } else {
            $('#professorCodeMessage').hide();
            isOk = true;
        }
        if ($('input[type=checkbox]:checked').length === 0) {
            $('#coursesMessage').show();
            $('#coursesMessage').html("*This field is required");
            return;
        } else {
            $('#coursesMessage').hide();
            isOk = true;
        }

        if (isOk) {
            insertNewProfessor();
        }
    });

}
);

function addCourses() {
    $.getJSON("CourseServlet?reqType=getFullCoursesList", addCoursesToList);
}

function addCoursesToList(coursesList) {
    $('#message').html(coursesList.message);
    $('#addCourses').empty();
    $.each(coursesList.courses,
            function () {

                $('#addCourses').append("<input type=\"checkbox\" name=\"courseList\" value=" +
                        this.departmentID + "-" + this.courseID + ">" + this.departmentID + "-" + this.courseID + ":-" + this.courseName + "</input><br>");
            });
}

function getProfessorList() {
    $.getJSON("ProfessorServlet?reqType=getProfessorList", displayTable);
}

function insertNewProfessor() {
    var professorID = $('#professorID').val();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var professorCode = $('#professorCode').val();
    var checkedValues = $("input[name='courseList']:checked").map(function () {

        return $(this).val();
    }).get();
    var checkedValuesString = "";
    for (i = 0; i < checkedValues.length; i++) {

        checkedValuesString += "&courses=" + checkedValues[i];
    }
    var surveyCode = "";
    $.getJSON("ProfessorServlet?professorID=" + professorID + "&firstName=" + firstName +
            "&lastName=" + lastName + "&professorCode=" + professorCode +
            checkedValuesString + "&surveyCode=" + surveyCode + "&reqType=inserting", displayMessage);
}

function displayMessage(professorMessage) {

    var divType = $('input:radio[name=divType]:checked').val();
    if (divType === "dataEntry") {
        $('#messageInsert').html(professorMessage.message);

    } else {
        $('#messageEdit').html(professorMessage.message);
    }

}

function displayTable(professorList) {
    $('#displayProfessors').show();
    $('#displayProfessors').empty();
    $('#displayProfessors').append("<tr>"
            + "<th>Professor ID</th>"
            + "<th>First Name</th>"
            + "<th>Last Name</th>"
            + "<th>Professor Code</th>"
            + "<th>Courses</th>"

            + "</tr>");
    $.each(professorList.professors,
            function () {

                $('#displayProfessors').append("<tr>"
                        + "<td>" + this.professorID + "</td>"
                        + "<td>" + this.firstName + "</td>"
                        + "<td>" + this.lastName + "</td>"
                        + "<td>" + this.professorCode + "</td>"
                        + "<td>" + this.courses + "</td>"

                        + "<td> <input type=\"button\" value=\"Edit\" onclick=\"makeEditable(this)\" class=\"btn btn-success\"></td>"
                        + "<td> <input type=\"button\" value=\"Delete\" onclick=\"deleteProf(this)\" class=\"btn btn-danger\"></td>"

                        + "</tr>");
            });
}



function makeEditable($this) {

    var x = $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').prev('td').html() + "_" + $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').html() + "_" + $($this).parent().closest('td').prev('td').prev('td').prev('td').html() + "_" + $($this).parent().closest('td').prev('td').prev('td').html() + "_" + $($this).parent().closest('td').prev('td').html();

    $.cookie("profvalues", x);

    window.location.href = "EditProfessor.jsp";
}

function deleteProf($this) {

    var professorID = $($this).parent().closest('td').prev('td').prev('td').prev('td').prev('td').prev('td').prev('td').html();

    if (window.confirm("Are you sure you want to delete this Professor??") == true) {
        $.getJSON("ProfessorServlet?professorID=" + professorID + "&reqType=delete", displayMessage);
    }

}
