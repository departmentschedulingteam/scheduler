/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('.checkbo').click(function () {
        var colours = ["AntiqueWhite", "yellow", "Lavender", "BurlyWood", "Aqua", "Linen", "Aquamarine", "Azure", "Bisque", "LightBlue", "PaleGreen", "Thistle", "paleblue", "SpringGreen", "cornsilk", "GreenYellow", "Khaki"];
        var proffes = document.getElementsByClassName('checkbo');
        for (j = 0, k=0; j < proffes.length; j++, k++) {
            if(k>=colours.length){
                k=0;
            }
            if (document.getElementById(proffes[j].id).checked) {
                var elements = document.getElementsByClassName(proffes[j].id);
                var str = proffes[j].id;
                $('.' + str).show();
                for (i = 0; i < elements.length; i++) {
                    elements[i].style.backgroundColor = colours[k];
                }
            } else {
                var elements = document.getElementsByClassName(proffes[j].id);
                var str = proffes[j].id;
                $('.' + str).hide();
            }
        }
    });
    $('#showALl').click(function () {
        var proffes = document.getElementsByClassName('checkbo');
        for (j = 0; j < proffes.length; j++) {
            document.getElementById(proffes[j].id).checked = false;
            var elements = document.getElementsByClassName(proffes[j].id);
            var str = proffes[j].id;
            $('.' + str).show();
            for (i = 0; i < elements.length; i++) {
                elements[i].style.backgroundColor = 'transparent';
            }
        }
    });

    $('#exportToExcel').click(function () {

        $("#table2excel").table2excel({
            name: "Schedule"
        });
        setTimeout( function() { window.location.reload() }, 100 );
        
    });

    $('#print').click(function () {

        window.print();
    });
});

