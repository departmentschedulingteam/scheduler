<%-- 
    Document   : HomeDuplicate
    Created on : 3 Apr, 2015, 9:08:53 AM
    Author     : sandeep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/jquery.table2excel.js"></script>
        <script src="./resources/js/jquery.cookie.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/js/bootstrap-hover-dropdown.js" type="text/javascript"></script>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
        <script>
            $(function() {
                $("#accordion").accordion({
                    heightStyle: "content"
                });
            });
        </script>
    </head>
    <body>
        <h2 class="text-center">Department Scheduling</h2>
        <div class="row">
            <div class="col-md-8 col-sm-8 col-sm-offset-2">
                <nav class="navbar navbar-inverse no-margin" style="background-color: green; color: blue">
                    <ul class="nav nav-pills ">
                        <li role="presentation" class="col-lg-push-3 col-sm-push-3 dropdown"><a href="ManagementDuplicate.jsp" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="Building.jsp">Building</a></li>
                                <li role="presentation"><a href="Room.jsp">Room</a></li>   
                                <li role="presentation"><a href="Department.jsp">Discipline</a></li>
                                <li role="presentation"><a href="Course.jsp">Course</a></li>   
                                <li role="presentation"><a href="Professor.jsp">Professor</a></li>  
                                <li role="presentation"><a href="SectionEdit.jsp">Section/Lab</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="col-lg-push-3 col-sm-push-3">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Scheduling Section/Lab
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="Scheduler.jsp">Add Section</a></li>
                                <li role="presentation"><a href="Lab.jsp">Add Lab</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="col-lg-push-3 col-sm-push-3"><a href="" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Report
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="ReportPage.jsp">View Professor Report</a></li>
                                <li role="presentation"><a href="ReportRoomPage.jsp">View Room Report</a></li> 
                                <li role="presentation"><a href="SaveReport.jsp">Clear Report</a></li>

                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">

            </div>

            <div class="col-md-8">
                <div id="accordion">
                    <h3>General</h3>
                    <div>
                        <p style="color: red">Be sure to check with application owner before you start, editing or deleting.</p>
                        <p>Please use <b>Google Chrome</b> for a better user experience.</p>
                        <p>Please enable your browser cookies.</p>
                        <p>All fields are mandatory.</p>
                        </div>
                    <h3>Buildings(add/edit)</h3>
                    <div>
                        <ul>
                            <li>Hover over the Management tab and select Building</li>
                            <li>Select the appropriate radio button, and enter all requested information
                                <ul>
                                    <li>The Building Code is the uppercase initials of the building name.</li>
                                    <li>To edit or delete a building, select the options provided when you click on Edit/View Buildings button.</li>
                                </ul>    
                            </li>
                            <li>Click Submit</li>
                        </ul>
                    </div>
                    <h3>Rooms(add/edit)</h3>
                    <div>
                        <ul>
                            <li>Hover over the Management tab and select Room</li>
                            <li>Select the appropriate radio button, and enter all requested information
                                <ul>
                                    <li>To edit or delete a building, select the options provided when you click on Edit/View Rooms button.</li>
                                </ul>
                            </li>
                            <li>Click Submit</li>
                        </ul>

                    </div>
                    
                    <h3>Couses(add/edit)</h3>
                    <div>
                        <ul>
                            <li>Hover over the Management tab and select Course</li>
                            <li>Select the appropriate radio button, and enter all requested information
                                <ul>
                                    <li>To edit or delete a course, select the options provided when you click on Edit/View Courses button.</li>
                                </ul>
                            </li>
                            <li>Click Submit</li>
                        </ul>
                    </div>
                    <h3>Professors(add/edit)</h3>
                    <div>
                        <ul>
                            <li>Hover over the Management tab and select Professor.</li>
                            <li>Select the appropriate radio button, and enter all requested information.
                                <ul>
                                    <li>The Professor ID is the three-digit code used for course evaluations</li>
                                    <li>The Professor Initials are the uppercase letters of the first and last name.</li>
                                    <li>To edit or delete a professor, select the options provided when you click on Edit/View Professor button.</li>
                                </ul>
                            </li>
                            <li>Click Submit</li>
                        </ul>

                    </div>
                    <h3>Scheduler</h3>
                    <div>
                        <ul>
                            <li>To schedule a class, hover over Schedule and select Section, enter all of the requested information, and click Submit. 
                                <ul>
                                    <li>If you get a soft conflict alert, you may Allow or Deny the conflict.</li>
                                    <li>If you get a hard conflict, you may read the conflict, but you will need to change the days, times, and/or professor in order to fix the conflict.</li>
                                </ul>
                                You may verify that the course has been added by viewing Report, below.
                            </li>
                            <li>To schedule a lab, you must first schedule the section of the course to which the lab is to be assigned. Hover over Schedule and select Lab, enter all of the requested information, and click Submit. 
                                <ul>
                                    <li>If you get a soft conflict alert, you may Allow or Deny the conflict.</li>
                                    <li>If you get a hard conflict, you may read the conflict, but you will need to change the days, times, and/or professor in order to fix the conflict.</li>
                                </ul>
                                You may verify that the course has been added by viewing Reports, below.
                            </li>
                        </ul>


                    </div>
                    <h3>Report</h3>
                    <div>
                        <ul>
                            <li>Report by Professor:
                                <ul>
                                    <li>Hover over Report and select View Professor Report.</li>
                                    <li>Check the instructor for whom you want a report. Note that lab classes will be displayed using  *course number*.</li>
                                    <li>You may select Print or Export to Excel.</li>
                                </ul>
                            </li>
                            <li>Report by Room:
                                <ul>
                                    <li>Hover over Report and select View Room Report.</li>
                                    <li>Check the rooms for which you want a report. Note that lab classes will be displayed using  *course number*.</li>
                                    <li>You may select Print or Export to Excel.</li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <h3>Creating a new schedule</h3>
                    <div>
                        <ul>
                            <li>Clear Report (the third option when hovering over Report) will clear all of the sections and labs but will not clear Professors or Buildings.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
