<%-- 
    Document   : Professor
    Created on : Nov 17, 2014, 2:41:25 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Professor</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="./resources/js/Professor.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-3">
            <br>
            <div class="col-sm-offset-3">
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="dataEntry" id="dataEntryRadio" checked="checked">Create Professor</label>
                </div>
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="tableValues" id="tableValuesRadio">Edit or View Professor</label>
                </div>
            </div>
            <div id="dataEntry">
                <h2 class="col-sm-offset-3">Add Professor</h2>
                <table id="entry">
                    <form>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Professor ID</span>
                                    <input type="text" name="professorID" class="form-control" id="professorID" style="width: 6.6cm">
                                </div>
                                <span id="professorIDMessage" style="color: red"  class="danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">First Name</span>
                                    <input type="text" name="firstName" class="form-control" id="firstName" style="width: 6.6cm">
                                </div>
                                <span id="firstNameMessage" style="color: red" class="danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Last Name</span>
                                    <input type="text" name="lastName" class="form-control" id="lastName" style="width: 6.6cm">
                                </div>
                                <span id="lastNameMessage" style="color: red" class="danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Professor Initials</span>
                                    <input type="text" name="professorCode" class="form-control" id="professorCode" style="width: 6.6cm">
                                </div>
                                <span id="professorCodeMessage" style="color: red" class="danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Course</span>
                                    <div class="container" id="addCourses">
                                    </div>
                                </div>
                                <span id="coursesMessage" style="color: red" class="danger control-label"></span>
                            </div>
                        </div>
                        <br>
                        <div class="col-sm-1 col-sm-offset-4">
                            <input type="button" value='Submit' class="btn btn-success" id="submit1">
                        </div>
                        <br><br>
                        <div class="col-sm-offset-4">
                            <span id="messageInsert" class="control-label"></span>
                        </div>
                    </form>
                </table>
            </div>
            <div id="tableValues">
                <br>
                <div class="col-sm-1 col-sm-offset-4">
                    <input type="button" value="View/ Edit Professors" class="btn btn-success" id="viewProfessors" />
                </div>
                <br>
                <br><br>
                <div class="form-inline input-margin-bottom">
                    <div class="form-group">
                        <p>N - No Sections related to this course</p>
                    <p>R - Sections are related to this course</p>
                        <table id="displayProfessors" class="table table-bordered table-striped table-hover" border="2px">
                        </table>
                    </div>
                </div>
                <div>
                    <span id="messageEdit" class="control-label"></span>
                </div>
            </div>
    </body>
</html>
