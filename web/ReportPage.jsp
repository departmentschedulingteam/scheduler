<%-- 
    Document   : ReportPage
    Created on : Dec 10, 2014, 9:36:57 PM
    Author     : S519479
--%>

<%@page import="com.scheduler.persistence.controller.ProfessorJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="com.scheduler.persistence.Professor"%>
<%@page import="com.scheduler.helper.ReportHelperBean"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.scheduler.helper.ReportHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Professor Report</title>
        <script src="./resources/js/jquery.min.js"></script>
        
        <script type="text/javascript" src="./resources/js/Report.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
        <link rel="stylesheet" type="text/css" href="./resources/css/report.css">
        <style type="text/css">
            .tftable {font-size:16px;font-family: Tahoma;color:#333333;width:70%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
            .tftable th {font-size:16px;background-color:#acc8cc;width:100px;border-width: 1px;border-style: solid;border-color: #729ea5;text-align:center;}
            .tftable tr {background-color:#d4e3e5; width:14%}
            .tftable td {font-size:16px;border-width: 1px;border-style: solid;border-color: #729ea5;}
        </style>
    </head>
    <body>
        <jsp:include page="Home.jsp"/>

        <div id="body">
            <h4 style="color: green">Select Professor to see his/her classes highlighted</h4>
            <input type ='button' value='Print' class="btn btn-info" id="print">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <input type ='button' value='Export To Excel' class="btn btn-primary" id="exportToExcel"><br><br>
            <div class="container-report">
            <%
                ProfessorJpaController professorController = new ProfessorJpaController(Persistence.createEntityManagerFactory("SchedulerTomcatPU"));
                for (Professor prof : professorController.findProfessorEntities()) {
                    out.println("<input type=\"checkbox\" class=\"checkbo\" id=\""
                            + prof.getProfessorId() + "\">" + prof.getFirstName() + " " + prof.getLastName()+"</br>");
                }
            %>
            </div>
            <br><br><br><br><br><br>
            <input type="button" class="btn btn-success" id="showALl" value="Show All"/>
            <br><br>
        </div>
        <table class="table table-bordered table-responsive table-striped table-hover" id ="table2excel" border="1">
            <tr>
                <th>Timeslot</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
                <th>Saturday</th>
                <th>Sunday</th>
            </tr>
            <%
                String timeSlots[] = {"08:00 - 08:30", "08:30 - 09:00", "09:00 - 09:30", "09:30 - 10:00",
                    "10:00 - 10:30", "10:30 - 11:00", "11:00 - 11:30", "11:30 - 12:00", "12:00 - 12:30", "12:30 - 13:00",
                    "13:00 - 13:30", "13:30 - 14:00", "14:00 - 14:30", "14:30 - 15:00", "15:00 - 15:30", "15:30 - 16:00",
                    "16:00 - 16:30", "16:30 - 17:00", "17:00 - 17:30", "17:30 - 18:00", "18:00 - 18:30", "18:30 - 19:00",
                    "19:00 - 19:30", "19:30 - 20:00", "20:00 - 20:30"};
                ReportHelper rHelper = new ReportHelper();
                ArrayList<HashMap> arr = rHelper.getReport();
                HashMap tMap = null;
                for (int i = 0; i < arr.size(); i++) {

                    tMap = arr.get(i);
            %><tr><td><%
                out.println(timeSlots[i]);
                    %></td><%
                        ArrayList days = (ArrayList) tMap.get(timeSlots[i]);
                        for (int j = 0; j < days.size(); j++) {
                    %><td><%
                        ArrayList daysData = (ArrayList) days.get(j);

                        for (int p = 0; p < daysData.size(); p++) {
                            ReportHelperBean rhBean = (ReportHelperBean) daysData.get(p);
                            out.println("<div class=" + rhBean.getProfessorID() + ">" + rhBean.getSectionFullID() + " "
                                    + rhBean.getProfessorCode() + " "
                                    + rhBean.getBuildingRoom() + "</div>");
                        }


                    %></td><%                            }
                %></tr><%
                    }
                %>
        </table>

    </body>
</html>
