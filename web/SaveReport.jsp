<%-- 
    Document   : SaveReport
    Created on : Mar 11, 2015, 11:20:33 AM
    Author     : S519479
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Delete Report</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/SaveReport.js" ></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">

    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div id="saveReport" class="col-md-offset-2 col-sm-offset-2">
            <form>
                <%--<table id="saveReportTable">
                    <tr>
                        <td> Year:</td>
                        <td>
                            <select name="year" id="year" class="dropdown">
                                <option value="select">Select Year</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                            </select>
                        </td>
                        <td><span id="courseIdMessage" class="redColor"></span></td></tr>
                    </tr>
                    <tr>
                        <td> Semester:</td>
                        <td>
                            <select name="semester" id="semester" class="dropdown">
                                <option value="select">Select Semester</option>
                                <option value="spring">Spring</option>
                                <option value="summer">Summer</option>
                                <option value="fall">Fall</option>
                            </select>
                        </td>
                        <td><span id="courseIdMessage" class="redColor"></span></td></tr>
                    </tr>
                    <tr>
                        <td> Report Name:</td>
                        <td>
                            <input type="text" name="reportName" id="reportName">
                        </td>
                        <td><span id="courseIdMessage" class="redColor"></span></td></tr>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="button" name="submit" id="submit" value="Submit">
                        </td>
                    </tr>
                </table>--%>
                <br>
                <div class="col-sm-offset-2">
                    Delete the current courses assigned to professors sections, and labs.
                </div>
                <div class="col-sm-offset-4">
                    <input type="button" id="delete" class="btn btn-success" value='Delete'>
                </div>
<!--                <table>
                    <tr>
                        <td>
                            Delete your current courses tagged to professors, section & lab data
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-sm-2 col-sm-offset-5">
                                <input type="button" id="delete" class="btn btn-success" value='Delete'>
                            </div>
                        </td>
                    </tr>-->
                    <%--
                    <tr>
                        <td></td><td><input type="button" value="View Legacy Reports" class="button" id="viewLegacyReports" /></td>
                    </tr>--%>
                <!--</table>-->
            </form>
        </div>
        <br>
        <br>
        <br><br><br><br><br><br><br>
        <div>
            <span id="message" name="message"></span>
        </div>
        <table id="displayLegacyReports" border="2px" class="displayBuildings">
        </table>

    </body>
</html>
