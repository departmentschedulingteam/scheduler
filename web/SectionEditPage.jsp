<%-- 
    Document   : SectionEditPage
    Created on : Apr 6, 2015, 3:10:53 PM
    Author     : S519479
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Section</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script type="text/javascript" src="./resources/js/SectionEditPage.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Home.jsp"/>
        <div class="col-md-11">
            <h2 class="schedule-heading-margin-left">Edit Section</h2>

            <div id="section" class="col-sm-offset-1">
                <form>
                    <table id="courseSectionEntry">
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Discipline</span>
                                    
                                    <input type="text" name="departmentID" class="form-control" id="departmentID" style="width: 4.5cm">
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Course</span>
                                    
                                    <input type="text" name="course" class="form-control" id="course" style="width: 4.5cm">
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Section Number</span>
                                    <input type="text" name="sectionNumber" class="form-control" id="sectionNumber" style="width: 4.5cm">
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Professor</span>
                                    
                                    <input type="text" name="professor" class="form-control" id="professor" style="width: 4.5cm">
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Building</span>
                                    <select name="building" id="building" class="form-control" style="width: 4.5cm">
                                        <option value="select">loading...</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Room</span>
                                    <select name="room" id="room" class="form-control" style="width: 4.5cm">
                                        <option value="select">loading...</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Seats</span>
                                    <input type="text" name="seats" class="form-control" id="seats" style="width: 4.5cm">
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Start Time</span>
                                    <select name="startTime" id="startTime" class="form-control" style="width: 4.5cm">
                                        <option value="select">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">End Time</span>
                                    <select name="endTime" id="endTime" class="form-control" style="width: 4.5cm">
                                        <option value="select">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon">Day</span>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="sectionDay" id="M" value="M"/>M
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="sectionDay" id="T" value="T"/>T
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="sectionDay" id="W" value="W"/>W
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="sectionDay" id="Th" value="Th"/>Th
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="sectionDay"id="F" value="F"/>F
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="sectionDay" id="Sa" value="Sa"/>Sa
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 col-sm-offset-6">
                            <input type="button" id="submit" class="btn btn-success" value='Submit'>
                        </div>
                    </table>
                </form>
            </div>
            <br>
            <br>
            <div>
                <div class="col-sm-offset-4">
                    <span id="messageScheduler" name="message"></span>
                </div>
            </div>

            <div id="detailsDiv">
                <div class="col-sm-offset-4">
                    <table id="displaySection" class="table table-bordered" border= "2px">
                        <tr>
                            <th>Section/Lab Id</th>
                            <th>Professor Name</th>
                            <th>Building Room</th>
                            <th style="width: 200px">Time Slots</th>
                            <th>Conflict Type</th>
                        </tr>
                    </table>
                    <div id="allowdeny" class="col-sm-offset-5">
                        <input type="button" id="allow" class="btn btn-success" value='Allow'>
                        <input type="button" id="deny" class="btn btn-danger" value='Deny'>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
