<%-- 
    Document   : Professor
    Created on : Nov 17, 2014, 2:41:25 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Professor</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script type="text/javascript" src="./resources/js/editProfessor.js"></script>
        <script src="./resources/js/jquery.cookie.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div  class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-3">
            <h2 class="col-sm-offset-4">Edit Professor</h2>
            <table id="entry">
                <form>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.5cm">Professor ID</span>
                                <input type="text" name="professorID" class="form-control" id="professorID" style="width: 6.6cm">
                            </div>
                            <span id="professorIDMessage" class="danger control-label"></span>
                        </div>
                    </div>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.5cm">First Name</span>
                                <input type="text" name="firstName" class="form-control" id="firstName" style="width: 6.6cm">
                            </div>
                            <span id="firstNameMessage" class="danger control-label"></span>
                        </div>
                    </div>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.5cm">Last Name</span>
                                <input type="text" name="lastName" class="form-control" id="lastName" style="width: 6.6cm">
                            </div>
                            <span id="lastNameMessage" class="danger control-label"></span>
                        </div>
                    </div>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.5cm">Professor Initials</span>
                                <input type="text" name="professorCode" class="form-control" id="professorCode" style="width: 6.6cm">
                            </div>
                            <span id="professorCodeMessage" class="danger control-label"></span>
                        </div>
                    </div>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.5cm">Course</span>
                                <div class="container" id="addCourses">
                                </div>
                            </div>
                            <span id="coursesMessage" class="danger control-label"></span>
                        </div>
                    </div>
                    <br>
                    <div class="col-sm-5 col-sm-offset-4">
                        <input type="button" class="btn btn-success" value='Save' id="submit1">
                        <input type="button" class="btn btn-warning" value='Cancel' id="cancel">
                    </div>
                    <div class="col-sm-3">
                        <span id="message" class="control-label"></span>
                    </div>
                </form>
            </table>
            <div>
                <table id="displayProfessors" class="displayBuildings" border="2px"></table>
            </div>


    </body>
</html>
