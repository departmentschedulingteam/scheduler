<%-- 
    Document   : SectionEdit
    Created on : Mar 31, 2015, 8:44:38 PM
    Author     : S519479
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Section/Lab</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="./resources/js/SectionEdit.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div class="col-md-11">
            <div id="dataEntry" class="col-md-offset-2 col-sm-offset-2">
                <h2 class="text-center">View/Edit/Delete : Section/Lab</h2>
                <table id="entry">
                    <form>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Discipline</span>
                                    <select name="departmentID" id="departmentID" class="form-control" style="width: 6cm">
                                        <option value="select">loading...</option>
                                    </select>
                                </div>
                                <span id="departmentNameMessage" class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.4cm">Course</span>
                                    <select name="course" id="course" class="form-control" style="width: 6cm">
                                        <option value="select">loading...</option>
                                    </select>
                                </div>
                                <span id="courseNameMessage" class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="col-sm-2 col-sm-offset-5">
                            <input type="button" id="submit" class="btn btn-success" value='Submit'>
                        </div>
                        <br>
<!--                        <div class="col-sm-offset-4">
                            <span id="message"></span>
                        </div>-->
                    </form>
                </table>
            </div>
            <br>
            <div class="col-md-offset-3">
                <div class="col-sm-offset-4">
                    <span id="message" name="message" ></span>
                </div>
            </div>
            <div id="detailsDiv" class="col-sm-offset-3">
                <table id="displaySection" class="table table-bordered table-responsive table-striped table-hover" border= "2px">
                    <tr>
                        <th>Section/Lab Id</th>
                        <th>Professor Name</th>
                        <th>Building Room</th>
                        <th style="width: 200px">Time Slots</th>
                    </tr>
                </table>
            </div>
        </div>
        <br><br>
    </body>
</html>