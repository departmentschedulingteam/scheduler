<%-- 
    Document   : Course
    Created on : Nov 17, 2014, 2:40:53 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Course</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="./resources/js/Course.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-3">
            <br><br>
            <div class="col-sm-offset-3">
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="dataEntry" id="dataEntryRadio" checked="checked">Create Course</label>
                </div>
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="tableValues" id="tableValuesRadio">Edit or View Courses</label>
                </div>
            </div>
            <div id="dataEntry" class="col-sm-offset-1">
                <h2 class="col-sm-offset-4">Add Course</h2>
                <table id="entry" >
                    <form>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.3cm">Discipline</span>
                                    <select name="departmentID" class="form-control" id="departmentID" style="width: 6cm">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                                <span id="departmentNameMessage" style="color: red"  class="danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.3cm">Course ID</span>
                                    <input type="text" name="courseID" class="form-control" id="courseID" style="width: 6cm">
                                </div>
                                <span id="courseIdMessage" style="color: red"  class="danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.3cm">Lab Type</span>
                                    <label class="radio-inline"><input type="radio" name="labType" value="pc" id="pc">Windows </label>
                                    <label class="radio-inline"><input type="radio" name="labType" value="mac" id="mac">MAC</label>
                                    <label class="radio-inline"><input type="radio" name="labType" value="NA" id="NA">Not Lab</label>
                                </div>
                                <span id="labTypeMessage" style="color: red"  class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.3cm">Course Name</span>
                                    <input type="text" name="courseName" class="form-control" id="courseName" style="width: 6cm">
                                </div>
                                <span id="courseNameMessage" style="color: red"  class="danger control-label"></span>
                            </div>
                        </div>
                        <br>
                        <div class="col-sm-1 col-sm-offset-4">
                            <input type="button" class="btn btn-success" value='Submit' id="submit1">
                        </div>
                        <br><br>
                        <div class="col-sm-offset-4">
                            <span id="messageInsert" class="control-label"></span>
                        </div>
                    </form>
                </table>
            </div>


            <div id="tableValues" class="col-sm-offset-1">
                <br>
                <div class="form-inline input-margin-bottom">
                    <div class="form-group col-sm-offset-2">
                        <div class="input-group">
                            <select name="departmentList" id="departmentList" class="form-control">
                                <option value="select">Loading...</option>
                            </select>
                        </div>
                        <input type="button" class="btn btn-success" value="Get Courses" onclick="getCourses()"/>
                    </div>
                </div>
                <div class="form-inline input-margin-bottom">
                    <div class="form-group col-sm-offset-2">
                        <table id="courseTable" class="table table-bordered table-responsive table-striped table-hover" border="2px">

                        </table>
                    </div>
                </div>
                <div class="col-sm-offset-3">
                    <span id="messageEdit" class="control-label"></span>
                </div>
            </div>
    </body>
</html>
