<%-- 
    Document   : index
    Created on : Nov 17, 2014, 2:39:45 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Room</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script type="text/javascript" src="./resources/js/Room.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-3">
            <br>
            <h2 class="col-sm-offset-3">Add/Edit Rooms</h2>
            <br>
            <div class="col-sm-offset-3">
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="dataEntry" id="dataEntryRadio" checked="checked">Room Entry</label>
                </div>
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="tableValues" id="tableValuesRadio">Edit or View Rooms</label>
                </div>
            </div>
            <br>
            <div id = "dataEntry" class="col-sm-offset-1">
                <table id="entry">
                    <form>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Building</span>
                                    <select name="displayBuildings" class="form-control" id="displayBuildings" style="width: 6cm">
                                        <option value="">Loading...</option>
                                    </select>
                                </div>
                                <span id="buildingMessage" style="color: red" class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Room Number</span>
                                    <input type="text" name="roomNumber" class="form-control" id="roomNumber" style="width: 6cm">
                                </div>
                                <span id="roomMessage" style="color: red" class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Lab Type</span>
                                    <label class="radio-inline"><input type="radio" name="labType" value="windows" id="pc">Windows</label>
                                    <label class="radio-inline"><input type="radio" name="labType" value="mac" id="mac">MAC</label>
                                    <label class="radio-inline"><input type="radio" name="labType" value="NA" id="NA">Not Lab</label>
                                </div>
                                <span id="labTypeMessage" style="color: red" class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Room Capacity</span>
                                    <input type="text" name="roomCapacity" class="form-control" id="roomCapacity" style="width: 6cm">
                                </div>
                                <span id="capacityMessage" style="color: red" class="text-danger control-label"></span>
                            </div>
                        </div>
                        <br>
                        <div class="col-sm-1 col-sm-offset-4">
                            <input type="button" value="Submit" class="btn btn-success" id="submit1">
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="col-sm-offset-2">
                            <span id="messageInsert"></span>
                        </div>
                    </form>
                </table>
            </div>
            <div id="tableValues">
                <div class="form-inline input-margin-bottom">
                    <div class="form-group col-sm-offset-2">
                        <div class="input-group">
                            <span class="input-group-addon">Building</span>
                            <select name="buildingList" id="buildingList" class="form-control col-sm-4">
                                <option value="select">Loading...</option>
                            </select>
                        </div>
                        <input type="button" class="btn btn-success" value="Get Rooms" onclick="getRooms()"/>
                    </div>
                </div>
                <div class="form-group col-sm-offset-2">
                    <table id="roomTable" class="table table-bordered table-responsive table-striped table-hover" border="2px">

                    </table>
                </div>
                <div class="form-group col-sm-offset-2">
                    <span id="messageEdit" class="control-label"></span>
                </div>
            </div>

    </body>
</html>
