<%-- 
    Document   : ManagementDuplicate
    Created on : 3 Apr, 2015, 8:40:49 AM
    Author     : sandeep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <jsp:include page="Home.jsp"/>

        <div class="top-nav">
            <div class="col-md-2">

            </div>
            <div class="col-md-2 margin-left" style="background-color: greenyellow; width: 3.5cm">
                <div>
                    <a href="Building.jsp">Building</a><br /><br />
                    <a href="Room.jsp">Room</a><br><br />
                    <a href="Department.jsp">Department</a><br><br />
                    <a href="Course.jsp">Course</a><br><br />
                    <a href="Professor.jsp">Professor</a><br><br />
                    <a href="SectionEdit.jsp">Section</a><br><br />
                    <a href="LabEdit.jsp">Lab</a><br><br />
                    <a href="SaveReport.jsp">Legacy Reports</a><br><br />
                </div>
            </div>
            <div class="col-md-6">
                <img src="resources/Images/Glass Windows.jpg" alt="" class="img-responsive"/>
            </div>
        </div>
    </body>
</html>
