<%-- 
    Document   : Home
    Created on : Nov 17, 2014, 10:58:23 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/jquery.table2excel.js"></script>
        <script src="./resources/js/jquery.cookie.js"></script>
        <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/js/bootstrap-hover-dropdown.js" type="text/javascript"></script>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body >
        <h2 class="text-center">Department Scheduling</h2>
        <div class="row">
            <div class="col-md-8 col-sm-8 col-sm-offset-2">
                <nav class="navbar navbar-inverse no-margin" style="background-color: green; color: blue">
                    <ul class="nav nav-pills ">
                        <li role="presentation" class="col-lg-push-3 col-sm-push-3 dropdown"><a href="ManagementDuplicate.jsp" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="Building.jsp">Building</a></li>
                                <li role="presentation"><a href="Room.jsp">Room</a></li>   
                                <li role="presentation"><a href="Department.jsp">Discipline</a></li>
                                <li role="presentation"><a href="Course.jsp">Course</a></li>   
                                <li role="presentation"><a href="Professor.jsp">Professor</a></li>  
                                <li role="presentation"><a href="SectionEdit.jsp">Section/Lab</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="col-lg-push-3 col-sm-push-3">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Schedule
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="Scheduler.jsp">Section</a></li>
                                <li role="presentation"><a href="Lab.jsp">Lab</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="col-lg-push-3 col-sm-push-3"><a href="" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Report
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="ReportPage.jsp">View Professor Report</a></li>
                                <li role="presentation"><a href="ReportRoomPage.jsp">View Room Report</a></li>
                                <li role="presentation"><a href="SaveReport.jsp">Clear Report</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="col-lg-push-3 col-sm-push-3"><a href="HomeDuplicate.jsp">Help</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </body>
</html>
