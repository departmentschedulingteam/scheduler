<%-- 
    Document   : Course
    Created on : Nov 17, 2014, 2:40:53 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Course</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="./resources/js/editCourse.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div class="col-md-7 col-md-offset-1">
            <br>

            <h2 class="col-sm-offset-3">Edit/Save Course</h2>
            <table id="entry" class="col-sm-offset-2">
                <form>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.3cm">Discipline</span>
                                <select name="departmentID" class="form-control" id="departmentID" style="width: 6cm"">
                                    <option value="">Select</option>
                                </select>
                            </div>
                            <span id="departmentNameMessage" class="danger control-label"></span>
                        </div>
                    </div>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.3cm">Course ID</span>
                                <input type="text" name="courseID" class="form-control" id="courseID" style="width: 6cm">
                            </div>
                            <span id="courseIdMessage" class="danger control-label"></span>
                        </div>
                    </div>

                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.3cm">Lab Type</span>
                                <label class="radio-inline"><input type="radio" name="labType" value="pc" id="pc">Windows </label>
                                <label class="radio-inline"><input type="radio" name="labType" value="mac" id="mac">MAC</label>
                                <label class="radio-inline"><input type="radio" name="labType" value="NA" id="NA">Not Lab</label>
                            </div>
                            <span id="labTypeMessage" class="text-danger control-label"></span>
                        </div>
                    </div>
                    <div class="form-inline input-margin-bottom">
                        <div class="form-group col-sm-offset-2">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 3.3cm">Course Name</span>
                                <input type="text" name="courseName" class="form-control" id="courseName" style="width: 6cm">
                            </div>
                            <span id="courseNameMessage" class="danger control-label"></span>
                        </div>
                    </div>
                    <br>
                    <div class="col-sm-5 col-sm-offset-4">
                        <input type="button" class="btn btn-success" value='Save' id="save">
                        <input type="button" class="btn btn-warning" value='Cancel' id="cancel">
                    </div>
                    <div class="col-sm-3">
                        <span id="message" class="control-label"></span></span>
                    </div>
                    <div class="col-sm-3">
                        <h3 id="message"></h3>
                    </div>
                </form>
            </table>
        </div>




    </body>
</html>
