<%-- 
    Document   : Department
    Created on : Nov 17, 2014, 2:41:16 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Discipline</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script type="text/javascript" src="./resources/js/Department.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">
    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-3">
            <br>
            <h2 class="col-sm-offset-3">Add/Edit Discipline</h2>
            <br>
            <div class="col-sm-offset-3">
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="dataEntry" id="dataEntryRadio" checked="checked">Create Discipline</label>
                </div>
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="tableValues" id="tableValuesRadio">Edit or View Disciplines</label>
                </div>
            </div>
            <br>
            <div id="dataEntry" class="col-sm-offset-1">
                
                <table id="entry">
                    <form>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 4.3cm">Discipline Id</span>
                                    <input type="text" class="form-control" id="departmentID" name="departmentID"/>
                                </div>
                                <span id="departmentIdMessage" style="color: red"  class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 4.3cm">Discipline Name</span>
                                    <input type="text" class="form-control" id="departmentName" name="departmentName"/>
                                </div>
                                <span id="disciplineNameMessage" style="color: red"  class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 4.3cm">Discipline Description</span>
                                    <input type="text" class="form-control" id="departmentDesc" name="departmentDesc"/>
                                </div>
                                <span id="disciplineCodeMessage" style="color: red"  class="text-danger control-label"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-4">
                                <div class="input-group">
                                    <input type="button" class="btn btn-success" value='Submit' id="submit1">
                                </div>
                            </div>
                        </div>

                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <input type="hidden" name="reqType" id="reqType" value="inserting"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-3">
                                <div class="input-group">
                                    <span id="messageInsert" class="control-label"></span>
                                </div>
                            </div>
                        </div>

                    </form>
                </table>
            </div>
            <div id="tableValues">
                
                <div class="col-sm-3 col-sm-offset-3">
                    <input type="button" value="View Disciplines" class="btn btn-success form-control" id="viewDepartments" />
                </div>
                <br><br><br>
                <div class=" col-sm-offset-1">
                    <table id="displayDepartments" class="table table-bordered table-responsive table-striped table-hover" border="2px">
                    </table>
                </div>
                <div class="form-inline input-margin-bottom">
                    <div class="form-group col-sm-offset-3">
                        <div class="input-group">
                            <span id="messageEdit" class="control-label"></span>
                        </div>
                    </div>
                </div>
            </div>

    </body>
</html>
