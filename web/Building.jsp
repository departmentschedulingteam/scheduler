<%-- 
    Document   : Department
    Created on : Nov 17, 2014, 2:41:16 PM
    Author     : S519397
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Building</title>
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/Building.js" ></script>
        <link rel="stylesheet" type="text/css" href="./resources/css/schedule.css">

    </head>
    <body>
        <jsp:include page="Management.jsp"/>
        <div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-3">
            <br>
            <h2 class="col-sm-offset-3">Add/Edit Building</h2>
            <br>
            <div class="col-sm-offset-3">
                <div class="radio-inline">
                    <label><input type="radio" name="divType" value="dataEntry" id="dataEntryRadio" checked="checked">Building Entry</label>
                </div>
                <div class="radio-inline">
                    <label> <input type="radio" name="divType" value="tableValues" id="tableValuesRadio">Edit or View Building</label>
                </div>
            </div>
            <br>
            <div id="dataEntry" class="col-sm-offset-1">
                <table id="entry">
                    <form>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Building Code</span>
                                    <input type="text" id="buildingID" class="form-control" name="buildingID" />
                                </div>
                                <span id="idMessage" class="control-label" style="color: red" for="inputGroupSuccess3"></span>
                            </div>
                        </div>
                        <div class="form-inline input-margin-bottom">
                            <div class="form-group col-sm-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width: 3.5cm">Building Name</span>
                                    <input type="text" id="buildingName" class="form-control" name="buildingName" />
                                </div>
                                <span id="nameMessage" class="control-label" style="color: red"  for="inputGroupSuccess3"></span>
                            </div>
                        </div>
                        <br>
                        <div class="col-sm-1 col-sm-offset-4">
                            <input type="button" value='Submit' class="btn btn-success" id="submit1">
                        </div>
                        <div class="col-sm-offset-3">
                            <input type="hidden" name="reqType" id="reqType" value="inserting" />
                        </div>
                        <br><br><br>
                        <div class="col-sm-offset-2">
                            <span id="messageInsert"></span>
                        </div>
                    </form>
                </table>
            </div>
            <div id="tableValues">
                <br>
                <div class="col-sm-offset-3">
                    <input type="button" value="View Buildings" class="btn btn-success" id="viewBuildings" />
                </div>
                <br>
                <div class="col-sm-offset-3">
                    <table id="displayBuildings" border="2px" class="table table-bordered table-responsive table-striped table-hover"></table>
                </div>
                <div class="col-sm-offset-3">
                    <span id="messageEdit"></span>
                </div>
            </div>
        </div>
    </body>
</html>
